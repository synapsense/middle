/**
 * 
 */
package com.synapsense.hpplugin.configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.snmp4j.smi.OctetString;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import com.synapsense.hpplugin.constants.EnumAccess;
import com.synapsense.hpplugin.constants.EnumHPSymbol;
import com.synapsense.hpplugin.constants.EnumSNMPAuthProtocol;
import com.synapsense.hpplugin.constants.EnumSNMPPrivProtocol;
import com.synapsense.hpplugin.constants.EnumSNMPTransport;
import com.synapsense.hpplugin.constants.EnumSNMPType;
import com.synapsense.hpplugin.constants.EnumSNMPVersion;
import com.synapsense.hpplugin.constants.EnumType;
import com.synapsense.hpplugin.snmpobjects.SNMPNotify;
import com.synapsense.hpplugin.snmpobjects.SNMPObject;
import com.synapsense.hpplugin.snmpobjects.SNMPv2c;
import com.synapsense.hpplugin.snmpobjects.SNMPv3;

/**
 * Class loads a configuration from XML file.
 * 
 * @author Alexander Kravin
 * 
 */
public class Configuration {

	private static final Logger log = Logger.getLogger(Configuration.class);
	private static Configuration _instance = null;
	private String pathXML;
	private String pathXSD;
	private ArrayList<XMLObject> arrayXMLObject;

	private static final String TAG_OBJECTS = "configuration/objects[@version='1']";
	private static final String TAG_TARGET = "configuration/notificationTarget";
	private static final String TAG_OBJECT = "object";
	private static final String TAG_PROPERTY = "property";
	private static final String TAG_V2C = "v2c";
	private static final String TAG_V3 = "v3";
	private static final String TAG_AUTHENTICATION = "authentication";
	private static final String TAG_PRIVACY = "privacy";

	private static final String PROPERTY_REFID = "@refID";
	private static final String PROPERTY_REFPARENTID = "@refParentID";
	private static final String PROPERTY_REFNAMEID = "@refNameID";

	private static final String PROPERTY_OID = "@OID";
	private static final String PROPERTY_ID = "@ID";
	private static final String PROPERTY_NAME = "@name";
	private static final String PROPERTY_TYPE = "@type";
	private static final String PROPERTY_SNMPTYPE = "@snmpType";
	private static final String PROPERTY_MIN = "@min";
	private static final String PROPERTY_MAX = "@max";
	private static final String PROPERTY_ACCESS = "@access";

	private static final String PROPERTY_ADDRESS = "@address";
	private static final String PROPERTY_PORT = "@port";
	private static final String PROPERTY_SNMPVERSION = "@SNMPversion";
	private static final String PROPERTY_TRANSPORT = "@transport";
	private static final String PROPERTY_TIMEOUT = "@timeout";
	private static final String PROPERTY_RETRIES = "@retries";

	private static final String PROPERTY_COMMUNITY = "@community";

	private static final String PROPERTY_SECURITYNAME = "@securityName";
	private static final String PROPERTY_CONTEXTNAME = "@contextName";
	private static final String PROPERTY_ENABLED = "@enabled";
	private static final String PROPERTY_PROTOCOL = "@protocol";
	private static final String PROPERTY_PASSWORD = "@password";

	/**
	 * Initialization of a class
	 */
	private Configuration() {
		arrayXMLObject = new ArrayList<XMLObject>();
	}

	/**
	 * @return An instance of object
	 */
	public synchronized static Configuration getInstance() {
		if (_instance == null)
			_instance = new Configuration();
		return _instance;
	}

	public void loadXML(String pathXML, String pathXSD) throws IOException, ParserConfigurationException, SAXException,
	        XPathExpressionException {

		this.pathXML = pathXML;
		this.pathXSD = pathXSD;

		if (log.isDebugEnabled()) {
			log.debug("Try to load XML file..." + this);
		}
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setNamespaceAware(true);
		docFactory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage",
		        XMLConstants.W3C_XML_SCHEMA_NS_URI);

		// Parse an XML document into a DOM tree.
		DocumentBuilder parser = docFactory.newDocumentBuilder();
		Document document = parser.parse(new File(pathXML));

		// Create a SchemaFactory capable of understanding WXS schemas.
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		// Load a WXS schema, represented by a Schema instance.
		Source schemaFile = new StreamSource(new File(pathXSD));
		Schema schema = factory.newSchema(schemaFile);

		// Create a Validator object, which can be used to
		// validate an instance document
		Validator validator = schema.newValidator();

		// Validate the DOM tree.
		validator.validate(new DOMSource(document));

		// Init XPath
		XPath xpath = XPathFactory.newInstance().newXPath();

		// Parsing
		Node objects = (Node) xpath.evaluate(TAG_OBJECTS, document, XPathConstants.NODE);

		NodeList objectSet = (NodeList) xpath.evaluate(TAG_OBJECT, objects, XPathConstants.NODESET);

		for (int i = 0; i < objectSet.getLength(); i++) {
			Node object = objectSet.item(i);
			String objectName = (String) xpath.evaluate(PROPERTY_NAME, object, XPathConstants.STRING);
			String oid = (String) xpath.evaluate(PROPERTY_OID, object, XPathConstants.STRING);
			Double refID = (Double) xpath.evaluate(PROPERTY_REFID, object, XPathConstants.NUMBER);
			Double refParentID = (Double) xpath.evaluate(PROPERTY_REFPARENTID, object, XPathConstants.NUMBER);
			Double refNameID = (Double) xpath.evaluate(PROPERTY_REFNAMEID, object, XPathConstants.NUMBER);

			// Create new XMLObject
			XMLObject xmlObject = new XMLObject(objectName, oid, refID.intValue(), refParentID.intValue(),
			        refNameID.intValue());

			String id;
			Double min, max;
			String name, snmpType, type, access;

			NodeList propertySet = (NodeList) xpath.evaluate(TAG_PROPERTY, object, XPathConstants.NODESET);
			for (int j = 0; j < propertySet.getLength(); j++) {
				Node property = propertySet.item(j);

				id = (String) xpath.evaluate(PROPERTY_ID, property, XPathConstants.STRING);
				min = (Double) xpath.evaluate(PROPERTY_MIN, property, XPathConstants.NUMBER);
				max = (Double) xpath.evaluate(PROPERTY_MAX, property, XPathConstants.NUMBER);

				name = (String) xpath.evaluate(PROPERTY_NAME, property, XPathConstants.STRING);
				snmpType = (String) xpath.evaluate(PROPERTY_SNMPTYPE, property, XPathConstants.STRING);
				type = (String) xpath.evaluate(PROPERTY_TYPE, property, XPathConstants.STRING);
				access = (String) xpath.evaluate(PROPERTY_ACCESS, property, XPathConstants.STRING);

				// Add property to XMLObject
				xmlObject.addProperty(new XMLProperty(id, name, EnumType.valueOf(type), EnumSNMPType.valueOf(snmpType),
				        min.longValue(), max.longValue(), EnumAccess.valueOf(access)));
			}

			// Add XMLObject to configuration
			arrayXMLObject.add(xmlObject);
			if (log.isDebugEnabled()) {
				log.debug("A new XMLObject was added: " + xmlObject);
			}
		}

		// Notification target
		Node target = (Node) xpath.evaluate(TAG_TARGET, document, XPathConstants.NODE);

		String address = (String) xpath.evaluate(PROPERTY_ADDRESS, target, XPathConstants.STRING);
		Double port = (Double) xpath.evaluate(PROPERTY_PORT, target, XPathConstants.NUMBER);
		String snmpVersion = (String) xpath.evaluate(PROPERTY_SNMPVERSION, target, XPathConstants.STRING);
		String transport = (String) xpath.evaluate(PROPERTY_TRANSPORT, target, XPathConstants.STRING);
		Double timeout = (Double) xpath.evaluate(PROPERTY_TIMEOUT, target, XPathConstants.NUMBER);
		Double retries = (Double) xpath.evaluate(PROPERTY_RETRIES, target, XPathConstants.NUMBER);

		SNMPObject snmpObject = null;
		String temp;
		switch (EnumSNMPVersion.valueOf(snmpVersion)) {
		case V2C:
			OctetString community = null;
			Node nodeV2C = (Node) xpath.evaluate(TAG_V2C, target, XPathConstants.NODE);
			temp = (String) xpath.evaluate(PROPERTY_COMMUNITY, nodeV2C, XPathConstants.STRING);

			if (!temp.isEmpty())
				community = new OctetString(temp);

			snmpObject = new SNMPv2c(address, port.intValue(), EnumSNMPTransport.valueOf(transport), community, null);
			break;
		case V3:

			OctetString authPassword = null;
			OctetString privPassword = null;
			String authProtocol = null;
			String privProtocol = null;

			Node nodeV3 = (Node) xpath.evaluate(TAG_V3, target, XPathConstants.NODE);

			// Set security name
			temp = (String) xpath.evaluate(PROPERTY_SECURITYNAME, nodeV3, XPathConstants.STRING);
			OctetString securityName = new OctetString(temp);

			// Set context name
			temp = (String) xpath.evaluate(PROPERTY_CONTEXTNAME, nodeV3, XPathConstants.STRING);
			OctetString contextName = new OctetString(temp);

			Node authV3 = (Node) xpath.evaluate(TAG_AUTHENTICATION, nodeV3, XPathConstants.NODE);

			Boolean enabled = (Boolean) xpath.evaluate(PROPERTY_ENABLED, authV3, XPathConstants.BOOLEAN);
			// Authentication protocol
			if (enabled) {
				authProtocol = (String) xpath.evaluate(PROPERTY_PROTOCOL, authV3, XPathConstants.STRING);
				temp = (String) xpath.evaluate(PROPERTY_PASSWORD, authV3, XPathConstants.STRING);
				if (!temp.isEmpty())
					authPassword = new OctetString(temp);
			}
			// Privacy protocol
			Node privV3 = (Node) xpath.evaluate(TAG_PRIVACY, nodeV3, XPathConstants.NODE);
			enabled = (Boolean) xpath.evaluate(PROPERTY_ENABLED, privV3, XPathConstants.BOOLEAN);

			if (enabled) {
				privProtocol = (String) xpath.evaluate(PROPERTY_PROTOCOL, privV3, XPathConstants.STRING);
				temp = (String) xpath.evaluate(PROPERTY_PASSWORD, privV3, XPathConstants.STRING);
				if (!temp.isEmpty())
					privPassword = new OctetString(temp);
			}

			snmpObject = new SNMPv3(address, port.intValue(), EnumSNMPTransport.valueOf(transport), securityName,
			        contextName, EnumSNMPAuthProtocol.valueOf(authProtocol), authPassword,
			        EnumSNMPPrivProtocol.valueOf(privProtocol), privPassword);
			break;
		}

		snmpObject.setTimeoutAndRetries(timeout.intValue(), retries.intValue());
		// Initialization the notification target
		SNMPNotify.getInstance().init(snmpObject);
		if (log.isDebugEnabled()) {
			log.debug("Notification target was initialized: " + snmpObject);
		}
		if (log.isInfoEnabled()) {
			log.info("XML file was loaded successfully");
		}
	}

	/**
	 * Gets XMLObject by specify name
	 * 
	 * @param object
	 *            it is a constant value from <code>XMLObject</code> class
	 * 
	 * @return an <code>XMLObject</code> instance
	 */
	public XMLObject getXMLObjectByName(EnumHPSymbol object) {
		XMLObject xmlObject = null;
		String name = object.getObjectName();
		for (int i = 0; i < arrayXMLObject.size(); i++) {
			xmlObject = arrayXMLObject.get(i);
			String objectName = xmlObject.getName();
			if (objectName.equals(name))
				break;
		}
		return xmlObject;
	}

	@Override
	public String toString() {
		return "{pathXML=\"" + pathXML + "\", pathXSD=\"" + pathXSD + "\"}";
	}

}
