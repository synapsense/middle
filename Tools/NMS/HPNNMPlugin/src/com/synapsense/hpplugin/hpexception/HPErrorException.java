package com.synapsense.hpplugin.hpexception;

/**
 * @author Alexander Kravin
 * 
 */
public class HPErrorException extends Exception {

	private static final long serialVersionUID = 1L;

	public HPErrorException() {
		super();
	}

	public HPErrorException(String msg) {
		super(msg);
	}

}
