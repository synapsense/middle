/**
 * 
 */
package com.synapsense.hpplugin.hpexception;

/**
 * @author Alexander Kravin
 * 
 */
public class HPWarnException extends Exception {

	private static final long serialVersionUID = 1L;

	public HPWarnException() {
		super();
	}

	public HPWarnException(String msg) {
		super(msg);
	}
}
