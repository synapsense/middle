/**
 * 
 */
package com.synapsense.hpplugin.auditor;

import hp.ov.libovw.LibOvw;
import hp.ov.libovw.LibOvwException;
import hp.ov.libovw.OVwFieldBindList;
import hp.ov.libovw.OVwFieldBinding;
import hp.ov.libovw.OVwFieldValue;
import hp.ov.libovw.OVwObjectList;
import hp.ov.libovw.OVwSymbolInfo;
import hp.ov.libovw.OVwSymbolList;

import org.apache.log4j.Logger;

import com.synapsense.hpplugin.HPMain;
import com.synapsense.hpplugin.configuration.Configuration;
import com.synapsense.hpplugin.configuration.XMLObject;
import com.synapsense.hpplugin.configuration.XMLProperty;
import com.synapsense.hpplugin.constants.EnumHPField;
import com.synapsense.hpplugin.constants.EnumHPSymbol;
import com.synapsense.hpplugin.hpobjects.HPLibovw;
import com.synapsense.hpplugin.hpobjects.HPObject;
import com.synapsense.hpplugin.snmpobjects.SNMPFactory;
import com.synapsense.hpplugin.snmpobjects.SNMPObject;
import com.synapsense.hpplugin.snmpobjects.SynapSNMP;
import com.synapsense.hpplugin.utils.HPUtils;

/**
 * Tries to connect with SNMP the agent through the specified time interval and
 * changes the status of a symbol
 * 
 * @author Alexander Kravin
 * 
 */
public class Auditor extends Thread {
	private static final Logger log = Logger.getLogger(Auditor.class);
	private static final XMLObject xmlAgent = Configuration.getInstance().getXMLObjectByName(
	        EnumHPSymbol.SYNAPSNMPAGENT);
	private static final OVwFieldBindList bindListSNMP2c = getBooleanFieldBindList(EnumHPField.FLD_IS_SYNAPPCSNMP2C);
	private static final OVwFieldBindList bindListSNMP3 = getBooleanFieldBindList(EnumHPField.FLD_IS_SYNAPPCSNMP3);
	private static Auditor a;

	static {
		a = new Auditor();
		a.start();
	}

	/**
	 * Default constructor
	 */
	protected Auditor() {
		super("Auditor");
		if (log.isInfoEnabled()) {
			log.info("Auditor has been started successfully");
		}
		setDaemon(true);
	}

	public static void runAudit() {

	};

	/**
	 * @param hpField
	 *            an <code>EnumHPField</code> instance
	 * 
	 * @return a list of bindings fields
	 * 
	 * @throws LibOvwException
	 *             if cannot get a field's name by specified field's id
	 */
	private static OVwFieldBindList getBooleanFieldBindList(EnumHPField hpField) throws LibOvwException {
		if (!hpField.isBoolean()) {
			throw new IllegalArgumentException("Argument is not a boolean field");
		}
		LibOvw libovw = HPLibovw.getInstance();

		int fieldId = libovw.OVwDbFieldNameToFieldId(hpField.getDbName());

		OVwFieldBinding fldBind = new OVwFieldBinding();
		fldBind.field_val = new OVwFieldValue();
		fldBind.field_val.field_type = OVwFieldValue.ovwBooleanField;
		fldBind.field_id = fieldId;
		fldBind.field_val.bool_val = true;

		OVwFieldBindList bindList = new OVwFieldBindList();
		bindList.count = 1;
		bindList.fields = new OVwFieldBinding[1];
		bindList.fields[0] = fldBind;
		return bindList;
	}

	@Override
	public void run() {

		while (HPLibovw.getInstance() != null) {
			try {
				log.debug("Audit");
				checkConnection(bindListSNMP2c);
				checkConnection(bindListSNMP3);
				sleep(HPMain.getPoolTime());
			} catch (Exception ex) {
				log.fatal("Error from WaitForDisconnect");
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("Audit disconnect");
		}
	}

	/**
	 * Checks the connection with SNMP Agent
	 * 
	 * @param bindList
	 *            an <code>OVwFieldBindList</code> instance
	 */
	private void checkConnection(OVwFieldBindList bindList) {
		OVwSymbolInfo symbol = null;
		try {
			LibOvw libovw = HPLibovw.getInstance();
			OVwObjectList objectList = libovw.OVwListObjectsOnMap(libovw.OVwGetMapInfo(), bindList);
			for (int i = 0; i < objectList.count; i++) {
				OVwSymbolList symbolList = libovw.OVwGetSymbolsByObject(libovw.OVwGetMapInfo(),
				        objectList.objects[i].object_id);

				for (int j = 0; j < symbolList.count; j++) {
					symbol = symbolList.symbols[j];
					if (symbol.symbol_status != OVwSymbolInfo.ovwUnmanagedStatus) {
						EnumHPSymbol hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
						if (hpSymbol == null)
							continue;
						if (log.isInfoEnabled()) {
							log.info("Checking the connection from [" + symbol.symbol_label + "] symbol");
						}

						HPObject hpObject = new HPObject(symbol);
						SNMPObject snmpObject = SNMPFactory.getSNMPObject(hpObject);

						// Set timeout and retries
						snmpObject.setTimeoutAndRetries(1000, 1);

						// Connect to SNMP agent
						SynapSNMP synapsnmp = new SynapSNMP(snmpObject);

						synapsnmp.connect();
						// Try to get information from SNMP agent
						for (int q = 0; q < xmlAgent.getArrayXMLProperty().size(); q++) {
							XMLProperty xmlProperty = xmlAgent.getArrayXMLProperty().get(q);
							synapsnmp.clearPDU();
							synapsnmp.addVariable(xmlAgent.getOIDById(xmlProperty.getId()));
							String response = synapsnmp.snmpGet();
							if (response.isEmpty()) {
								HPUtils.setSymbolStatus(symbol, OVwSymbolInfo.ovwCriticalStatus);
								break;
							}
							HPUtils.setSymbolStatus(symbol, OVwSymbolInfo.ovwNormalStatus);
						}
						synapsnmp.disconnect();
					}
				}
			}
		} catch (Exception e) {
			log.warn("Cannot connect from " + HPUtils.symbolToString(symbol), e);
			HPUtils.setSymbolStatus(symbol, OVwSymbolInfo.ovwCriticalStatus);
		}
	}

}
