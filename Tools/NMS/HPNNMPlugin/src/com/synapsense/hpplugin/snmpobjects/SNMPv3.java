/**
 * 
 */
package com.synapsense.hpplugin.snmpobjects;

import org.apache.log4j.Logger;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.smi.OctetString;

import com.synapsense.hpplugin.constants.EnumHPField;
import com.synapsense.hpplugin.constants.EnumHPSymbol;
import com.synapsense.hpplugin.constants.EnumSNMPAuthProtocol;
import com.synapsense.hpplugin.constants.EnumSNMPPrivProtocol;
import com.synapsense.hpplugin.constants.EnumSNMPTransport;
import com.synapsense.hpplugin.hpobjects.HPObject;

/**
 * Class describes necessary parameters to implement the User Based Security
 * Model (USM) in SNMP v3.
 * 
 * @author Alexander Kravin
 */
public class SNMPv3 extends SNMPObject {

	private static final Logger log = Logger.getLogger(SNMPv3.class);

	private OctetString securityName = null;
	private OctetString contextName = null;

	private EnumSNMPAuthProtocol authProtocol;
	private OctetString authPassword = null;

	private EnumSNMPPrivProtocol privProtocol;
	private OctetString privPassword = null;

	private int securityLevel;

	/**
	 * This constructor initializes a class by specified parameters.
	 * 
	 * @param address
	 *            SNMP agent's IP address.
	 * @param port
	 *            SNMP agent's port.
	 * @param snmpTransport
	 *            SNMP transport (UDP or TCP).
	 * @param securityName
	 *            the securityName used for authenticated SNMPv3 messages.
	 * @param contextName
	 *            the destination contextName used for SNMPv3 messages.
	 * @param authProtocol
	 *            the authentication protocol (MD5 or SHA) used for
	 *            authenticated SNMPv3 messages.
	 * @param authPassword
	 *            the authentication pass phrase used for authenticated SNMPv3
	 *            messages.
	 * @param privProtocol
	 *            the privacy protocol used for encrypted SNMPv3 messages.
	 * @param privPassword
	 *            the privacy pass phrase used for authenticated SNMPv3
	 *            messages.
	 */
	public SNMPv3(String address, int port, EnumSNMPTransport snmpTransport, OctetString securityName,
	        OctetString contextName, EnumSNMPAuthProtocol authProtocol, OctetString authPassword,
	        EnumSNMPPrivProtocol privProtocol, OctetString privPassword) {
		super(address, port, snmpTransport);

		this.securityName = securityName;
		this.contextName = contextName;

		this.authProtocol = authProtocol;
		this.authPassword = authPassword;

		this.privProtocol = privProtocol;
		this.privPassword = privPassword;

		// Set security level
		setSecurityLevel();

	}

	/**
	 * @param hpObject
	 *            an <code>HPObject</code> instance must has a type of
	 *            <code>EnumHPSymbol.SYNAPPCSNMP3</code>.
	 */
	public SNMPv3(HPObject hpObject) {
		super(hpObject);
		if (hpObject.getHPSymbol() != EnumHPSymbol.SYNAPPCSNMP3) {
			throw new IllegalArgumentException("SNMP3 instance supports only");
		}

		String str;

		// Set security name
		str = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SNMPSECNAME);
		if (str != null)
			securityName = new OctetString(str);

		// Set context name
		str = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SNMPCONTEXTNAME);
		if (str != null)
			contextName = new OctetString(str);

		// Set authentication protocol and pass phrase
		authProtocol = EnumSNMPAuthProtocol.valueOf(hpObject.getHPFieldBindList().getStringValue(
		        EnumHPField.FLD_SNMPAUTHPROTOCOL));
		if (EnumSNMPAuthProtocol.Unset != authProtocol) {
			str = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SNMPAUTHPWD);
			if (str != null)
				authPassword = new OctetString(str);
		}

		// Set privacy protocol and pass phrase
		privProtocol = EnumSNMPPrivProtocol.valueOf(hpObject.getHPFieldBindList().getStringValue(
		        EnumHPField.FLD_SNMPPRIVPROTOCOL));
		if (EnumSNMPPrivProtocol.Unset != privProtocol) {
			str = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SNMPPRIVPWD);
			if (str != null)
				privPassword = new OctetString(str);
		}

		// Set timeout and retries
		int timeout = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SNMPTIMEOUT));
		int retries = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SNMPRETRIES));
		setTimeoutAndRetries(timeout, retries);

		// Set security level
		setSecurityLevel();

		if (log.isDebugEnabled()) {
			log.debug("Object has been initialized " + this);
		}
	}

	/**
	 * Sets a security level
	 */
	private void setSecurityLevel() {
		if (EnumSNMPAuthProtocol.Unset != authProtocol && EnumSNMPPrivProtocol.Unset != privProtocol) {
			securityLevel = SecurityLevel.AUTH_PRIV;
		} else if (EnumSNMPAuthProtocol.Unset != authProtocol) {
			securityLevel = SecurityLevel.AUTH_NOPRIV;
		} else {
			securityLevel = SecurityLevel.NOAUTH_NOPRIV;
		}
	}

	/**
	 * @return SNMP authentication protocol.
	 */
	public EnumSNMPAuthProtocol getAuthProtocol() {
		return authProtocol;
	}

	/**
	 * @return An authentication password.
	 */
	public OctetString getAuthPassword() {
		return authPassword;
	}

	/**
	 * @return A privacy protocol.
	 */
	public EnumSNMPPrivProtocol getPrivProtocol() {
		return privProtocol;
	}

	/**
	 * @return A privacy password.
	 */
	public OctetString getPrivPassword() {
		return privPassword;
	}

	/**
	 * @return A security name.
	 */
	public OctetString getSecurityName() {
		return securityName;
	}

	/**
	 * @return A context name.
	 */
	public OctetString getContextName() {
		return contextName;
	}

	/**
	 * @return A security level.
	 */
	public int getSecurityLevel() {
		return securityLevel;
	}

	@Override
	public String toString() {
		return super.toString() + "{secName=\"" + securityName + "\", contextName=\"" + contextName
		        + "\", authProtocol=\"" + authProtocol + "\", authPwd=\"" + authPassword + "\", privProtocol=\""
		        + privProtocol + "\", privPwd=\"" + privPassword + "\", secLevel=" + securityLevel + "}";
	}
}
