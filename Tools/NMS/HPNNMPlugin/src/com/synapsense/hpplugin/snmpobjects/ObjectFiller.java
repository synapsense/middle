/**
 * 
 */
package com.synapsense.hpplugin.snmpobjects;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TableListener;

import com.synapsense.hpplugin.configuration.XMLObject;
import com.synapsense.hpplugin.synapobjects.SynapObject;

/**
 * Creates an array list of SynapObject instances from SNMP agent.
 * 
 * @author Alexander Kravin
 * 
 */
public class ObjectFiller implements TableListener {

	private static final Logger log = Logger.getLogger(ObjectFiller.class);

	private ArrayList<SynapObject> arraySynapObject;
	private String[] filter;
	private XMLObject xmlObject;

	/**
	 * Initializes the instance.
	 * 
	 * @param xmlObject
	 *            an <code>XMLObject</code> instance.
	 */
	public ObjectFiller(XMLObject xmlObject) {
		this.xmlObject = xmlObject;
		arraySynapObject = new ArrayList<SynapObject>();
	}

	/**
	 * Initialize the instance and set a filter.
	 * 
	 * @param xmlObject
	 *            an <code>XMLObject</code> instance
	 * @param filter
	 *            a filter consists of necessary objects' ID from SNMP agent
	 */
	public ObjectFiller(XMLObject xmlObject, String[] filter) {
		this(xmlObject);
		this.filter = filter;
	}

	/**
	 * Sets a filter
	 * 
	 * @param filter
	 *            a filter consists of necessary objects' ID from SNMP agent
	 */
	public void setFilter(String[] filter) {
		this.filter = filter;
	}

	@Override
	public void finished(TableEvent tblEvent) {

		synchronized (tblEvent.getUserObject()) {
			tblEvent.getUserObject().notify();
		}
	}

	@Override
	public boolean next(TableEvent tblEvent) {

		SynapObject synapObject = new SynapObject(xmlObject);
		int count = synapObject.getDataSize();

		for (int i = 0; i < count; i++) {
			synapObject.setValue(i, tblEvent.getColumns()[i].getVariable().toString());
		}

		if (filter != null) {
			for (int i = 0; i < filter.length; i++) {
				if (filter[i].isEmpty())
					continue;
				else if (!filter[i].equals(synapObject.getValue(i))) {
					return true;
				}
			}
		}
		arraySynapObject.add(synapObject);

		if (log.isDebugEnabled()) {
			log.debug(synapObject);
		}

		return true;
	}

	/**
	 * @return an array list of the <code>SynapObject</code> instances
	 */
	public ArrayList<SynapObject> getListSynapObject() {
		return arraySynapObject;
	}

	/**
	 * @return an <code>XMLObject</code> instance
	 */
	public XMLObject getXMLObject() {
		return xmlObject;
	}
}
