package com.synapsense.hpplugin.snmpobjects;

import org.apache.log4j.Logger;

import com.synapsense.hpplugin.constants.EnumHPField;
import com.synapsense.hpplugin.constants.EnumSNMPTransport;
import com.synapsense.hpplugin.hpobjects.HPObject;

/**
 * @author Alexander Kravin
 */
public class SNMPObject {

	private static final Logger log = Logger.getLogger(SNMPObject.class);

	private int retries = 2;
	private int timeout = 1000; // count of seconds

	private String address;
	private int port;
	private EnumSNMPTransport snmpTransport;

	/**
	 * This constructor initializes a class by specified parameters.
	 * 
	 * @param address
	 *            SNMP agent's IP address.
	 * @param port
	 *            SNMP agent's port.
	 * @param snmpTransport
	 *            an <code>EnumSNMPTransport</code> instance must be (UDP or
	 *            TCP).
	 */
	public SNMPObject(String address, int port, EnumSNMPTransport snmpTransport) {
		this.address = address;
		this.port = port;
		this.snmpTransport = snmpTransport;
	}

	/**
	 * This constructor initializes a class by specified an
	 * <code>HPObject</code> instance.
	 * 
	 * @param hpObject
	 *            an <code>HPObject</code> instance must be (
	 *            <code>SYNAPPCSNMPv2c or SYNAPPCSNMPv3</code>).
	 */
	public SNMPObject(HPObject hpObject) {
		if (hpObject.getHPSymbol().getLevel() != 0) {
			throw new IllegalArgumentException("SNMPv2c instance supports SYNAPPCSNMPv2c or SYNAPPCSNMPv3 only");
		}

		this.address = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_ADDRESS);
		this.port = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_PORT));
		this.snmpTransport = EnumSNMPTransport.valueOf(hpObject.getHPFieldBindList().getStringValue(
		        EnumHPField.FLD_SNMPTRANSPORT));
	}

	/**
	 * Sets a timeout and retries.
	 * 
	 * @param timeout
	 *            a timeout in seconds between retries. The default is 10
	 *            seconds.
	 * @param retries
	 *            a number of retries to be used in the requests. The default is
	 *            2.
	 */
	public void setTimeoutAndRetries(int timeout, int retries) {
		this.timeout = timeout;
		this.retries = retries;
		if (log.isDebugEnabled()) {
			log.debug("Changed: {timeout=" + timeout + ", retries=" + retries);
		}
	}

	/**
	 * @return SNMP transport (UDP or TCP).
	 */
	public EnumSNMPTransport getSNMPTransport() {
		return snmpTransport;
	}

	/**
	 * @return SNMP agent's IP address.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return SNMP agent's port.
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @return a number of retries to be used in the requests.
	 */
	public int getRetries() {
		return retries;
	}

	/**
	 * @return a timeout in seconds between retries.
	 */
	public int getTimeout() {
		return timeout;
	}

	@Override
	public String toString() {
		return "{IP address=\"" + address + "\", port=" + port + ", " + "transport=\"" + snmpTransport + "\", retries="
		        + retries + ", timeout=" + timeout + "}";
	}
}
