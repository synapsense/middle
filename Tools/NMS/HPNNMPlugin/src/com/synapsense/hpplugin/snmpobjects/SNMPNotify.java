/**
 * 
 */
package com.synapsense.hpplugin.snmpobjects;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OctetString;

import com.synapsense.hpplugin.constants.EnumNotifyType;
import com.synapsense.hpplugin.constants.EnumSNMPTransport;
import com.synapsense.hpplugin.constants.EnumSNMPType;

/**
 * Allows to send a notify message to the specify target from the XML file
 * 
 * @author Alexander Kravin
 * 
 */
public class SNMPNotify {

	private static final Logger log = Logger.getLogger(SNMPNotify.class);

	private static SNMPNotify _instance = null;
	private SNMPObject snmpObject = null;

	/**
	 * A default constructor. Creates an object instance.
	 */
	public SNMPNotify() {
	}

	/**
	 * @param snmpObject
	 *            an <code>SNMPObject</code> instance.
	 */
	public void init(SNMPObject snmpObject) {
		this.snmpObject = snmpObject;
	}

	/**
	 * @return an <code>SNMPObject</code> instance.
	 */
	public SNMPObject getSNMPObject() {
		return snmpObject;

	}

	/**
	 * @return an instance object.
	 */
	public synchronized static SNMPNotify getInstance() {
		if (_instance == null)
			_instance = new SNMPNotify();
		return _instance;
	}

	/**
	 * Sends a notify message with a specified severity.
	 * 
	 * @param notifyType
	 *            an <code>EnumNotifyType</code> instance is a severity of
	 *            message.
	 * @param message
	 *            a string of the message, respectively.
	 */
	public void sendMessage(EnumNotifyType notifyType, String message) {

		// if snmpObject == NULL then initialize object by default
		if (snmpObject == null) {
			snmpObject = new SNMPv2c("127.0.0.1", 162, EnumSNMPTransport.UDP, new OctetString("public"), null);
		}

		// Connect to SNMP agent
		SynapSNMP synapsnmp = new SynapSNMP(snmpObject);
		try {
			synapsnmp.connect();
		} catch (IOException e) {
			log.warn("Can not send an inform message to the HP Open View. SNMPObject: " + snmpObject + ", Exception: "
			        + e);
			return;
		}
		synapsnmp.addVariable(SnmpConstants.sysUpTime, EnumSNMPType.TIMETICKS, new Long(0));
		synapsnmp.addVariable(SnmpConstants.snmpTrapOID, EnumSNMPType.OID, notifyType.getOID());
		synapsnmp.addVariable(notifyType.getOID(), EnumSNMPType.OCTETSTRING, message);
		try {
			synapsnmp.snmpNotify();
			synapsnmp.disconnect();
		} catch (IOException e) {
			log.warn("Can not send an inform message to the HP Open View. SNMPObject: " + snmpObject + ", Exception: "
			        + e);
		}
	}
}
