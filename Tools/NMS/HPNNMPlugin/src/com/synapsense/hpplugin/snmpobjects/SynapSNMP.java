/**
 * 
 */
package com.synapsense.hpplugin.snmpobjects;

import java.io.IOException;
import org.apache.log4j.Logger;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.UserTarget;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.event.ResponseListener;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityModel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.Gauge32;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.TableUtils;

import com.synapsense.hpplugin.configuration.XMLObject;
import com.synapsense.hpplugin.constants.EnumSNMPTransport;
import com.synapsense.hpplugin.constants.EnumSNMPType;
import com.synapsense.hpplugin.hpexception.HPErrorException;
import com.synapsense.hpplugin.synapobjects.SynapObject;

/**
 * Class allows to manage connection with the SNMP agent and sending GET or SET
 * commands.
 * 
 * @author Alexander Kravin
 * 
 */
public class SynapSNMP {

	private static final Logger log = Logger.getLogger(SynapSNMP.class);

	SNMPObject snmpObject;
	private Snmp snmp;
	private Target target;
	private TransportMapping transport;
	private ResponseListener listener = null;
	private PDU pdu;

	/**
	 * @param snmpObject
	 *            a <code>SNMPObject</code> instance.
	 */
	public SynapSNMP(SNMPObject snmpObject) {
		this.snmpObject = snmpObject;
	}

	/**
	 * Creates a default transport (UDP or TCP) and listen.
	 * 
	 * @throws IOException
	 *             If SNMP object has not been created.
	 */
	public void connect() throws IOException {

		// Set SNMP transport
		EnumSNMPTransport snmpTransport = snmpObject.getSNMPTransport();
		switch (snmpTransport) {
		case UDP:
			transport = new DefaultUdpTransportMapping();
			break;
		case TCP:
			transport = new DefaultTcpTransportMapping();
			break;
		}

		snmp = new Snmp(transport);

		createTarget(); // Create target

		// Create a new protocol data unit (PDU)
		if (snmpObject instanceof SNMPv2c) {
			pdu = new PDU();
		} else if (snmpObject instanceof SNMPv3) {
			UsmUser usmUser;
			SNMPv3 snmpv3 = (SNMPv3) snmpObject;
			USM usm = new USM(SecurityProtocols.getInstance(), new OctetString(MPv3.createLocalEngineID()), 0);
			SecurityModels.getInstance().addSecurityModel(usm);
			pdu = new ScopedPDU();
			((ScopedPDU) pdu).setContextName(snmpv3.getContextName());

			log.debug(((ScopedPDU) pdu).getContextName().toString());

			// Create a new user
			usmUser = new UsmUser(snmpv3.getSecurityName(), snmpv3.getAuthProtocol().getOID(),
			        snmpv3.getAuthPassword(), snmpv3.getPrivProtocol().getOID(), snmpv3.getPrivPassword());

			// Add user to User Security Model (USM)
			snmp.getUSM().addUser(snmpv3.getSecurityName(), usmUser);
		}

		snmp.listen();
		// transport.listen();

		if (log.isInfoEnabled()) {
			log.info("SNMP connection is success");
		}
	}

	/**
	 * Disconnects from the SNMP agent.
	 * 
	 * @throws IOException
	 *             If cannot close a SNMP session.
	 */
	public void disconnect() throws IOException {
		snmp.close();
		if (log.isInfoEnabled()) {
			log.info("SNMP disconnect");
		}
	}

	/**
	 * Creates a target, with default parameters
	 */
	private void createTarget() {
		Address targetAddress = null;

		// Set address
		EnumSNMPTransport snmpTransport = snmpObject.getSNMPTransport();
		if (snmpTransport == EnumSNMPTransport.TCP) {
			targetAddress = new TcpAddress(snmpObject.getAddress() + "/" + snmpObject.getPort());
		} else {
			targetAddress = new UdpAddress(snmpObject.getAddress() + "/" + snmpObject.getPort());
		}

		if (snmpObject instanceof SNMPv2c) {
			target = new CommunityTarget();
			target.setVersion(SnmpConstants.version2c);
		} else if (snmpObject instanceof SNMPv3) {
			SNMPv3 snmpv3 = (SNMPv3) snmpObject;
			target = new UserTarget();
			target.setVersion(SnmpConstants.version3);
			((UserTarget) target).setSecurityLevel(snmpv3.getSecurityLevel());
			((UserTarget) target).setSecurityName(snmpv3.getSecurityName());
			((UserTarget) target).setSecurityModel(SecurityModel.SECURITY_MODEL_USM);

		}

		target.setAddress(targetAddress);
		target.setRetries(snmpObject.getRetries());
		target.setTimeout(snmpObject.getTimeout());

		if (log.isInfoEnabled()) {
			log.info("The target was created");
		}
	}

	/**
	 * Fills every <code>SynapObject</code> from array list from
	 * <code>ObjectFiller</code>.
	 * 
	 * @param objectFiller
	 *            an <code>ObjectFiller</code> instance.
	 * @param lowerBoundIndex
	 *            an <code>org.snmp4j.smi.OID</code> instance.
	 * @param upperBoundIndex
	 *            an <code>OID</code> instance.
	 * @throws InterruptedException
	 *             if can't retrieve the data from the SNMP agent.
	 */
	public void fillTableObject(ObjectFiller objectFiller, OID lowerBoundIndex, OID upperBoundIndex)
	        throws InterruptedException {
		if (snmpObject instanceof SNMPv2c) {
			((CommunityTarget) target).setCommunity(((SNMPv2c) snmpObject).getCommunityPublic());
		}
		TableUtils tblUtils = new TableUtils(snmp, new SNMPPDUFactory(snmpObject));
		tblUtils.setMaxNumRowsPerPDU(10);

		long startTime = System.currentTimeMillis();

		Counter32 counter = new Counter32();
		synchronized (counter) {

			tblUtils.getTable(target, objectFiller.getXMLObject().getOIDs(), objectFiller, counter, lowerBoundIndex,
			        upperBoundIndex);

			counter.wait();
		}

		if (log.isDebugEnabled())
			log.debug("Table processing time: " + (System.currentTimeMillis() - startTime) + " milliseconds.");
	}

	/**
	 * Fills a <code>SynapObject</code> instance by specified
	 * <code>XMLObject</code> instance from SNMP agent.
	 * 
	 * @param xmlObject
	 *            a <code>XMLObject</code> instance.
	 * 
	 * @return a <code>SynapObject</code> instance.
	 * 
	 * @throws HPErrorException
	 *             if a remote host doesn't answer or a timeout occurred.
	 * @throws IOException
	 *             if the SNMP GET command cannot be sent.
	 * 
	 */
	public SynapObject fillSimpleObject(XMLObject xmlObject) throws IOException, HPErrorException {
		SynapObject synapObject = new SynapObject(xmlObject);

		for (int i = 0; i < synapObject.getDataSize(); i++) {
			clearPDU();
			String id = xmlObject.getArrayXMLProperty().get(i).getId();
			addVariable(xmlObject.getOIDById(id));
			String response = snmpGet();
			synapObject.setValue(i, response);
		}
		return synapObject;
	}

	/**
	 * Sets a listener for SET command.
	 * 
	 * @param listener
	 *            a <code>org.snmp4j.event.ResponseListener</code> instance.
	 */
	public void addSNMPListener(ResponseListener listener) {
		this.listener = listener;
	}

	/**
	 * Adds a new variable in <code>org.snmp4j.PDU</code> for GET commands.
	 * 
	 * @param oid
	 *            an <code>org.snmp4j.smi.OID</code> instance.
	 */
	public void addVariable(OID oid) {
		pdu.add(new VariableBinding(oid));
		if (log.isDebugEnabled()) {
			log.debug("Variable has been added: {OID=\"" + oid + "\"}");
		}
	}

	/**
	 * Adds a new variable in PDU.
	 * 
	 * @param oid
	 *            an <code>org.snmp4j.smi.OID</code> instance.
	 * @param type
	 *            an <code>EnumSNMPType</code> instance.
	 * @param value
	 *            one of the standard types.
	 */
	public void addVariable(OID oid, EnumSNMPType type, Object value) {
		VariableBinding var = null;
		switch (type) {
		case OID:
			var = new VariableBinding(oid, new OID(value.toString()));
			break;
		case INTEGER32:
			var = new VariableBinding(oid, new Integer32(Integer.parseInt(value.toString())));
			break;
		case GAUGE32:
			var = new VariableBinding(oid, new Gauge32(Long.parseLong(value.toString())));
			break;
		case OCTETSTRING:
			var = new VariableBinding(oid, new OctetString(value.toString()));
			break;
		case TIMETICKS:
			var = new VariableBinding(oid, new TimeTicks(Long.parseLong(value.toString())));
			break;
		}
		pdu.add(var);
		if (log.isDebugEnabled()) {
			log.debug("Variable has been added: {OID=\"" + oid + "\", type=\"" + type + "\", value=\"" + value + "\"}");
		}
	}

	/**
	 * Sends a SET command
	 * 
	 * @throws IOException
	 *             if the SNMP SET command cannot be sent
	 */
	public void snmpSet() throws IOException {
		if (snmpObject instanceof SNMPv2c) {
			OctetString communityPrivate = ((SNMPv2c) snmpObject).getCommunityPrivate();
			if (communityPrivate != null) {
				((CommunityTarget) target).setCommunity(communityPrivate);
			}
		}

		Boolean success = new Boolean(false);
		long startTime = System.currentTimeMillis();

		synchronized (success) {
			snmp.set(pdu, target, success, listener);
			try {
				success.wait();
			} catch (InterruptedException e) {
				log.warn("Cannot send SET command");
			}
		}

		if (log.isInfoEnabled())
			log.info("SET processing time: " + (System.currentTimeMillis() - startTime) + " milliseconds.");
	}

	/**
	 * Sends GET command
	 * 
	 * @return A value of GET command
	 * @throws IOException
	 *             if SET command cannot be sent
	 */
	public String snmpGet() throws IOException, HPErrorException {
		String str = "";
		if (snmpObject instanceof SNMPv2c) {
			((CommunityTarget) target).setCommunity(((SNMPv2c) snmpObject).getCommunityPublic());
		}

		ResponseEvent response = snmp.get(pdu, target);
		if (response == null) {
			throw new HPErrorException("Remote host doesn't answer");
		}
		if (response.getResponse() != null) {
			if (response.getResponse().getErrorStatusText().equalsIgnoreCase("Success")) {
				PDU pduresponse = response.getResponse();
				str = pduresponse.getVariableBindings().firstElement().toString();
				if (str.contains("=")) {
					int len = str.indexOf("=");
					str = str.substring(len + 1, str.length()).trim();
				}
			}
		} else {
			throw new HPErrorException("The answer is not received. A timeout occurred");
		}
		if (log.isDebugEnabled()) {
			log.debug("SNMP GET command: response=" + str);
		}

		return str;
	}

	/**
	 * Clears a <code>PDU</code> instance
	 */
	public void clearPDU() {
		pdu.getVariableBindings().clear();
	}

	/**
	 * Sends a notify message
	 * 
	 * @throws IOException
	 *             if the SNMP notify message cannot be sent
	 */
	public void snmpNotify() throws IOException {
		if (snmpObject instanceof SNMPv2c) {
			((CommunityTarget) target).setCommunity(((SNMPv2c) snmpObject).getCommunityPublic());
		}
		snmp.notify(pdu, target);
	}
}