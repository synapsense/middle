/**
 * 
 */
package com.synapsense.hpplugin.snmpobjects;

import org.apache.log4j.Logger;
import org.snmp4j.smi.OctetString;

import com.synapsense.hpplugin.constants.EnumHPField;
import com.synapsense.hpplugin.constants.EnumHPSymbol;
import com.synapsense.hpplugin.constants.EnumSNMPTransport;
import com.synapsense.hpplugin.hpobjects.HPObject;

/**
 * Class describes necessary parameters for SNMP v2c.
 * 
 * @author Alexander Kravin
 * 
 */
public class SNMPv2c extends SNMPObject {

	private static final Logger log = Logger.getLogger(SNMPv2c.class);

	private OctetString communityPublic = null;
	private OctetString communityPrivate = null;

	/**
	 * This constructor initializes a class by specified parameters.
	 * 
	 * @param address
	 *            SNMP agent's IP address.
	 * @param port
	 *            SNMP agent's port.
	 * @param snmpTransport
	 *            SNMP transport (UDP or TCP).
	 * @param communityPublic
	 *            a community string for GET commands.
	 * @param communityPrivate
	 *            a community string for SET commands.
	 */
	public SNMPv2c(String address, int port, EnumSNMPTransport snmpTransport, OctetString communityPublic,
	        OctetString communityPrivate) {
		super(address, port, snmpTransport);

		this.communityPublic = communityPublic;
		this.communityPrivate = communityPrivate;
	}

	/**
	 * @param hpObject
	 *            an <code>HPObject</code> instance must has a type of
	 *            <code>EnumHPSymbol.SYNAPPCSNMP2C</code>.
	 */
	public SNMPv2c(HPObject hpObject) {
		super(hpObject);
		if (hpObject.getHPSymbol() != EnumHPSymbol.SYNAPPCSNMP2C) {
			throw new IllegalArgumentException("SNMPv2c instance supports only");
		}

		String str;

		// Set public community string
		str = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_COMMUNITYPUBLIC);
		if (str != null)
			communityPublic = new OctetString(str);

		// Set private community string
		str = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_COMMUNITYPRIVATE);
		if (str != null)
			communityPrivate = new OctetString(str);

		// Set timeout and retries
		int timeout = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SNMPTIMEOUT));
		int retries = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SNMPRETRIES));
		setTimeoutAndRetries(timeout, retries);

		if (log.isDebugEnabled()) {
			log.debug("Object has been initialized " + this);
		}
	}

	/**
	 * @return a community string for GET commands
	 */
	public OctetString getCommunityPublic() {
		return communityPublic;
	}

	/**
	 * @return a community string for SET commands
	 */
	public OctetString getCommunityPrivate() {
		return communityPrivate;
	}

	@Override
	public String toString() {
		return super.toString() + "{communityPublic=\"" + communityPublic + "\", communityPrivate=\""
		        + communityPrivate + "\"}";
	}
}
