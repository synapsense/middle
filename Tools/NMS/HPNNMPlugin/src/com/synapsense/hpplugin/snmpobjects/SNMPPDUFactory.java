/**
 * 
 */
package com.synapsense.hpplugin.snmpobjects;

import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Target;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OctetString;
import org.snmp4j.util.DefaultPDUFactory;

/**
 * @author Alexander Kravin
 * 
 */
public class SNMPPDUFactory extends DefaultPDUFactory {

	private static OctetString contextName = null;
	private int pduType = PDU.GET;

	/**
	 * Creates a PDU factory for the {@link PDU#GET} PDU type.
	 */
	public SNMPPDUFactory(SNMPObject snmpObject) {
		if (snmpObject instanceof SNMPv3) {
			contextName = ((SNMPv3) snmpObject).getContextName();
		}
	}

	/**
	 * Creates a PDU factory for the specified PDU type.
	 * 
	 * @param pduType
	 *            a PDU type as specified by {@link PDU}.
	 */
	public SNMPPDUFactory(int pduType) {
		setPduType(pduType);
	}

	@Override
	public void setPduType(int pduType) {
		this.pduType = pduType;
	}

	@Override
	public int getPduType() {
		return pduType;
	}

	@Override
	public PDU createPDU(Target target) {
		return createPDU(target, pduType);
	}

	/**
	 * Create a <code>PDU</code> instance for the supplied target.
	 * 
	 * @param target
	 *            the <code>Target</code> where the PDU to be created will be
	 *            sent.
	 * @param pduType
	 *            a PDU type as specified by {@link PDU}.
	 * @return PDU a PDU instance that is compatible with the supplied target.
	 */
	public static PDU createPDU(Target target, int pduType) {
		PDU request = createPDU(target.getVersion());
		request.setType(pduType);
		return request;
	}

	/**
	 * Create a <code>PDU</code> instance by specified version.
	 * 
	 * @param targetVersion
	 *            a <code>SnmpConstants</code> version value.
	 * @return PDU a PDU instance that is compatible with specified version.
	 */
	public static PDU createPDU(int targetVersion) {
		PDU request = null;
		switch (targetVersion) {
		case SnmpConstants.version3: {
			request = new ScopedPDU();
			((ScopedPDU) request).setContextName(contextName);
			break;
		}
		case SnmpConstants.version1: {
			request = new PDUv1();
			break;
		}
		default:
			request = new PDU();
		}
		return request;
	}
}
