/**
 * 
 */
package com.synapsense.hpplugin.snmpobjects;

import com.synapsense.hpplugin.hpobjects.HPObject;

/**
 * @author Alexander Kravin
 * 
 */
public class SNMPFactory {

	public static SNMPObject getSNMPObject(HPObject hpObject) {
		switch (hpObject.getHPSymbol()) {
		case SYNAPPCSNMP3:
			return new SNMPv3(hpObject);
		case SYNAPPCSNMP2C:
			return new SNMPv2c(hpObject);
		default:
			throw new IllegalArgumentException("The instance only supports SYNAPPCSNMPv2c and SYNAPPCSNMPv3");
		}
	}
}
