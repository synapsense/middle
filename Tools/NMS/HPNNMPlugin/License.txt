Copyright � 2008 SynapSense Corporation. All Rights Reserved.

This software is licensed for use only in conjunction with SynapSense Products.
Do not install or use this software until you have read and agreed to the
software license.

You may not copy, modify, rent, sell, distribute or transfer any part of the
Software except as provided in the License Agreement, and you agree to prevent
unauthorized copying of the Software.

No rights or licenses are granted by SynapSense to You, expressly or by
implication, with respect to any proprietary information or patent, copyright,
trademark, trade secret, or other intellectual property right owned or
controlled by SynapSense, except as expressly provided in this Agreement.

Title to all copies of the Software remains with SynapSense or its suppliers.

The Software is copyrighted and protected by the laws of the United States and
other countries, and international treaty provisions. You may not remove any
copyright notices from the Software. SynapSense may make changes to the
Software, or to items referenced therein, at any time without notice, but is
not obligated to support or update the Software. Except as otherwise expressly
provided, SynapSense grants no express or implied right under SynapSense
patents, copyrights, trademarks, or other intellectual property rights.

EXCEPT AS PROVIDED ABOVE, THE SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY EXPRESS
OR IMPLIED WARRANTY OF ANY KIND INCLUDING WARRANTIES OF MERCHANTABILITY,
NONINFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. SynapSense does not
warrant or assume responsibility for the accuracy or completeness of any
information, text, graphics, links or other items contained within the Software.

IN NO EVENT SHALL SYNAPSENSE OR ITS SUPPLIERS BE LIABLE FOR ANY DAMAGES
WHATSOEVER (INCLUDING, WITHOUT LIMITATION, LOST PROFITS, BUSINESS INTERRUPTION,
OR LOST INFORMATION) ARISING OUT OF THE USE OF OR INABILITY TO USE THE
SOFTWARE, EVEN IF SYNAPSENSE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES. SOME JURISDICTIONS PROHIBIT EXCLUSION OR LIMITATION OF LIABILITY FOR
IMPLIED WARRANTIES OR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE
LIMITATION MAY NOT APPLY TO YOU. YOU MAY ALSO HAVE OTHER LEGAL RIGHTS THAT VARY
FROM JURISDICTION TO JURISDICTION.

Except as contained in this notice, the name of a copyright holder shall not be
used in advertising or otherwise to promote the sale, use or other dealings in
this Software without prior written authorization of the copyright holder.

