
Name "SynapSense HP NNM Plugin"

SetCompressor /SOLID lzma

# Variables
Var OV_INSTDIR
Var OV_VERSION
Var OV_DIR_BITMAPS
Var OV_DIR_FIELDS
Var OV_DIR_REG
Var OV_DIR_SYMBOLS
Var OV_DIR_TRAPD

Var EVENT_SUPPORT

Var CATEGORY_IN
Var ALIAS_IN
Var EVENT_IN
Var FIND_VALUE

# Defines
!define REGKEY "SOFTWARE\$(^Name)"
!define VERSION "$SYNAPVER$ - $SYNAPREV$"
!define COMPANY SynapSense
!define URL http://www.synapsense.com/

!define FALSE 0
!define TRUE 1
!define newLine $\r$\n
!define OV_SUBKEY "SOFTWARE\Hewlett-Packard\OpenView\Network Node Manager"

 
# MUI defines

!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_LICENSEPAGE_RADIOBUTTONS
!define MUI_STARTMENUPAGE_REGISTRY_ROOT HKLM
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_REGISTRY_KEY ${REGKEY}
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME StartMenuGroup
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "SynapSense HP NNM Plugin"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"
!define MUI_UNFINISHPAGE_NOAUTOCLOSE
!define MUI_LANGDLL_REGISTRY_ROOT HKLM
!define MUI_LANGDLL_REGISTRY_KEY ${REGKEY}
!define MUI_LANGDLL_REGISTRY_VALUENAME InstallerLanguage

# Included files
!include Sections.nsh
!include MUI.nsh
!include "WordFunc.nsh"
!include "LogicLib.nsh"

!insertmacro WordFind
!insertmacro un.WordFind
!insertmacro WordReplace

# Reserved Files
!insertmacro MUI_RESERVEFILE_LANGDLL
ReserveFile "${NSISDIR}\Plugins\AdvSplash.dll"

# Variables
Var StartMenuGroup

# Installer pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE License.txt
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuGroup
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

# Installer languages
!insertmacro MUI_LANGUAGE English
!insertmacro MUI_LANGUAGE Russian
!insertmacro MUI_LANGUAGE German
!insertmacro MUI_LANGUAGE French


# Installer attributes
OutFile setup.exe
InstallDir "$PROGRAMFILES\SynapHPNNMPlugin"
CRCCheck on
XPStyle on
ShowInstDetails show
InstallDirRegKey HKLM "${REGKEY}" Path
ShowUninstDetails show

# Installer sections
Section "!HP NNM Plugin" SEC0000
    SetOutPath $INSTDIR\conf
    SetOverwrite on
    File /r conf\*
    SetOutPath $INSTDIR\lib
    File /r lib\*
    SetOutPath $INSTDIR
    File HPNNMPlugin.jar

    # Install registration files
    SetOutPath $OV_DIR_BITMAPS
    File /r hpconfig\bitmaps\*
    SetOutPath $OV_DIR_FIELDS
    File /r hpconfig\fields\*
    SetOutPath $OV_DIR_REG
    File /r hpconfig\registration\*
    SetOutPath $OV_DIR_SYMBOLS
    File /r hpconfig\symbols\*
    
    WriteRegStr HKLM "${REGKEY}\Components" "HP NNM Plugin" 1
    
    WriteRegStr HKLM "${REGKEY}\HPConfig\Bitmaps" "Path" $OV_DIR_BITMAPS
    WriteRegStr HKLM "${REGKEY}\HPConfig\Fields" "Path" $OV_DIR_FIELDS
    WriteRegStr HKLM "${REGKEY}\HPConfig\Registration" "Path" $OV_DIR_REG
    WriteRegStr HKLM "${REGKEY}\HPConfig\Symbols" "Path" $OV_DIR_SYMBOLS
    WriteRegStr HKLM "${REGKEY}\HPConfig\trapd" "Path" $OV_DIR_TRAPD
SectionEnd


SectionGroup /e docs SECGRP0000

    Section "HP NNM plug-in user guide" SEC0001
        SetOutPath $INSTDIR
        SetOverwrite on
        File HPNNMPlugin_UG.doc
        WriteRegStr HKLM "${REGKEY}\Components" "HP NNM plug-in user guide" 1
    SectionEnd

    Section /o "HP NNM plug-in API" SEC0002
        SetOutPath $INSTDIR\doc
        SetOverwrite on
        File /r doc\*
        WriteRegStr HKLM "${REGKEY}\Components" "HP NNM plug-in API" 1
    SectionEnd

SectionGroupEnd

Section -post SEC0003
    WriteRegStr HKLM "${REGKEY}" Path $INSTDIR
    SetOutPath $INSTDIR
    WriteUninstaller $INSTDIR\uninstall.exe
    !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    SetOutPath $SMPROGRAMS\$StartMenuGroup
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\$(^UninstallLink).lnk" $INSTDIR\uninstall.exe
    !insertmacro MUI_STARTMENU_WRITE_END
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayName "$(^Name)"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" Publisher "${COMPANY}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" URLInfoAbout "${URL}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayIcon $INSTDIR\uninstall.exe
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" UninstallString $INSTDIR\uninstall.exe
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoModify 1
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoRepair 1
    
    # Change files
    Call changeLog4jProperties
    Call changeRegFile
    
    WriteRegStr HKLM "${REGKEY}" "EventSupport" $EVENT_SUPPORT
    ${If} $EVENT_SUPPORT = ${TRUE}
        Call instalEvents
    ${EndIf}
SectionEnd

# Macro for selecting uninstaller sections
!macro SELECT_UNSECTION SECTION_NAME UNSECTION_ID
    Push $R0
    ReadRegStr $R0 HKLM "${REGKEY}\Components" "${SECTION_NAME}"
    StrCmp $R0 1 0 next${UNSECTION_ID}
    !insertmacro SelectSection "${UNSECTION_ID}"
    GoTo done${UNSECTION_ID}
next${UNSECTION_ID}:
    !insertmacro UnselectSection "${UNSECTION_ID}"
done${UNSECTION_ID}:
    Pop $R0
!macroend

# Uninstaller sections
Section /o "-un.HP NNM plug-in API" UNSEC0002
    RmDir /r /REBOOTOK $INSTDIR\doc
    DeleteRegValue HKLM "${REGKEY}\Components" "HP NNM Plugin API"
SectionEnd

Section /o "-un.HP NNM plug-in user guide" UNSEC0001
    Delete /REBOOTOK $INSTDIR\HPNNMPlugin_UG.doc
    DeleteRegValue HKLM "${REGKEY}\Components" "HP NNM plug-in user guide"
SectionEnd

Section /o "-un.HP NNM Plugin" UNSEC0000
    Delete /REBOOTOK $INSTDIR\HPNNMPlugin.jar
    RmDir /r /REBOOTOK $INSTDIR\logs
    RmDir /r /REBOOTOK $INSTDIR\lib
    RmDir /r /REBOOTOK $INSTDIR\conf
    
    RmDir /r /REBOOTOK $OV_DIR_BITMAPS
    RmDir /r /REBOOTOK $OV_DIR_SYMBOLS
    
    Delete /REBOOTOK $OV_DIR_FIELDS\synap_fields
    Delete /REBOOTOK $OV_DIR_REG\synapmap
    
    DeleteRegValue HKLM "${REGKEY}\Components" "HP NNM Plugin"
SectionEnd

Section -un.post UNSEC0003
    DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)"
    Delete /REBOOTOK "$SMPROGRAMS\$StartMenuGroup\$(^UninstallLink).lnk"
    Delete /REBOOTOK $INSTDIR\uninstall.exe
    DeleteRegValue HKLM "${REGKEY}" StartMenuGroup
    DeleteRegValue HKLM "${REGKEY}" Path
    DeleteRegValue HKLM "${REGKEY}" EventSupport
    
    DeleteRegValue HKLM "${REGKEY}\HPConfig\Bitmaps" Path
    DeleteRegValue HKLM "${REGKEY}\HPConfig\Fields" Path
    DeleteRegValue HKLM "${REGKEY}\HPConfig\Registration" Path
    DeleteRegValue HKLM "${REGKEY}\HPConfig\Symbols" Path
    DeleteRegValue HKLM "${REGKEY}\HPConfig\trapd" Path
    DeleteRegKey HKLM "${REGKEY}\HPConfig"
    
    DeleteRegKey /IfEmpty HKLM "${REGKEY}\Components"
    DeleteRegKey /IfEmpty HKLM "${REGKEY}"
    RmDir /REBOOTOK $SMPROGRAMS\$StartMenuGroup
    RmDir /REBOOTOK $INSTDIR
    
    ${If} $EVENT_SUPPORT = ${TRUE}
        Call un.instalEvents
    ${EndIf}
SectionEnd

# Installer functions
Function .onInit
    InitPluginsDir
    Push $R1
    File /oname=$PLUGINSDIR\spltmp.bmp pics\logo.bmp
    advsplash::show 1200 600 400 -1 $PLUGINSDIR\spltmp
    Pop $R1
    Pop $R1
    !insertmacro MUI_LANGDLL_DISPLAY 
    
    # Support events
    StrCpy $EVENT_SUPPORT ${TRUE}
    
    # Try to find HP Open View
    ClearErrors
    
    ReadRegStr $OV_INSTDIR HKLM "${OV_SUBKEY}" PathName
    ReadRegStr $OV_VERSION HKLM "${OV_SUBKEY}" CurrentVersion
    IfErrors noInstalled
    
    ${If} $OV_VERSION != "7.50"
    MessageBox MB_YESNO|MB_ICONQUESTION "Plugin was tested on HP Open View of version 7.50. \
        You have HP Open View of version $OV_VERSION.${newLine}\
        You can continue the installation but the work of a plug-in can be not stable.${newLine}\
        Do you want to continue?" IDNO stop
    ${EndIf}
    
    StrCpy $OV_DIR_BITMAPS "$OV_INSTDIR\bitmaps\c\synapsense"
    StrCpy $OV_DIR_FIELDS "$OV_INSTDIR\fields\C"
    StrCpy $OV_DIR_REG "$OV_INSTDIR\registration\C"
    StrCpy $OV_DIR_SYMBOLS "$OV_INSTDIR\symbols\C\synapsense"
    StrCpy $OV_DIR_TRAPD "$OV_INSTDIR\conf\C"
    
    # Try to find a "tarapd.conf" file    
    IfFileExists "$OV_DIR_TRAPD\trapd.conf" end
    MessageBox MB_YESNO|MB_ICONQUESTION "Cannot find [$OV_DIR_TRAPD\trapd.conf] file. \  
           Support of events will not be installed.${newLine} \
           Do you want to continue?" IDNO stop
    StrCpy $EVENT_SUPPORT ${FALSE}        

        
noInstalled:
    MessageBox MB_OK|MB_ICONSTOP "Cannot find the HP Open View. \
                    Installing cannot be continued."
    Abort
stop:
    Abort
end:
FunctionEnd


# Uninstaller functions
Function un.onInit
    ReadRegStr $EVENT_SUPPORT HKLM "${REGKEY}" EventSupport
    
    ReadRegStr $OV_DIR_BITMAPS HKLM "${REGKEY}\HPConfig\Bitmaps" Path
    ReadRegStr $OV_DIR_FIELDS HKLM "${REGKEY}\HPConfig\Fields" Path
    ReadRegStr $OV_DIR_REG HKLM "${REGKEY}\HPConfig\Registration" Path
    ReadRegStr $OV_DIR_SYMBOLS HKLM "${REGKEY}\HPConfig\Symbols" Path
    ReadRegStr $OV_DIR_TRAPD HKLM "${REGKEY}\HPConfig\trapd" Path
    
    ReadRegStr $INSTDIR HKLM "${REGKEY}" Path
    
    !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuGroup
    !insertmacro MUI_UNGETLANGUAGE
    !insertmacro SELECT_UNSECTION "HP NNM plug-in API" ${UNSEC0002}
    !insertmacro SELECT_UNSECTION "HP NNM plug-in user guide" ${UNSEC0001}
    !insertmacro SELECT_UNSECTION "HP NNM Plugin" ${UNSEC0000}
FunctionEnd

Function changeLog4jProperties
    ClearErrors
    
    # Open file
    FileOpen $0 "$INSTDIR\lib\log4j.properties" r
    FileOpen $1 "$INSTDIR\lib\log4j.new" w
    IfErrors noChange
    
loop:
    FileRead $0 $2
    IfErrors end
    
    StrCpy $3 $2 23
    ${If} $3 == "log4j.appender.log.File"
        ${WordReplace} $INSTDIR "\" "/" "+" $4 
        FileWrite $1 'log4j.appender.log.File=$4/logs/SynapHPNNMPlugin.log${newLine}'
    ${Else}
        FileWrite $1 $2
    ${EndIf}
    Goto loop
    
end:
    FileClose $0
    FileClose $1
    
    Delete "$INSTDIR\lib\log4j.properties"
    Rename "$INSTDIR\lib\log4j.new" "$INSTDIR\lib\log4j.properties" 
    Return
                           
noChange:
    MessageBox MB_OK|MB_ICONSTOP "Cannot change [$INSTDIR\lib\log4j.properties] file.${newLine} \
                You don't have permissions."    
    Abort    
FunctionEnd

Function changeRegFile
    ClearErrors
    
    # Open files  
    FileOpen $0 "$OV_DIR_REG\synapmap" r
    FileOpen $1 "$OV_DIR_REG\new.synapmap" w
    IfErrors noChange
loop:    
    FileRead $0 $2
    IfErrors end
    
    StrCpy $3 $2 9
    ${If} $3 == "//Command"
        ${WordReplace} $INSTDIR "\" "/" "+" $4 
        FileWrite $1 'Command -Shared -Initial -Restart "java -jar \"$4/HPNNMPlugin.jar\" -session $$OVwSessionID";'
    ${Else}
        FileWrite $1 $2
    ${EndIf}
    Goto loop
end:
    FileClose $0
    FileClose $1
    
    Delete "$OV_DIR_REG\synapmap"
    Rename "$OV_DIR_REG\new.synapmap" "$OV_DIR_REG\synapmap" 
    Return                
noChange:
    MessageBox MB_OK|MB_ICONSTOP "Cannot change [$OV_DIR_REG\synapmap] file.${newLine} \
                You don't have permissions."    
    Abort
FunctionEnd

Function un.instalEvents
    ClearErrors

    # Open files  
    FileOpen $0 "$OV_DIR_TRAPD\trapd.conf" r
    FileOpen $1 "$OV_DIR_TRAPD\trapd.new" w
    IfErrors end
loop:
    FileRead $0 $2
    IfErrors end
   
    # Fing a category group
    StrCpy $3 $2 9
    StrCmpS $3 "CATEGORY " check
    
    # Find an alias group
    StrCpy $3 $2 10
    StrCmpS $3 "OID_ALIAS " check
    
    # Find an event group
    StrCpy $3 $2 6
    StrCmp $3 "EVENT " event write
        
check:

    ${un.WordFind} $2 "SynapPlugin" "+1}" $3
    StrCmp $3 $2 0 loop
    FileWrite $1 $2
    Goto loop

event:

    ${un.WordFind} $2 "SynapPlugin" "+1}" $3
    StrCmp $3 $2 0 findValue
    FileWrite $1 $2
    Goto loop
    
findValue:
    
    StrCpy $4 $2 5
    ${While} $4 S!= "EDESC"
        FileRead $0 $2
        IfErrors end
        StrCpy $4 $2 5
    ${EndWhile}
    
    FileRead $0 $2
    IfErrors end
 
    FileRead $0 $2
    IfErrors end
 
    FileRead $0 $2
    IfErrors end
 
    Goto loop
write:
    FileWrite $1 $2
    Goto loop
end:
    FileClose $1
    FileClose $0
        
    Delete "$OV_DIR_TRAPD\trapd.conf"
    Rename "$OV_DIR_TRAPD\trapd.new" "$OV_DIR_TRAPD\trapd.conf"
 
FunctionEnd

Function instalEvents
    ClearErrors
    # Init variables
    StrCpy $CATEGORY_IN ${FALSE}
    StrCpy $ALIAS_IN ${FALSE}
    StrCpy $EVENT_IN ${FALSE}
    StrCpy $FIND_VALUE ${FALSE}
    
    #Counter for categories
    StrCpy $R0 0 
    
    # Open files  
    FileOpen $0 "$OV_DIR_TRAPD\trapd.conf" r
    FileOpen $1 "$OV_DIR_TRAPD\trapd.new" w
loop:
    FileRead $0 $2
    IfErrors end
   
    # Fing a category group
    StrCpy $3 $2 9
    StrCmpS $3 "CATEGORY " category
    
    # Find an alias group
    StrCpy $3 $2 10
    StrCmpS $3 "OID_ALIAS " alias
    
    # Find an event group
    StrCpy $3 $2 6
    StrCmp $3 "EVENT " event write
        
    #Goto loop 
category:
    StrCpy $CATEGORY_IN ${TRUE}

    IntOp $R0 $R0 + 1
    ${WordFind} $2 "SynapPlugin" "+1}" $3
    StrCmp $3 $2 0 findValue
    FileWrite $1 $2
    Goto loop
    
alias:
    StrCpy $ALIAS_IN ${TRUE}
    
    ${WordFind} $2 "SynapPlugin" "+1}" $3
    StrCmp $3 $2 0 findValue
    FileWrite $1 $2
    
    Goto loop
    
event:
    StrCpy $EVENT_IN ${TRUE}
    
    ${WordFind} $2 "SynapPlugin" "+1}" $3
    StrCmp $3 $2 0 findValue
    
    StrCpy $4 $2 5
    ${While} $4 S!= "EDESC"
        FileWrite $1 $2

        FileRead $0 $2
        IfErrors end
        
        StrCpy $4 $2 5
    ${EndWhile}
    
    FileWrite $1 $2
    
    FileRead $0 $2
    IfErrors end
    FileWrite $1 $2
    
    FileRead $0 $2
    IfErrors end
    FileWrite $1 $2
    
    FileRead $0 $2
    IfErrors end
    FileWrite $1 $2
    
    Goto loop
    
findValue:
    StrCpy $FIND_VALUE ${TRUE}
    FileWrite $1 $2
    Goto loop
write:
    ${If} $CATEGORY_IN = ${TRUE}
        StrCpy $CATEGORY_IN ${FALSE}
        ${If} $FIND_VALUE = ${FALSE}
            FileWrite $1 'CATEGORY $R0 "SynapSense Alarms" "SynapPlugin Alarms"${newLine}'
            StrCpy $R0 0                    
        ${EndIf}
        StrCpy $FIND_VALUE ${FALSE}
    ${ElseIf} $ALIAS_IN = ${TRUE}
        StrCpy $ALIAS_IN ${FALSE}
        ${If} $FIND_VALUE = ${FALSE}
            FileWrite $1 'OID_ALIAS SynapPlugin .1.3.6.1.4.1.29078.1${newLine}'
        ${EndIf}
        StrCpy $FIND_VALUE ${FALSE}
    ${EndIf}
    FileWrite $1 $2
    Goto loop
end:
    ${If} $EVENT_IN = ${TRUE}
        StrCpy $EVENT_IN ${FALSE}
        ${If} $FIND_VALUE = ${FALSE}
            # Info
            FileWrite $1 'EVENT SynapPluginInfo .1.3.6.1.4.1.29078.1.0.40000000 "SynapSense Alarms" Normal${newLine}'
            FileWrite $1 'FORMAT $$1${newLine}'
            FileWrite $1 'SDESC${newLine}'
            FileWrite $1 'SynapPlugin info message${newLine}'
            FileWrite $1 'EDESC${newLine}'
            FileWrite $1 '#${newLine}'
            FileWrite $1 '#${newLine}'
            FileWrite $1 '#${newLine}'
            
            # Warn
            FileWrite $1 'EVENT SynapPluginWarn .1.3.6.1.4.1.29078.1.0.40000001 "SynapSense Alarms" Minor${newLine}'
            FileWrite $1 'FORMAT $$1${newLine}'
            FileWrite $1 'SDESC${newLine}'
            FileWrite $1 'SynapPlugin warning message${newLine}'
            FileWrite $1 'EDESC${newLine}'
            FileWrite $1 '#${newLine}'
            FileWrite $1 '#${newLine}'
            FileWrite $1 '#${newLine}'
            
            # Error
            FileWrite $1 'EVENT SynapPluginError .1.3.6.1.4.1.29078.1.0.40000002 "SynapSense Alarms" Critical${newLine}'
            FileWrite $1 'FORMAT $$1${newLine}'
            FileWrite $1 'SDESC${newLine}'
            FileWrite $1 'SynapPlugin error message${newLine}'
            FileWrite $1 'EDESC${newLine}'
            FileWrite $1 '#${newLine}'
            FileWrite $1 '#${newLine}'
            FileWrite $1 '#${newLine}'
        ${EndIf}
        StrCpy $FIND_VALUE ${FALSE}
    ${EndIf}
    FileClose $1
    FileClose $0
        
    Delete "$OV_DIR_TRAPD\trapd.conf"
    Rename "$OV_DIR_TRAPD\trapd.new" "$OV_DIR_TRAPD\trapd.conf"

FunctionEnd


# Section Descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
!insertmacro MUI_DESCRIPTION_TEXT ${SEC0000} $(SEC0000_DESC)
!insertmacro MUI_DESCRIPTION_TEXT ${SEC0001} $(SEC0001_DESC)
!insertmacro MUI_DESCRIPTION_TEXT ${SEC0002} $(SEC0002_DESC)
!insertmacro MUI_DESCRIPTION_TEXT ${SECGRP0000} $(SECGRP0000_DESC)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

# Installer Language Strings
# TODO Update the Language Strings with the appropriate translations.

LangString ^UninstallLink ${LANG_ENGLISH} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_RUSSIAN} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_GERMAN} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_FRENCH} "Uninstall $(^Name)"

LangString SEC0000_DESC ${LANG_ENGLISH} "SynapSense HP Network Node Manager Plug-in"
LangString SECGRP0000_DESC ${LANG_ENGLISH} Documents
LangString SEC0001_DESC ${LANG_ENGLISH} "Instruction about installation and using SynapSense HP Network Node Manager Plug-in"
LangString SEC0002_DESC ${LANG_ENGLISH} "Description of API"

LangString SEC0000_DESC ${LANG_RUSSIAN} "SynapSense HP Network Node Manager ������"
LangString SECGRP0000_DESC ${LANG_RUSSIAN} ���������
LangString SEC0001_DESC ${LANG_RUSSIAN} "���������� �� ��������� � ������������� SynapSense HP Network Node Manager �������"
LangString SEC0002_DESC ${LANG_RUSSIAN} "�������� API"

LangString SEC0000_DESC ${LANG_GERMAN} "SynapSense HP Network Node Manager Plug-in"
LangString SECGRP0000_DESC ${LANG_GERMAN} Documents
LangString SEC0001_DESC ${LANG_GERMAN} "Instruction about installation and using SynapSense HP Network Node Manager Plug-in"
LangString SEC0002_DESC ${LANG_GERMAN} "Description of API"

LangString SEC0000_DESC ${LANG_FRENCH} "SynapSense HP Network Node Manager Plug-in"
LangString SECGRP0000_DESC ${LANG_FRENCH} Documents
LangString SEC0001_DESC ${LANG_FRENCH} "Instruction about installation and using SynapSense HP Network Node Manager Plug-in"
LangString SEC0002_DESC ${LANG_FRENCH} "Description of API"
