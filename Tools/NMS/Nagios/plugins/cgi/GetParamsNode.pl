#!/usr/bin/perl

use strict;
#use CGI;

my $p1;
$|=1;
main();
exit 0;

sub main
{
    my (@nvp,$nv);
    my ($namep,$valuep);
    my $NetOID="1.3.6.1.4.1.29078.1.1.1.1";
    my $NodeOID="1.3.6.1.4.1.29078.1.1.3.1.1";
    my $SensorOID="1.3.6.1.4.1.29078.1.1.3.3.1.1";
    my $NagCGIP;
    my $NagWWWP;
    my $NagPLGP;
    my (@ss,$lss,$fh);
    my ($HR);
    my $AgHost;
    my $Net=1;
    my $Node=1;
    my ($GP);
    my (@pt0,@pt1,@pt2);
    my ($i);
    my ($INodeName,$ILocation,$IDescription,$Isi,$Ilocx,$Ilocy);
    print "Content-type:text/html\n\n";
    print "<HTML><BODY>";
    @nvp=split/&/,$ENV{QUERY_STRING};
    for($i=0;$i<12;++$i) {
	($namep,$valuep)=split/=/,$nvp[$i];
	if($namep eq "AgHost") { $AgHost=$valuep; }
	if($namep eq "Net") { $Net=$valuep; }
	if($namep eq "Node") { $Node=$valuep; }
	if($namep eq "ngcgi") { $NagCGIP="$valuep"; }
	if($namep eq "ngwww") { $NagWWWP="$valuep/notes"; }
	if($namep eq "ngplg") { $NagPLGP="$valuep"; }
	if($namep eq "nodename") { $INodeName="$valuep"; }
	if($namep eq "location") { $ILocation="$valuep"; }
	if($namep eq "description") { $IDescription="$valuep"; }
	if($namep eq "si") { $Isi="$valuep"; }
	if($namep eq "locatinonx") { $Ilocx="$valuep"; }
	if($namep eq "locationy") { $Ilocy="$valuep"; }
    }
#------------------------
    if($INodeName ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NodeOID.4.$Node.$Net s $INodeName`;
    }
    if($ILocation ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NodeOID.5.$Node.$Net s $ILocation`;
    }
    if($IDescription ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NodeOID.6.$Node.$Net s $IDescription`;
    }
    if($Isi ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NodeOID.15.$Node.$Net i $Isi`;
    }
    if($Ilocx ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NodeOID.18.$Node.$Net s $Ilocx`;
    }
    if($Ilocy ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NodeOID.19.$Node.$Net s $Ilocy`;
    }
#------------------------    
    print "<BR>Node parameters:<BR>";
    print "<FORM method=\"GET\" action=\"/nagios/cgi-bin/GetParamsNode.pl\">";
    print "<TABLE border=1 bgcolor=\"gray\" >";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.4.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>Name:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=nodename></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.5.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>Location:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=location></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.6.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>Description:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=description></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.9.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>Radio:</TD><TD>$pt1[1]</TD</TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.10.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>RAM:</TD><TD>$pt1[1]</TD</TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.11.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>ROM:</TD><TD>$pt1[1]</TD</TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.15.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>SamplingInterval:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=si></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.16.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>SensorCount:</TD><TD>$pt1[1]</TD</TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.17.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>AlertCount:</TD><TD>$pt1[1]</TD</TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.18.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>LocationX:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=locationx></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.19.$Node.$Net`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>LocationY:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=locationy></TD></TR>";
    print "</TABLE>";
    print "<INPUT type=submit name=go>";
    print "</FORM>";
    print "<BR><a href=/nagios/cgi-bin/GetAlertsDefsNode.pl?Aghost=$AgHost&Node=$Node&Net=$Net>AlertDefinitions</a><BR>";
    print "<BR><a href=/nagios/cgi-bin/GetTraps.pl?Aghost=$AgHost&t=d&Sensor=0&Node=$Node&Net=$Net&ngwww=$NagWWWP>Alerts</a><BR>";
    print "<BR>List of sensors:<BR>";
    print "<TABLE border=\"1\" bgcolor=\"gray\">";
open(fh,"<$NagWWWP/s.lst");
while($lss=<fh>) {
$lss=<fh>;
 @pt2=split/: /,$lss;
 if($pt2[0]==$Node) {
   @ss=split/ /,$pt2[1];
  foreach $lss(@ss) {
	@pt2=split/\n/,$lss;
	$i=$pt2[0];
	$GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.4.$i.$Node`;
	@pt0=split/=/,$GP;
	@pt1=split/:/,$pt0[1];
	print "<TR>";
	$HR=`cat $NagWWWP/$AgHost.$i.$Node.$Net`;
#	print "<TD>$HR</TD>";
	print "<TD><a href=\"$HR\">SensorEntry $pt1[1]</a></TD>";
	print "</TR>";
  }
 }
}
close(fh);
    print "</TABLE>";
### Triggered alerts!!!
#    print "<BR>ALERTS:<BR>";
#    print "<TABLE border=\"1\" bgcolor=\"gray\">";
#    print `$NagCGIP/GetTraps.pl $AgHost /tmp/snmptrap.log`;
#    print "</TABLE>";
    print "</BODY></HTML>";
}


