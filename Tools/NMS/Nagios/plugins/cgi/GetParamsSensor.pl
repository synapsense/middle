#!/usr/bin/perl

use strict;
#use CGI;

my $p1;
$|=1;
main();
exit 0;

sub main
{
    my (@nvp,$nv);
    my ($namep,$valuep);
    my $NetOID="1.3.6.1.4.1.29078.1.1.1.1";
    my $NodeOID="1.3.6.1.4.1.29078.1.1.3.1.1";
    my $SensorOID="1.3.6.1.4.1.29078.1.1.3.3.1.1";
    my $LastValOID="1.3.6.1.4.1.29078.1.1.3.3.3.1.1";
    my $NagCGIP;
    my $NagPLGP;
    my $NagWWWP;
    my ($HR);
    my $AgHost;
    my $Net=1;
    my $Node=1;
    my $Sensor=1;
    my ($GP);
    my (@pt0,@pt1);
    my ($i);
    my ($ISensorName,$IRemin,$IRemax,$IAlmin,$IAlmax,$IDmin,$IDmax);
    print "Content-type:text/html\n\n";
    print "<HTML><BODY>";
    @nvp=split/&/,$ENV{QUERY_STRING};
    for($i=0;$i<14;++$i) {
	($namep,$valuep)=split/=/,$nvp[$i];
	if($namep eq "AgHost") { $AgHost=$valuep; }
	if($namep eq "Net") { $Net=$valuep; }
	if($namep eq "Node") { $Node=$valuep; }
	if($namep eq "Sensor") { $Sensor=$valuep; }
	if($namep eq "ngcgi") { $NagCGIP="$valuep"; }
	if($namep eq "ngwww") { $NagWWWP="$valuep/notes"; }
	if($namep eq "ngplg") { $NagPLGP="$valuep"; }
	if($namep eq "sname") { $ISensorName=$valuep; }
	if($namep eq "remin") { $IRemin=$valuep; }
	if($namep eq "remax") { $IRemax=$valuep; }
	if($namep eq "almin") { $IAlmin=$valuep; }
	if($namep eq "almax") { $IAlmax="$valuep"; }
	if($namep eq "dmin") { $IDmin="$valuep/notes"; }
	if($namep eq "dmax") { $IDmax="$valuep"; }
    }
#------------------------
    if($ISensorName ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $SensorOID.5.$Sensor.$Node s $ISensorName`;
    }
    if($IRemin ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $SensorOID.7.$Sensor.$Node s $IRemin`;
    }
    if($IRemax ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $SensorOID.8.$Sensor.$Node s $IRemax`;
    }
    if($IAlmin ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $SensorOID.9.$Sensor.$Node s $IAlmin`;
    }
    if($IAlmax ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $SensorOID.10.$Sensor.$Node s $IAlmax`;
    }
    if($IDmin ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $SensorOID.11.$Sensor.$Node s $IDmin`;
    }
    if($IDmax ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $SensorOID.12.$Sensor.$Node s $IDmax`;
    }
#------------------------    
    print "<BR>Sensor parameters(AgHost=$AgHost,Sensor=$Sensor,Node=$Node):<BR>";
    print "<FORM method=\"GET\" action=\"/nagios/cgi-bin/GetParamsSensor.pl\">";
    print "<TABLE border=1 bgcolor=\"gray\" >";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.3.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>SensorNodeChannel:</TD><TD>$pt1[1]</TD</TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.5.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>Name:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=sname></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.7.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>RecomendedMin:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=remin></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.8.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>RecomendedMax:</TD><TD>$pt1[1]</TD<TD><INPUT type=text name=remax></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.9.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>AllowableMin:</TD><TD>$pt1[1]</TD><TD><INPUT type=text name=almin></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.10.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>AllowableMax:</TD><TD>$pt1[1]</TD><TD><INPUT type=text name=almax></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.11.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>DisplayMin:</TD><TD>$pt1[1]</TD><TD><INPUT type=text name=dmin></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.12.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>DisplayMax:</TD><TD>$pt1[1]</TD><TD><INPUT type=text name=dmax></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.13.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>LastValue:</TD><TD>$pt1[1]</TD</TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.14.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/: /,$pt0[1];
    print "<TR><TD>LastValueTimestamp:</TD><TD>$pt1[1]</TD</TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $SensorOID.15.$Sensor.$Node`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>SensorAlertsCount:</TD><TD>$pt1[1]</TD</TR>";
    print "</TABLE>";
    print "<INPUT type=submit name=go>";
    print "</FORM><BR>";
   print  "<a href=/nagios/cgi-bin/GetLastValues.pl?AgHost=$AgHost&Sensor=$Sensor&Node=$Node&Net=$Net&ngcgi=$NagCGIP&ngplg=$NagPLGP&ngwww=$NagWWWP> Last values </a></font>";
    print "<BR><a href=/nagios/cgi-bin/GetTraps.pl?AgHost=$AgHost&t=n&Sensor=$Sensor&Node=$Node&Net=$Net&ngwww=$NagWWWP>Alerts</a><BR>";
#echo "<BR>ALERTS:<BR>" >> $FNH;
#print "<TABLE border=\"1\" bgcolor=\"gray\">" >> $FNH;
#print `cat $NF/$1.0` >> $FNH;
#print "</TABLE>";
    print "</BODY></HTML>";
}


