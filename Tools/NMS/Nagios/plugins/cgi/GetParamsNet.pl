#!/usr/bin/perl

use strict;
#use CGI;

my $p1;
$|=1;
main();
exit 0;

sub main
{
    my (@nvp,$nv);
    my ($namep,$valuep);
    my $NetOID="1.3.6.1.4.1.29078.1.1.1.1";
    my $NodeOID="1.3.6.1.4.1.29078.1.1.3.1.1";
    my $NagCGIP;
    my $NagPLGP;
    my $NagWWWP;
    my ($HR);
    my ($AgHost,$lss,$fh);
    my $Net=1;
    my ($GP);
    my (@pt0,@pt1,@pt2,@ss);
    my ($i);
    my $AL;
    my ($IName,$IDesc,$IHN,$IHP,$IHI);
    print "Content-type:text/html\n\n";
    print "<HTML><BODY>";
    @nvp=split/&/,$ENV{QUERY_STRING};
    for($i=0;$i<10;++$i) {
	($namep,$valuep)=split/=/,$nvp[$i];
	if($namep eq "AgHost") { $AgHost=$valuep; }
	if($namep eq "Net") { $Net=$valuep; }
	if($namep eq "ngcgi") { $NagCGIP="$valuep"; }
	if($namep eq "ngwww") { $NagWWWP="$valuep/notes"; }
	if($namep eq "ngwww") { $NagPLGP="$valuep"; }
	if($namep eq "nname") { $IName=$valuep; }
	if($namep eq "desc") { $IDesc=$valuep; }
	if($namep eq "hn") { $IHN="$valuep"; }
	if($namep eq "hp") { $IHP="$valuep/notes"; }
	if($namep eq "si") { $ISI="$valuep"; }
    }
#------------------------
    if($IName ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NetOID.2.$Node.$Net s $IName`;
    }
    if($IDesc ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NetOID.3.$Node.$Net s $IDesc`;
    }
    if($IHN ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NetOID.4.$Node.$Net s $IHN`;
    }
    if($IHP ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NetOID.5.$Node.$Net i $IHP`;
    }
    if($ISI ne "") {
	#Set
	#print "SET<BR>";
	$GP=`snmpset -t 5 -v 2c -c public $AgHost $NetOID.7.$Node.$Net i $ISI`;
    }
#------------------------    
    print "<BR>Net parameters:<BR>";
    print "<FORM method=\"GET\" action=\"/nagios/cgi-bin/GetParamsNet.pl\">";
    print "<TABLE border=1 bgcolor=\"gray\" >";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NetOID.2.1`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>NetworkName:</TD><TD>\"$pt1[1]\"</TD><TD><INPUT type=text name=nname></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NetOID.3.1`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>NetworkDescription:</TD><TD>\"$pt1[1]\"</TD><TD><INPUT type=text name=desc></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NetOID.4.1`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>NetworkHostname:</TD><TD>\"$pt1[1]\"</TD><TD><INPUT type=text name=hn></TD></TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NetOID.5.1`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>NetworkHostport:</TD><TD>\"$pt1[1]\"</TD><TD><INPUT type=text name=hp></TD><TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NetOID.6.1`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>NetworkChannel:</TD><TD>\"$pt1[1]\"</TD><TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NetOID.7.1`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>DefaultSampleInterval:</TD><TD>\"$pt1[1]\"</TD><TD><INPUT type=text name=si></TD><TR>";
    $GP=`snmpget -t 5 -v 2c -c public $AgHost $NetOID.8.1`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "<TR><TD>NetworkAlertCount:</TD><TD>\"$pt1[1]\"</TD><TR>";
    print "</TABLE>";
    print "<INPUT type=submit name=go>";
    print "</FORM>";

    $GP=`snmpget -t 5 -v 2c -c public $AgHost 1.3.6.1.4.1.29078.1.3.1.2`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "SynapStoreStatus $pt1[1];<BR>";

    $GP=`snmpget -t 5 -v 2c -c public $AgHost 1.3.6.1.4.1.29078.1.3.2.2`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "SQLServerStatus $pt1[1];<BR>";
    
    $GP=`snmpget -t 5 -v 2c -c public $AgHost 1.3.6.1.4.1.29078.1.3.3.2`;
    @pt0=split/=/,$GP;
    @pt1=split/:/,$pt0[1];
    print "WEBServerStatus $pt1[1];<BR>";
    
    print "<BR><a href=/nagios/cgi-bin/GetAlertsConds.pl?AgHost=$AgHost>Alert conditions</a><BR>";
    print "<BR><a href=/nagios/cgi-bin/GetTraps.pl?AgHost=$AgHost&t=t&Sensor=0&Node=0&Net=$Net&ngwww=$NagWWWP>Alerts</a><BR>";
    print "<BR><a href=/nagios/cgi-bin/GetRecipients.pl?AgHost=$AgHost>Recipients</a><BR>";
    print "<BR>List of nodes:<BR>";
    print "<TABLE border=\"1\" bgcolor=\"gray\">";
open(fh,"<$NagWWWP/n.lst");
while($lss=<fh>) {
$lss=<fh>;
 @pt2=split/: /,$lss;
 if($pt2[0]==$Net) {
   @ss=split/ /,$pt2[1];
  foreach $lss(@ss) {
	@pt2=split/\n/,$lss;
	$i=$pt2[0];
#	$i=$lss;
	$GP=`snmpget -t 5 -v 2c -c public $AgHost $NodeOID.4.$i.$Net`;
	@pt0=split/=/,$GP;
	@pt1=split/:/,$pt0[1];
	print "<TR>";
#	print "<TD><a href=\"http://$AgHost/cgi-bin/GetParamsNode.pl?AgHost=$AgHost&Node=$i&Net=$Net\">NodeEntry $pt1[1]</a></TD>";
	$HR=`cat $NagWWWP/$AgHost.$i.$Net`;
	print "<TD><a href=\"$HR\">NodeEntry $pt1[1]</a></TD>";
	print "</TR>";
}
}
}
close(fh);
    print "</TABLE>";
### Triggered Alerts!!!
#    print "<BR>List of notifications:<BR>";
#    print "<TABLE border=\"1\" bgcolor=\"gray\">";
#    $AL=`$NagCGIP/GetTraps.pl $AgHost /tmp/snmptrap.log 1`;
#    print "$AL";
#    print "</TABLE>";
    print "</BODY></HTML>";
}


