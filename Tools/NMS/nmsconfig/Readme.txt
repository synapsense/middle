              NMS Configurator User Manual

1. Basic concepts
    NMS Configurator is a tool for automatic building of NMS configuration files.
    Currently, the only supported NMS is Nagios.
    NMS Configurator reads SynapSense configuration from specified SNMP Agent
    and serializes it into config files. 

1.1 SynapSense network configuration objects
    SynapSense configuration can be represented by the following object tree:
        Configuration
            Networks
                Nodes
                    Sensors
    Each object in the tree has several attributes associated with it:
    Configuration:
        - Nagios server host name and port;
        - SNMP Agent server host name and port.
    Network:
        - ID;
        - Name;
        - Alias;
        - Host;
    Node:
        - ID;
        - Name;
        - Alias;
    Sensor:
        - ID;
        - Name;
        - Alias;
1.2 Serialization
    Network configuration objects are stored via templates.
    Template represents an arbitrary text file with with predefined macros.
    Macros are described in the Appendix A. When serialized, macros are
    expanded with corresponding values and templates are appended to the
    output files. Configuration of output files and templates used for
    each output file is described in xml configuration file.
     
2. Configuration file
    The configuration of serialization is stored in an xml file.
    The format of this file is as follows:
    
        <?xml version="1.0" encoding="UTF-8"?>
        <serializers templatesBase="<templates folder path>"
                outputBase="<output folder path>">
            <serializer out="<output file name or path/name>">
                <confBegin template="<template file name or path/name>"/>
                <netBegin template="<template file name or path/name>"/>
                <nodeBegin template="<template file name or path/name>"/>
                <sensorBegin template="<template file name or path/name>"/>
                <confEnd template="<template file name or path/name>"/>
                <netEnd template="<template file name or path/name>"/>
                <nodeEnd template="<template file name or path/name>"/>
                <sensorEnd template="<template file name or path/name>"/>
            </serializer>
            <serializer>
            ...
            </serializer>
        </serializers>
        
    Root element must be called "serializers" and may optionally contain
    two attributes: templatesBase (path to the templates folder) and
    outputBase (path to the folder where output files will be stored).
    
    Subsequent element must be called "serializer". Its mandatory attribute
    denotes a file name (or a path/file name, more on this below) that will
    store the result of this serializer. Note: An arbitrary number of
    serializers can be configured and used.
    
    Under each serializer element there go the descriptions of templates
    to be used for each serialization event type. Event element names are predefined:
    confBegin, netBegin , nodeBegin, sensorBegin, confEnd, netEnd,
    nodeEnd, sensorEnd. For each event, the template attribute is used
    to denote a file name (or a path/file name, more on this below) of
    the template to be used for this event. Each event type must be listed
    only one time or be not listed at all. Missed event description
    assumes that nothing will be serialized for that specific event.
    
    Note on paths: if templateBase and/or outputBase paths are specified
    then other paths are considered to be relative. Otherwise, all paths
    are taken on their own: relatives ones are considered as relative to
    the current folder, absolutes as absolutes. An example:
    
    <serializers templatesBase="c:\templ">
        <serializer out="serv.cfg">
            <confBegin template="serv/confBegin.txt"/>
            ...
    - means that c:\templ\serv\confBegin.txt will be used as a template
    for "configuration serialization begin" event handler; serialization
    result will be stored in serv.cfg file in the current folder.
    
    Note that templateBase and outputBase paths can be overridden by
    the command-line arguments. 
     
3. Command line parameters

Usage: configurator [options] nagios-address[:port] agent-address[:port]

  -?                Print this help and exit.
  -h[elp]           Print this help and exit.
  -t=templatesBase  Full or relative path to the nagios configuration
                    template files.
                    If specified, overwrites value of the templates_base
                    attribute provided in the config-file.
  -o=outputBase     Full or relative path of the folder to save
                    the resulting configuration files. If not
                    specified, the default "output" folder will be
                    created in the current directory.
  -c=configFile     Full or relative path and name of the serializers
                    configuration file. If not specified,
                    the default (installed) file will be used.
  nagios-address    DNS name or IP-address of the server with
                    Nagios manager running on.
  agent-address     DNS name or IP-address of the server with
                    Synapse SNMP agent running on.
  port              Port that the Nagios/agent is running on.
                    Defaults are 80 for Nagios and 161 for agent.

Example: the bare minimum launch
    configurator nagios.myorg.com:8080 synapserv.myorg.com
    - reads configuration from agent running on synapserv.myorg.com, port 161
      and serializes it into the current folder under "output" directory.
      Default configuration file and templates are used. Configuration
      will run on the Nagios installed under Apache server running on
      the host nagios.myorg.com, port 8080.


APPENDIX A: Macro names
    The macro names and their substitution meaning are described below.

A.1 Configuration macros
    $NAGIOS_HOST - name of the Nagios host. Substituted by the nagios-address passed in the command line.
    $NAGIOS_PORT - Nagios port. Substituted by the nagios port passed in the command line.
    $AGENT_HOST - name of the SynapSense SNMP Agent host. Substituted by the agent-address passed in the command line.
    $AGENT_PORT - SNMP Agent port. Substituted by the agent port passed in the command line.

A.2 Network macros
    $NET_ID - an id of the network being saved.
    $NET_NAME - a name of the network being saved.
    $NET_N_NAME - a normalized name of the network being saved.
                     (spaces are replaced with underscores).
    $NET_ALIAS - an alias of the network being saved.
    $NET_HOST - a host name of the network being saved (value read from agent).

A.3 Node macros
    $NODE_ID - an id of the node being saved.
    $NODE_NAME - a name of the node being saved.
    $NODE_N_NAME - a normalized name of the node being saved
                 (spaces are replaced with underscores).
    $NODE_ALIAS - an alias of the node being saved.

A.3 Sensor macros
    $SENSOR_ID - an id of the sensor being saved.
    $SENSOR_NAME - a name of the sensor being saved.
    $SENSOR_N_NAME - a normalized name of the sensor being saved
                     (spaces are replaced with underscores).
    $SENSOR_CHANNEL - channel number of the sensor being saved.
    $SENSOR_ALIAS - an alias of the sensor being saved.
