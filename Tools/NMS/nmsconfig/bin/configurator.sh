#!/bin/bash

#APPNAME="synapnms-configurator"
APPNAME="$0"
APPHOME="/opt/synapnms-configurator"

CURDIR=$PWD

# Search app home
# if application $APPNAME is exist in $PATH and it's executable
APPPATH=`which $APPNAME 2>/dev/null`
if [ -x "$APPPATH" ]; then
    # if it symlink then get target
    if [ -L "$APPPATH" ]; then
	APPPATH=`stat $APPPATH | grep "File:" | sed -e "s/\(.*-> .\)\(.*\)\('\)/\2/"`
    fi
    # get application home
    # APPHOME=`echo $APPPATH | sed -e "s/\(.*\)\(\/\)\(.*\)/\1/"`
    APPHOME=`dirname $APPPATH`	
    # cd to APPHOME, cd to up because application locate in bin
    # get abs path and return to CURDIR
    pushd $APPHOME &>/dev/null; cd ..
    APPHOME=$PWD;
    popd &>/dev/null
fi

# default parametrs
CONFIG=$APPHOME/conf/serconf.xml
TEMPLBASE=$APPHOME/templates/Nagios
OUTBASE=$CURDIR/output/Nagios
JARFILE=$APPHOME/lib/nmsconf.jar
PROPERTIES="-Dlog4j.configuration=file:$APPHOME/lib/log4j.properties"
JAVA=$JAVA_HOME/bin/java

usage() {
    cat << USAGEXX

Usage: configurer [options] nagios-address[:port] agent-address[:port]

  -?                Print this help and exit.
  -h[elp]           Print this help and exit.
  -t=templatesBase  Full or relative path to the nagios configuration
                    template files.
                    If specified, overwrites value of the templates_base
                    attribute provided in the config-file.
  -o=outputBase     Full or relative path of the folder to save
                    the resulting configuration files.
  -c=configFile     Full or relative name to the serializers
                    configuration file.
  nagios-address    DNS name or IP-address of the server with
                    Nagios manager running on.
  agent-address     DNS name or IP-address of the server with
                    Synapse SNMP agent running on.
  port              Port that the Nagios/agent is running on.
                    Defaults are 80 for Nagios and 161 for agent.
USAGEXX
}

# parse comand line
while getopts ":c:t:o:" Option
do
    # remove symbol '=' at begin argument
    OPTARG=`echo $OPTARG | sed -e "s/^=//"`
    case $Option in
    c ) CONFIG="$OPTARG";;    
    t ) TEMPLBASE="$OPTARG";;
    o ) OUTBASE="$OPTARG";;
    * ) usage; exit 1;;
    esac
done
shift $(($OPTIND - 1))

# check for nag-address and agent-address
if [ $# -ne 2 ]; then
    usage; exit 1;
fi    

# if output dir is not existed then create it
if [ ! -d $OUTBASE ]; then
    mkdir -p $OUTBASE
fi

# get libs list
CLASSPATH=
for i in `find $APPHOME/lib -name "*.jar"`; do
    CLASSPATH="$CLASSPATH:$i"
done

echo "-------------------------------------------"
echo "APPHOME = $APPHOME"
echo "-------------------------------------------"
echo "CONFIG         = $CONFIG"
echo "TEMPLBASE      = $TEMPLBASE"
echo "OUTBASE        = $OUTBASE"
echo "NAGIOS-ADDRESS = $1"
echo "AGENT-ADDRESS  = $2"
#echo "CLASSPATH = <$CLASSPATH>"
echo "-------------------------------------------"

# -classpath $CLASSPATH  
# -t=$TEMPLBASE -o=$OUTBASE $CONFIG $@"

echo "$JAVA $PROPERTIES -jar $JARFILE -t=$TEMPLBASE -o=$OUTBASE $CONFIG $@"

$JAVA $PROPERTIES -jar $JARFILE -t=$TEMPLBASE -o=$OUTBASE $CONFIG $@
exit $?
 
