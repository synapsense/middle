@echo off

REM check for empty and help parameters to
REM substitute usage string of the jar with ours.
if "%1"=="" goto :PRINT_USAGE
if "%1"=="-?" goto :PRINT_USAGE
if "%1"=="-h" goto :PRINT_USAGE
if "%1"=="-help" goto :PRINT_USAGE

setlocal

REM find path to bat file:
set BAT_PATH=%~dp0
set JAR_PATH="%BAT_PATH%\..\lib\nmsconf.jar"
set LOG4J_PROPERTIES="%BAT_PATH%\..\lib\log4j.properties"

REM go through all the command-line arguments
:LOOP
if "%1"=="" goto PROCEED
   if "%1"=="-t" (
      set TEMPL_BASE=%2
      shift
      shift
      goto LOOP
   ) 
   if "%1"=="-o" (
      set OUTPUT_BASE=%2
      shift
      shift
      goto LOOP
   )
   if "%1"=="-c" (
      set CONFIG_FILE=%2
      shift
      shift
      goto LOOP
   )
   set NAG_ADDR=%1
   set AGENT_ADDR=%2
   goto :PROCEED
SHIFT
goto LOOP
:PROCEED

if not defined NAG_ADDR goto :PRINT_USAGE
if not defined AGENT_ADDR goto :PRINT_USAGE

REM if config file is not specified, set the defaults:
if not defined CONFIG_FILE (
   set CONFIG_FILE="%BAT_PATH%\..\conf\serconf.xml"
   REM override
   if not defined TEMPL_BASE set TEMPL_BASE="%BAT_PATH%\..\templates\Nagios"
)

REM compose command line:
set CMD_LINE=-Dlog4j.configuration=file:%LOG4J_PROPERTIES% -jar %JAR_PATH%
if defined TEMPL_BASE set CMD_LINE=%CMD_LINE% -t=%TEMPL_BASE% 
if defined OUTPUT_BASE set CMD_LINE=%CMD_LINE% -o=%OUTPUT_BASE% 
set CMD_LINE=%CMD_LINE% %CONFIG_FILE% %NAG_ADDR% %AGENT_ADDR%

java %CMD_LINE%
endlocal
goto :EOF

:PRINT_USAGE
echo Usage: configurator [options] nagios-address[:port] agent-address[:port]
echo.
echo  -?                Print this help and exit.
echo  -h[elp]           Print this help and exit.
echo  -t=templatesBase  Full or relative path to the nagios configuration
echo                    template files.
echo                    If specified, overwrites value of the templates_base
echo                    attribute provided in the config-file.
echo  -o=outputBase     Full or relative path of the folder to save
echo                    the resulting configuration files.
echo  -c=configFile     Full or relative name to the serializers
echo                    configuration file.
echo  nagios-address    DNS name or IP-address of the server with
echo                    Nagios manager running on.
echo  agent-address     DNS name or IP-address of the server with
echo                    Synapse SNMP agent running on.
echo  port              Port that the Nagios/agent is running on.
echo                    Defaults are 80 for Nagios and 161 for agent.
endlocal