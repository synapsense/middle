# Generic host definition template - This is NOT a real host, just a template!

define host{
        name                            generic-host    ; The name of this host template
        notifications_enabled           1       ; Host notifications are enabled
        event_handler_enabled           1       ; Host event handler is enabled
        flap_detection_enabled          1       ; Flap detection is enabled
        failure_prediction_enabled      1       ; Failure prediction is enabled
        process_perf_data               1       ; Process performance data
        retain_status_information       1       ; Retain status information across program restarts
        retain_nonstatus_information    1       ; Retain non-status information across program restarts
        register                        0       ; DONT REGISTER THIS DEFINITION - ITS NOT A REAL HOST, JUST A TEMPLATE!
	check_interval		1
        max_check_attempts      10                      ; Number of reties before an alert is sent
        check_period            24x7
        notification_interval   1                     ; Time period to wait until re-notifying contacts
        notification_period     24x7                    ; Time period where notifications are allowed to be sent
        notification_options    d,r
        contact_groups          admins                  ; Groups that are notified when notifications are sent
        }

define host{
        use                     generic-host            ; Name of host template to use
        host_name               $AGENT_HOST               ; Name of device being monitored
        alias                   $AGENT_HOST	               ; Longer name or description of device
        address                 $AGENT_HOST               ; IP or FQDN of device being monitored
        check_command           check-host-alive        ; Short name of command used to check if host is up or down, usually a ping
}

