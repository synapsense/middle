define host{
        use                     generic-host            ; Name of host template to use
        host_name               Sensor_$SENSOR_ID_channel_$SENSOR_CHANNEL               ; Name of device being monitored
        alias                   Name=$SENSOR_NAME, Descr=$SENSOR_ALIAS	               ; Longer name or description of device
        address                 $AGENT_HOST               ; IP or FQDN of device being monitored
	parents			Node_$NODE_ID
        check_command           a_check_sensors!$NAGIOS_HOST!$SENSOR_ID!$NODE_ID!$NET_ID        ; Short name of command used to check if host is up or down, usually a ping
}

