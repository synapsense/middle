define host{
        use                     generic-host            ; Name of host template to use
        host_name               SensorNet-$NET_ID               ; Name of device being monitored
        alias                   $NET_ALIAS	               ; Longer name or description of device
        address                 $AGENT_HOST               ; IP or FQDN of device being monitored
		parents					$AGENT_HOST
        check_command           a_check_nets!$NAGIOS_HOST!$NET_ID!1!1        ; Short name of command used to check if host is up or down, usually a ping
}

