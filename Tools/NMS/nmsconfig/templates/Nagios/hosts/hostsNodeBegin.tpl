define host{
	use     		generic-host    	; Name of host template to use
	host_name       	Node-$NODE_PHYS_ID      	 ; Name of device being monitored
	alias   		ID=$NODE_ID, Name=$NODE_NAME, Descr=$NODE_ALIAS     ; Longer name or description of device
	address 		$AGENT_HOST     	  ; IP or FQDN of device being monitored
	parents 		Net_$NET_ID
	check_command   	a_check_nodes!$NAGIOS_HOST!$NODE_ID!$NET_ID!1!1        ; Short name of command used to check if host is up or down, usually a ping
}
