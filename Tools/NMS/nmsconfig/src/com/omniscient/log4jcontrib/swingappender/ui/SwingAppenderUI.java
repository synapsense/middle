/**
 * 
 */
package com.omniscient.log4jcontrib.swingappender.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 * Creates a UI to display log messages from a SwingAppender
 * 
 * @author pshah
 * 
 */
public class SwingAppenderUI {
	private JFrame jframe;
	private JButton clear; // button to clear the text area
	private JPanel buttonsPanel; // panel to hold all buttons
	private JTextPane logMessagesDisp; // display area
	private JScrollPane scrollPane;
	private List<String> logBuffer;
	private int appState;

	/* Constants */
	public static final String STYLE_REGULAR = "regular";
	public static final String STYLE_HIGHLIGHTED = "highlighted";
	public static final String START = "start";
	public static final String PAUSE = "pause";
	public static final String STOP = "stop";
	public static final String CLEAR = "clear";
	public static final int STARTED = 0;
	public static final int PAUSED = 1;
	public static final int STOPPED = 2;

	/**
	 * An instance for SwingAppenderUI class. This holds the Singleton.
	 */
	private static SwingAppenderUI instance;

	/**
	 * Method to get an instance of the this class. This method ensures that
	 * SwingAppenderUI is a Singleton using a doule checked locking mechanism.
	 * 
	 * @return An instance of SwingAppenderUI
	 */
	public static SwingAppenderUI getInstance() {
		// System.out.println("getting UI Instance");
		if (instance == null) {
			synchronized (SwingAppenderUI.class) {
				if (instance == null) {
					instance = new SwingAppenderUI();
				}
			}
		}
		return instance;
	}

	/**
	 * Private constructer to ensure that this object cannot e instantiated from
	 * outside this class.
	 */
	private SwingAppenderUI() {
		// set internal attributes
		logBuffer = new ArrayList<String>();
		appState = STARTED;

		// create main window
		jframe = new JFrame();
		jframe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jframe.setTitle("Logs appeared during Configuration Building/Writing");

		// initialize buttons
		initButtonsPanel();

		// create text area to hold the log messages
		initMessageDispArea();

		// add components to the contentPane
		jframe.getContentPane().add(BorderLayout.NORTH, buttonsPanel);
		jframe.getContentPane().add(BorderLayout.CENTER, scrollPane);
		jframe.setSize(800, 600);
		// jframe.setVisible(true);
	}

	/**
	 * Displays the log in the text area unless dispMsg is set to false in which
	 * case it adds the log to a buffer. When dispMsg becomes true, the buffer
	 * is first flushed and it's contents are displayed in the text area.
	 * 
	 * @param log
	 *            The log message to be displayed in the text area
	 */
	public void doLog(String log) {
		if (appState == STARTED) {
			try {
				StyledDocument sDoc = logMessagesDisp.getStyledDocument();
				if (!logBuffer.isEmpty()) {
					Iterator<String> iter = logBuffer.iterator();
					while (iter.hasNext()) {
						sDoc.insertString(sDoc.getEndPosition().getOffset() - 1, iter.next(),
						        sDoc.getStyle(STYLE_REGULAR));
						iter.remove();
					}
				}
				sDoc.insertString(sDoc.getEndPosition().getOffset() - 1, log, sDoc.getStyle(STYLE_REGULAR));
			} catch (BadLocationException ble) {
				System.out.println("Bad Location Exception : " + ble.getMessage());
			}
		} else if (appState == PAUSED) {
			logBuffer.add(log);
		}
	}

	/**
	 * creates a panel to hold the buttons
	 */
	private void initButtonsPanel() {
		buttonsPanel = new JPanel();
		clear = new JButton(CLEAR);
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				logMessagesDisp.setText("");
			}
		});

		buttonsPanel.add(clear);

	}

	/**
	 * Creates a scrollable text area
	 */
	private void initMessageDispArea() {
		logMessagesDisp = new JTextPane();
		scrollPane = new JScrollPane(logMessagesDisp);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		// add styles
		StyledDocument sDoc = logMessagesDisp.getStyledDocument();
		Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
		Style s1 = sDoc.addStyle(STYLE_REGULAR, def);
		StyleConstants.setFontFamily(def, "SansSerif");

		Style s2 = sDoc.addStyle(STYLE_HIGHLIGHTED, s1);
		StyleConstants.setBackground(s2, Color.BLUE);

	}

	public void close() {
		// clean up code for UI goes here.
		jframe.setVisible(false);
	}

	public JFrame getJframe() {
		return jframe;
	}

	public void setJframe(JFrame jframe) {
		this.jframe = jframe;
	}
}