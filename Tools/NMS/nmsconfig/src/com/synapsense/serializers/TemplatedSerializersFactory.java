package com.synapsense.serializers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.serializers.TemplatedSerializer.Templates;

/**
 * Class for creating templated erializers basing on XML configuration file.
 * 
 * @author Oleg Stepanov
 * 
 */
public class TemplatedSerializersFactory {

	private static final Logger log = Logger.getLogger(TemplatedSerializersFactory.class);

	private String templatesBase;
	private String outputBase;

	/**
	 * Creates new factory.
	 * 
	 * @param templatesBase
	 *            - override for templates path (null for no override).
	 * @param outputBase
	 *            - override for output path (null for no override).
	 */
	public TemplatedSerializersFactory(String templatesBase, String outputBase) {
		this.templatesBase = templatesBase != null ? templatesBase : "";
		this.outputBase = outputBase != null ? outputBase : "";
	}

	/**
	 * Builds serializators list basing on xml configuration.
	 * 
	 * @param confFileName
	 *            name of configuration file (with or without path).
	 * @return list populated with serializers.
	 * @throws SerializersFactoryException
	 */
	public Collection<TemplatedSerializer> getSerializers(String confFileName) throws SerializersFactoryException {
		// parse configuration
		Collection<SerializerConfig> confs = parseConfig(confFileName);
		LinkedList<TemplatedSerializer> serializers = new LinkedList<TemplatedSerializer>();
		// create serializer according to config parameters
		for (SerializerConfig conf : confs) {
			serializers.add(new TemplatedSerializer(conf.templates, conf.out));
		}
		return serializers;
	}

	/**
	 * Parses configuration file and extracts configuration parameters for each
	 * described serializer.
	 * 
	 * @param confFileName
	 *            configuration file name.
	 * @return list of serializer configurations.
	 * @throws SerializersFactoryException
	 */
	private Collection<SerializerConfig> parseConfig(String confFileName) throws SerializersFactoryException {
		// list of configurations
		LinkedList<SerializerConfig> conf = new LinkedList<SerializerConfig>();
		// get an xml parser factory and parser
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new SerializersFactoryException("Cannot create xml parser", e);
		}

		Document confDoc;

		try {
			confDoc = docBuilder.parse(new File(confFileName));
		} catch (SAXException e) {
			throw new SerializersFactoryException("Config parsing error", e);
		} catch (IOException e) {
			throw new SerializersFactoryException("Cannot parse Config File due to IO error", e);
		}

		// get templates base path
		if (templatesBase.isEmpty()) {
			templatesBase = confDoc.getDocumentElement().getAttribute("templatesBase");
		}
		if (templatesBase.isEmpty())
			log.warn("templatesBase parameter is not specified");
		// get templates base path
		if (outputBase.isEmpty()) {
			outputBase = confDoc.getDocumentElement().getAttribute("outputBase");
		}
		if (outputBase.isEmpty())
			log.warn("outputBase parameter is not specified");

		// go through serializers descriptions
		NodeList serializers = confDoc.getElementsByTagName("serializer");
		for (int i = 0; i < serializers.getLength(); i++) {
			if (serializers.item(i).getNodeType() != Node.ELEMENT_NODE)
				continue;// skip text nodes etc.
			Element serializer = (Element) serializers.item(i);
			String outFileName = serializer.getAttribute("out");
			if (outFileName.isEmpty())
				throw new SerializersFactoryException("Empty out attribute specified for the serializer #" + i);

			NodeList templ_defs = serializer.getChildNodes();
			Map<String, String> templ_files = new HashMap<String, String>();

			// get template file name for each serialization event type
			for (int j = 0; j < templ_defs.getLength(); j++) {
				if (templ_defs.item(j).getNodeType() != Node.ELEMENT_NODE)
					continue;// skip text nodes etc.
				Element template = (Element) templ_defs.item(j);
				String templ_type = template.getNodeName();
				// check the event type
				if (!TemplatedSerializer.Templates.isKey(templ_type)) {
					log.warn("Unknown event type " + templ_type + " for " + outFileName + " serializer, ignored.");
					continue;
				}
				String templ_file = template.getAttribute("template");
				if (templ_file.isEmpty())
					throw new SerializersFactoryException("Empty template file name for the event " + templ_type
					        + " of the serializer for " + outFileName);
				templ_files.put(templ_type, templ_file);
			}

			// create out:
			Writer out;
			try {
				File outFile = new File(outputBase.isEmpty() ? null : outputBase, outFileName);
				File outDir = outFile.getParentFile();
				// forcibly create output folder if not exists
				if (outDir != null && !outDir.exists()) {
					if (!outDir.mkdirs())
						throw new SerializersFactoryException("Cannot create output folder " + outDir.getAbsolutePath());
				}
				out = new BufferedWriter(new FileWriter(outFile));
			} catch (IOException e) {
				throw new SerializersFactoryException("Cannot create writer for " + outFileName, e);
			}

			// read template content for each event type
			Map<String, CharSequence> templates = new HashMap<String, CharSequence>();
			for (Map.Entry<String, String> templFile : templ_files.entrySet()) {
				File in_file = new File(templatesBase.isEmpty() ? null : templatesBase, templFile.getValue());
				CharSequence template = readFileContent(in_file);
				templates.put(templFile.getKey(), template);
			}
			conf.add(new SerializerConfig(out, new TemplatedSerializer.Templates(templates)));
		}

		return conf;
	}

	/**
	 * Reads file content as a single character sequence.
	 * 
	 * @param templateFile
	 *            name of file to be read.
	 * @return file content as a character sequence.
	 * @throws SerializersFactoryException
	 */
	private static CharSequence readFileContent(File templateFile) throws SerializersFactoryException {
		FileInputStream fis;
		try {
			fis = new FileInputStream(templateFile);
		} catch (FileNotFoundException e) {
			throw new SerializersFactoryException("Cannot open template file", e);
		}
		ByteBuffer bbuf;
		try {
			FileChannel channel = fis.getChannel();
			bbuf = ByteBuffer.allocate((int) channel.size());
			channel.read(bbuf);
		} catch (IOException e) {
			throw new SerializersFactoryException("Cannot read content of " + templateFile.getAbsolutePath(), e);
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				log.warn("Error while closing file", e);
			}
		}
		bbuf.flip();

		// TODO: default charset is used for encoding. charset can be extracted
		// into params.
		CharBuffer cbuf = Charset.defaultCharset().decode(bbuf);
		return cbuf;
	}

	/**
	 * Test main entry point.
	 * 
	 * @param args
	 *            command line.
	 * @throws SerializersFactoryException
	 */
	public static void main(String[] args) throws SerializersFactoryException {
		TemplatedSerializersFactory factory = new TemplatedSerializersFactory("", "");
		Collection<SerializerConfig> conf = factory.parseConfig("config/serconf.xml");
		conf.clear();// stops compiler complains
	}
}

/**
 * Holder class for serializer parameters.
 * 
 * @author Oleg Stepanov
 * 
 */
class SerializerConfig {
	Writer out;
	TemplatedSerializer.Templates templates;

	public SerializerConfig(Writer out, Templates templates) {
		super();
		this.out = out;
		this.templates = templates;
	}
}