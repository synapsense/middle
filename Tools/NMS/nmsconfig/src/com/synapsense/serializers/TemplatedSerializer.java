package com.synapsense.serializers;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.synapsense.conf.ConfigurationSerializer;
import com.synapsense.conf.SerializationException;
import com.synapsense.conf.objects.Configuration;
import com.synapsense.conf.objects.Network;
import com.synapsense.conf.objects.Node;
import com.synapsense.conf.objects.Sensor;

/**
 * A serializer that can be configured with templates. Template represents any
 * arbitrary text with macros. For each serialization event the corresponding
 * template is processed (macros are expanded) and appended to the output file.
 * 
 * @author Oleg Stepanov
 * 
 */
public class TemplatedSerializer implements ConfigurationSerializer {
	private static final Logger log = Logger.getLogger(TemplatedSerializer.class);

	/**
	 * Class-holder for template bodies.
	 * 
	 * @author Oleg Stepanov
	 * 
	 */
	public static class Templates {
		protected final CharSequence confBegin;
		protected final CharSequence confEnd;
		protected final CharSequence netBegin;
		protected final CharSequence netEnd;
		protected final CharSequence nodeBegin;
		protected final CharSequence nodeEnd;
		protected final CharSequence sensorBegin;
		protected final CharSequence sensorEnd;

		/**
		 * Create templates holder. Template bodies are passed as parameters.
		 * 
		 * @param confBegin
		 *            - template for configuration save begin event.
		 * @param confEnd
		 *            - template for configuration save end event.
		 * @param netBegin
		 *            - template for network save begin event.
		 * @param netEnd
		 *            - template for network save end event.
		 * @param nodeBegin
		 *            - template for node save begin event.
		 * @param nodeEnd
		 *            - template for node save end event.
		 * @param sensorBegin
		 *            - template for sensor save begin event.
		 * @param sensorEnd
		 *            - template for sensor save end event.
		 */
		public Templates(CharSequence confBegin, CharSequence confEnd, CharSequence netBegin, CharSequence netEnd,
		        CharSequence nodeBegin, CharSequence nodeEnd, CharSequence sensorBegin, CharSequence sensorEnd) {
			super();
			this.confBegin = confBegin;
			this.confEnd = confEnd;
			this.netBegin = netBegin;
			this.netEnd = netEnd;
			this.nodeBegin = nodeBegin;
			this.nodeEnd = nodeEnd;
			this.sensorBegin = sensorBegin;
			this.sensorEnd = sensorEnd;
		}

		/**
		 * Create templates holder. Template bodies are passed as map values.
		 * 
		 * @param templates
		 *            - a map with template bodies. Keys are the same strings as
		 *            for constructor with explicit parameters.
		 */
		public Templates(Map<String, CharSequence> templates) {
			super();
			this.confBegin = safeGetSequence(templates, "confBegin");
			this.confEnd = safeGetSequence(templates, "confEnd");
			this.netBegin = safeGetSequence(templates, "netBegin");
			this.netEnd = safeGetSequence(templates, "netEnd");
			this.nodeBegin = safeGetSequence(templates, "nodeBegin");
			this.nodeEnd = safeGetSequence(templates, "nodeEnd");
			this.sensorBegin = safeGetSequence(templates, "sensorBegin");
			this.sensorEnd = safeGetSequence(templates, "sensorEnd");
		}

		private static Set<String> TEMPL_KEYS = new HashSet<String>();
		static {
			TEMPL_KEYS.add("confBegin");
			TEMPL_KEYS.add("confEnd");
			TEMPL_KEYS.add("netBegin");
			TEMPL_KEYS.add("netEnd");
			TEMPL_KEYS.add("nodeBegin");
			TEMPL_KEYS.add("nodeEnd");
			TEMPL_KEYS.add("sensorBegin");
			TEMPL_KEYS.add("sensorEnd");
		}

		/**
		 * Checks whether given string is a valid event name.
		 * 
		 * @param name
		 *            string to be tested.
		 * @return true if string is an event name, false otherwise.
		 */
		public static boolean isKey(String name) {
			return TEMPL_KEYS.contains(name);
		}

		/**
		 * Allows to retrieve template body for a given template name.
		 * 
		 * @param templates
		 *            map with templates.
		 * @param tmplName
		 *            name of template.
		 * @return template's body if such one exists in the map or empty
		 *         string.
		 */
		private static CharSequence safeGetSequence(Map<String, CharSequence> templates, String tmplName) {
			CharSequence seq = templates.get(tmplName);
			if (seq == null) {
				// if(log.isDebugEnabled())
				// log.debug("No value for "+tmplName+" specified, using empty string");
				return "";
			}
			return seq;
		}
	}

	private final Templates templates;

	private final Writer out;

	/**
	 * Creates serializer.
	 * 
	 * @param templates
	 *            - set of templates bodies.
	 * @param out
	 *            - writer for output receiving.
	 */
	public TemplatedSerializer(Templates templates, Writer out) {
		if (templates == null)
			throw new IllegalArgumentException("templates cannot be null");
		if (out == null)
			throw new IllegalArgumentException("out Writer cannot be null");
		this.templates = templates;
		this.out = out;
	}

	public void confSaveBegin(Configuration conf) throws SerializationException {
		Map<Pattern, String> values = new HashMap<Pattern, String>();
		try {
			out.append(replaceAll(templates.confBegin, fillConfValues(values, conf)));
		} catch (IOException e) {
			throw new SerializationException("Cannot write conf begin part", e);
		}
	}

	public void confSaveEnd(Configuration conf) throws SerializationException {
		Map<Pattern, String> values = new HashMap<Pattern, String>();
		try {
			out.append(replaceAll(templates.confEnd, fillConfValues(values, conf)));
		} catch (IOException e) {
			throw new SerializationException("Cannot write conf end part", e);
		}
	}

	public void netSaveBegin(Network net) throws SerializationException {
		Map<Pattern, String> values = new HashMap<Pattern, String>();
		try {
			out.append(replaceAll(templates.netBegin, fillNetValues(values, net)));
		} catch (IOException e) {
			throw new SerializationException("Cannot write network begin part", e);
		}
	}

	public void netSaveEnd(Network net) throws SerializationException {
		Map<Pattern, String> values = new HashMap<Pattern, String>();
		try {
			out.append(replaceAll(templates.netEnd, fillNetValues(values, net)));
		} catch (IOException e) {
			throw new SerializationException("Cannot write network end part", e);
		}
	}

	public void nodeSaveBegin(Node node) throws SerializationException {
		Map<Pattern, String> values = new HashMap<Pattern, String>();
		try {
			out.append(replaceAll(templates.nodeBegin, fillNodeValues(values, node)));
		} catch (IOException e) {
			throw new SerializationException("Cannot write node begin part", e);
		}
	}

	public void nodeSaveEnd(Node node) throws SerializationException {
		Map<Pattern, String> values = new HashMap<Pattern, String>();
		try {
			out.append(replaceAll(templates.nodeEnd, fillNodeValues(values, node)));
		} catch (IOException e) {
			throw new SerializationException("Cannot write node end part", e);
		}
	}

	public void sensorSaveBegin(Sensor sensor) throws SerializationException {
		Map<Pattern, String> values = new HashMap<Pattern, String>();
		try {
			out.append(replaceAll(templates.sensorBegin, fillSensorValues(values, sensor)));
		} catch (IOException e) {
			throw new SerializationException("Cannot write sensor begin part", e);
		}
	}

	public void sensorSaveEnd(Sensor sensor) throws SerializationException {
		Map<Pattern, String> values = new HashMap<Pattern, String>();
		try {
			out.append(replaceAll(templates.sensorEnd, fillSensorValues(values, sensor)));
		} catch (IOException e) {
			throw new SerializationException("Cannot write sensor end part", e);
		}
	}

	public void close() {
		try {
			out.close();
			if (log.isDebugEnabled())
				log.debug("Out is closed " + out);
		} catch (IOException e) {
			log.warn("Exception while closing the out " + out, e);
		}
	}

	/**
	 * Fills map with configuration macros values.
	 * 
	 * @param map
	 *            map to be filled.
	 * @param conf
	 *            configuration to get values from.
	 * @return filled map.
	 */
	private static Map<Pattern, String> fillConfValues(Map<Pattern, String> map, Configuration conf) {
		map.put(Macro.NAGIOS_HOST, conf.getNagiosHost());
		map.put(Macro.NAGIOS_PORT, conf.getNagiosPort());
		map.put(Macro.AGENT_HOST, conf.getAgentHost());
		map.put(Macro.AGENT_PORT, conf.getAgentPort());
		return map;
	}

	/**
	 * Fills map with network macros values.
	 * 
	 * @param map
	 *            map to be filled.
	 * @param net
	 *            network to get values from.
	 * @return filled map.
	 */
	private static Map<Pattern, String> fillNetValues(Map<Pattern, String> map, Network net) {
		map.put(Macro.NET_ID, String.valueOf(net.getId()));
		map.put(Macro.NET_NAME, net.getName());
		map.put(Macro.NET_N_NAME, normalizeName(net.getName()));
		map.put(Macro.NET_ALIAS, normalizeAlias(net.getAlias()));
		map.put(Macro.NET_HOST, net.getAddress());
		// map.put(Macro.NET_PORT, net.getAddress());TODO: not implemented
		return fillConfValues(map, net.getConf());
	}

	/**
	 * Fills map with nodes macros values.
	 * 
	 * @param map
	 *            map to be filled.
	 * @param node
	 *            node to get values from.
	 * @return filled map.
	 */
	private static Map<Pattern, String> fillNodeValues(Map<Pattern, String> map, Node node) {
		map.put(Macro.NODE_ID, String.valueOf(node.getId()));
		map.put(Macro.NODE_PHYS_ID, node.getPhysId());
		map.put(Macro.NODE_NAME, node.getName());
		map.put(Macro.NODE_N_NAME, normalizeName(node.getName()));
		map.put(Macro.NODE_ALIAS, normalizeAlias(node.getAlias()));
		return fillNetValues(map, node.getParentNet());
	}

	/**
	 * Fills map with sensor macros values.
	 * 
	 * @param map
	 *            map to be filled.
	 * @param sensor
	 *            sensor to get values from.
	 * @return filled map.
	 */
	private static Map<Pattern, String> fillSensorValues(Map<Pattern, String> map, Sensor sensor) {
		map.put(Macro.SENSOR_ID, String.valueOf(sensor.getId()));
		map.put(Macro.SENSOR_NAME, sensor.getName());
		map.put(Macro.SENSOR_N_NAME, normalizeName(sensor.getName()));
		map.put(Macro.SENSOR_CHANNEL, String.valueOf(sensor.getNodeChannel()));
		map.put(Macro.SENSOR_ALIAS, normalizeAlias(sensor.getAlias()));
		return fillNodeValues(map, sensor.getParentNode());
	}

	/**
	 * Expands macros with corresponding values.
	 * 
	 * @param in
	 *            input sequence (template body).
	 * @param macroValues
	 *            map with macros substitutions.
	 * @return resulting sequence with expanded macros.
	 */
	private static CharSequence replaceAll(CharSequence in, Map<Pattern, String> macroValues) {
		CharSequence result = in;
		for (Map.Entry<Pattern, String> macroValue : macroValues.entrySet()) {
			Matcher m = macroValue.getKey().matcher(result);
			result = m.replaceAll(macroValue.getValue());
		}
		return result;
	}

	/**
	 * Function for replacing empty aliases with predefined string.
	 * 
	 * @param alias
	 *            string to be checked.
	 * @return "Not specified" string for null or "" aliases or the alias string
	 *         itself otherwise.
	 */
	private static String normalizeAlias(String alias) {
		if (alias == null || alias.isEmpty())
			return "Not specified";
		return alias;
	}

	/** Pattern for matching white space characters. */
	private static Pattern whiteSpace;
	static {
		whiteSpace = Pattern.compile("\\s");
	}

	/**
	 * Normalizes name so it can be used as an identifier. Current
	 * implementation replaces all white spaces with underscore.
	 * 
	 * @param name
	 *            string to be normalized.
	 * @return normalized string.
	 */
	private static String normalizeName(String name) {
		return whiteSpace.matcher(name).replaceAll("_");
	}

}

/**
 * Macros holder. TODO: extract to configuration file?
 */
class Macro {
	public static final Pattern NAGIOS_HOST;
	public static final Pattern NAGIOS_PORT;
	public static final Pattern AGENT_HOST;
	public static final Pattern AGENT_PORT;

	public static final Pattern NET_ID;
	public static final Pattern NET_NAME;
	public static final Pattern NET_N_NAME;
	public static final Pattern NET_ALIAS;
	public static final Pattern NET_HOST;
	public static final Pattern NET_PORT;// TODO: add support

	public static final Pattern NODE_ID;
	public static final Pattern NODE_PHYS_ID;
	public static final Pattern NODE_NAME;
	public static final Pattern NODE_N_NAME;
	public static final Pattern NODE_ALIAS;

	public static final Pattern SENSOR_ID;
	public static final Pattern SENSOR_NAME;
	public static final Pattern SENSOR_N_NAME;
	public static final Pattern SENSOR_CHANNEL;
	public static final Pattern SENSOR_ALIAS;

	static {
		NAGIOS_HOST = Pattern.compile("\\$NAGIOS_HOST");
		NAGIOS_PORT = Pattern.compile("\\$NAGIOS_PORT");
		AGENT_HOST = Pattern.compile("\\$AGENT_HOST");
		AGENT_PORT = Pattern.compile("\\$AGENT_PORT");

		NET_ID = Pattern.compile("\\$NET_ID");
		NET_NAME = Pattern.compile("\\$NET_NAME");
		NET_N_NAME = Pattern.compile("\\$NET_N_NAME");
		NET_ALIAS = Pattern.compile("\\$NET_ALIAS");
		NET_HOST = Pattern.compile("\\$NET_HOST");
		NET_PORT = Pattern.compile("\\$NET_PORT");// TODO: add support

		NODE_ID = Pattern.compile("\\$NODE_ID");
		NODE_PHYS_ID = Pattern.compile("\\$NODE_PHYS_ID");
		NODE_NAME = Pattern.compile("\\$NODE_NAME");
		NODE_N_NAME = Pattern.compile("\\$NODE_N_NAME");
		NODE_ALIAS = Pattern.compile("\\$NODE_ALIAS");

		SENSOR_ID = Pattern.compile("\\$SENSOR_ID");
		SENSOR_NAME = Pattern.compile("\\$SENSOR_NAME");
		SENSOR_N_NAME = Pattern.compile("\\$SENSOR_N_NAME");
		SENSOR_CHANNEL = Pattern.compile("\\$SENSOR_CHANNEL");
		SENSOR_ALIAS = Pattern.compile("\\$SENSOR_ALIAS");
	}
}