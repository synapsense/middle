package com.synapsense.serializers;

/**
 * Base class for all kinds of exceptions that may occur in the serializers
 * factory.
 * 
 * @author Oleg Stepanov
 * 
 */
public class SerializersFactoryException extends Exception {

	private static final long serialVersionUID = 8376600918561111590L;

	/**
	 * Creates exception with no description.
	 */
	public SerializersFactoryException() {
		super();
	}

	/**
	 * Creates exception with description.
	 * 
	 * @param message
	 *            exception description.
	 */
	public SerializersFactoryException(String message) {
		super(message);
	}

	/**
	 * Creates exception wrapping initial cause.
	 * 
	 * @param cause
	 *            root cause exception.
	 */
	public SerializersFactoryException(Throwable cause) {
		super(cause);
	}

	/**
	 * Creates exception using description and initial cause.
	 * 
	 * @param message
	 *            exception description.
	 * @param cause
	 *            root cause exception.
	 */
	public SerializersFactoryException(String message, Throwable cause) {
		super(message, cause);
	}

}
