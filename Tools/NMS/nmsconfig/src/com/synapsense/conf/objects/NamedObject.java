package com.synapsense.conf.objects;

/**
 * Base class for any object that has a name.
 * 
 * @author Oleg Stepanov
 * 
 */
public class NamedObject {

	private String name;

	private String alias;

	/**
	 * Creates object with name and alias.
	 * 
	 * @param name
	 *            object's name.
	 * @param alias
	 *            object's description.
	 * @throws IllegalArgumentException
	 *             if name is null.
	 */
	public NamedObject(String name, String alias) {
		if (name == null || name.isEmpty())
			throw new IllegalArgumentException("Object name cannot be null or empty string");
		this.name = name;
		this.alias = alias != null ? alias : "";
	}

	/**
	 * Creates object with name and empty alias.
	 * 
	 * @param name
	 *            object's name.
	 */
	public NamedObject(String name) {
		this(name, "");
	}

	/**
	 * @return object's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Changes object's name.
	 * 
	 * @param name
	 *            new object's name.
	 * @throws IllegalArgumentException
	 *             when name is null.
	 */
	protected void setName(String name) {
		if (name == null || name.isEmpty())
			throw new IllegalArgumentException("Object name cannot be null or empty string");
		this.name = name;
	}

	/**
	 * @return object's alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Changes object's alias.
	 * 
	 * @param alias
	 *            new object's name. If null, the alias will be set to empty
	 *            string.
	 */
	protected void setAlias(String alias) {
		this.alias = alias != null ? alias : "";
	}
}
