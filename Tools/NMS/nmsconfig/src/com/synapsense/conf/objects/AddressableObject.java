package com.synapsense.conf.objects;

/**
 * Base class for object that has an address.
 * 
 * @author stepanov
 * 
 */
public class AddressableObject extends NamedObject {

	/** A string representing an address of this object. */
	private String address;

	/**
	 * Creates object with name, alias and address.
	 * 
	 * @param name
	 *            object's name.
	 * @param alias
	 *            object's description.
	 * @param address
	 *            object's address.
	 * @throws IllegalArgumentException
	 *             if address is null.
	 */
	public AddressableObject(String name, String alias, String address) {
		super(name, alias);
		if (address == null || address.isEmpty())
			throw new IllegalArgumentException("Object address cannot be null or empty string");
		this.address = address;
	}

	/**
	 * Creates object with name, address and empty alias.
	 * 
	 * @param name
	 *            object's name.
	 * @param alias
	 *            object's description.
	 * 
	 */
	public AddressableObject(String name, String address) {
		this(name, "", address);
	}

	/**
	 * @return object's address.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Changes object's alias.
	 * 
	 * @param alias
	 *            new object's name.
	 * @throws IllegalArgumentException
	 *             if address is null.
	 */
	protected void setAddress(String address) {
		if (address == null || address.isEmpty())
			throw new IllegalArgumentException("Object address cannot be null or empty string");
		this.address = address;
	}
}
