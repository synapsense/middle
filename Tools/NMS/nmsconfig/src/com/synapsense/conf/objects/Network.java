package com.synapsense.conf.objects;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Class that stores network parameters.
 * 
 * @author stepanov
 * 
 */
public class Network extends AddressableObject {

	private LinkedList<Node> nodes = new LinkedList<Node>();

	private int id;

	private Configuration conf;

	/**
	 * Creates network object with empty alias.
	 * 
	 * @param id
	 *            network's id.
	 * @param name
	 *            network's name.
	 * @param address
	 *            network's address.
	 */
	public Network(int id, String name, String address) {
		super(name, address);
		this.id = id;
	}

	/**
	 * Creates network object.
	 * 
	 * @param id
	 *            network's id.
	 * @param name
	 *            network's name.
	 * @param alias
	 *            network's alias.
	 * @param address
	 *            network's address.
	 */
	public Network(int id, String name, String alias, String address) {
		super(name, alias, address);
		this.id = id;
	}

	/**
	 * Adds a node to this network. Node's <code>setParentNet</code> is called
	 * with <code>this</code>.
	 * 
	 * @param node
	 *            a node to be added.
	 * @throws IllegalArgumentException
	 *             if node is null.
	 */
	public void addNode(Node node) {
		if (node == null)
			throw new IllegalArgumentException("Cannot add null node");
		nodes.add(node);
		node.setParentNet(this);
	}

	/**
	 * Removes node from this network. Node's <code>setParentNet</code> will be
	 * called with <code>null</code>.
	 * 
	 * @param node
	 *            a node to be removed.
	 */
	public void removeNode(Node node) {
		if (nodes.remove(node))
			node.setParentNet(null);
	}

	/**
	 * @return Unmodifiable collection of nodes belong to this network.
	 */
	public Collection<Node> getNodes() {
		return Collections.unmodifiableCollection(nodes);
	}

	/**
	 * Allows to get a node with given id.
	 * 
	 * @param id
	 *            id of a node being searched for.
	 * @return A node with given id.
	 * @throws IndexOutOfBoundsException
	 *             if there's no node with such id in this network.
	 */
	public Node getNode(int id) {
		for (Node node : nodes)
			if (node.getId() == id)
				return node;
		throw new IndexOutOfBoundsException("There's no node with id=" + id + " in network " + this);
	}

	/**
	 * @return network's id.
	 */
	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "{Id=" + id + ", Name=\"" + getName() + "\", alias=\"" + getAlias() + "\", address=\"" + getAddress()
		        + "\"}";
	}

	/**
	 * @return Configuration this network belongs to.
	 */
	public Configuration getConf() {
		return conf;
	}

	/**
	 * Set parent configuration object.
	 * 
	 * @param conf
	 *            parent configuration object or <code>null</code> for none.
	 */
	void setConf(Configuration conf) {
		this.conf = conf;
	}

}
