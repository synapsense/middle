package com.synapsense.conf.objects;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Class holding the SynapSense networks configuration.
 * 
 * @author Oleg Stepanov.
 * 
 */
public class Configuration {

	private LinkedList<Network> networks = new LinkedList<Network>();

	private final String nagios_host;
	private final String nagios_port;
	private final String agent_host;
	private final String agent_port;

	/**
	 * Creates new empty configuration.
	 * 
	 * @param nagios_host
	 *            address of Nagios server.
	 * @param nagios_port
	 *            Nagios server port.
	 * @param agent_host
	 *            address of SNMP agent server.
	 * @param agent_port
	 *            SNMP agent server port.
	 * @throws IllegalArgumentException
	 *             if any of arguments is null.
	 */
	public Configuration(String nagios_host, String nagios_port, String agent_host, String agent_port) {
		super();
		if (nagios_host == null || nagios_host.isEmpty())
			throw new IllegalArgumentException("Nagios host cannot be null or empty");
		if (nagios_port == null || nagios_port.isEmpty())
			throw new IllegalArgumentException("Nagios port cannot be null or empty");
		if (agent_host == null || agent_host.isEmpty())
			throw new IllegalArgumentException("Agent host cannot be null or empty");
		if (agent_port == null || agent_port.isEmpty())
			throw new IllegalArgumentException("Agent port cannot be null or empty");
		this.nagios_host = nagios_host;
		this.nagios_port = nagios_port;
		this.agent_host = agent_host;
		this.agent_port = agent_port;
	}

	/**
	 * Adds a network to this configuration. Network's <code>setConf</code> is
	 * called with <code>this</code>.
	 * 
	 * @param net
	 *            a network to be added.
	 * @throws IllegalArgumentException
	 *             if net is null.
	 */
	public void addNetwork(Network net) {
		if (net == null)
			throw new IllegalArgumentException("Cannot add null network");
		networks.add(net);
		net.setConf(this);
	}

	/**
	 * Removes network from this configuration. Network's <code>setConf</code>
	 * will be called with <code>null</code>.
	 * 
	 * @param net
	 *            a network to be removed.
	 */
	public void removeNetwork(Network net) {
		if (networks.remove(net))
			net.setConf(null);
	}

	/**
	 * Returns unmodifiable collection of networks in this configuration.
	 * 
	 * @return Unmodifiable collection of networks.
	 */
	public Collection<Network> getNetworks() {
		return Collections.unmodifiableCollection(networks);
	}

	/**
	 * Allows to get a network with given id.
	 * 
	 * @param id
	 *            id of a network being searched for.
	 * @return A network with given id.
	 * @throws IndexOutOfBoundsException
	 *             if there's no network with such id in this configuration.
	 */
	public Network getNetwork(int id) {
		for (Network net : networks)
			if (net.getId() == id)
				return net;
		throw new IndexOutOfBoundsException("There's no network with id=" + id + " in the configuration");
	}

	/**
	 * Debug method that outputs configuration on system console.
	 * 
	 * @param config
	 *            configuration to be printed.
	 */
	public static void printConfig(Configuration config) {
		for (Network network : config.getNetworks()) {
			System.out.println("Network: " + network);
			for (Node node : network.getNodes()) {
				System.out.println("\tNode: " + node);
				for (Sensor sensor : node.getSensors()) {
					System.out.println("\t\tSensor: " + sensor);
				}
			}
		}
	}

	/**
	 * @return Nagios server host address.
	 */
	public String getNagiosHost() {
		return nagios_host;
	}

	/**
	 * @return Nagios server port.
	 */
	public String getNagiosPort() {
		return nagios_port;
	}

	/**
	 * @return SNMP agent host address.
	 */
	public String getAgentHost() {
		return agent_host;
	}

	/**
	 * @return SNMP agent port.
	 */
	public String getAgentPort() {
		return agent_port;
	}
}
