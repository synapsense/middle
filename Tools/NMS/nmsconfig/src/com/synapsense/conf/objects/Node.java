package com.synapsense.conf.objects;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public class Node extends NamedObject {

	private LinkedList<Sensor> sensors = new LinkedList<Sensor>();

	private int id;
	private String phys_id;

	private Network parentNet;

	/**
	 * Creates new node with an empty alias.
	 * 
	 * @param id
	 *            node's id.
	 * @param name
	 *            node's name.
	 */
	public Node(int id, String name, String phys_id) {
		super(name);
		this.id = id;
		this.phys_id = phys_id;
	}

	/**
	 * Creates new node.
	 * 
	 * @param id
	 *            node's id.
	 * @param name
	 *            node's name.
	 * @param alias
	 *            node's description.
	 */
	public Node(int id, String name, String alias, String phys_id) {
		super(name, alias);
		this.id = id;
		this.phys_id = phys_id;
	}

	/**
	 * Adds a node to this network. Sensors's <code>setParentNode</code> is
	 * called with <code>this</code>.
	 * 
	 * @param sensor
	 *            a sensor to be added.
	 * @throws IllegalArgumentException
	 *             if sensor is null.
	 */
	public void addSensor(Sensor sensor) {
		if (sensor == null)
			throw new IllegalArgumentException("Cannot add null sensor");
		sensors.add(sensor);
		sensor.setParentNode(this);
	}

	/**
	 * Removes sensor from this node. Sensor's <code>setParentNode</code> will
	 * be called with <code>null</code>.
	 * 
	 * @param sensor
	 *            a sensor to be removed.
	 */
	public void removeSensor(Sensor sensor) {
		if (sensors.remove(sensor))
			sensor.setParentNode(null);
	}

	/**
	 * @return Unmodifiable collection of sensors under this node.
	 */
	public Collection<Sensor> getSensors() {
		return Collections.unmodifiableCollection(sensors);
	}

	/**
	 * Allows to get a sensor with given id.
	 * 
	 * @param id
	 *            id of a sensor being searched for.
	 * @return A sensor with given id.
	 * @throws IndexOutOfBoundsException
	 *             if there's no sensor with such id under this node.
	 */
	public Sensor getSensor(int id) {
		for (Sensor sensor : sensors)
			if (sensor.getId() == id)
				return sensor;
		throw new IndexOutOfBoundsException("There's no sensor with id=" + id + " under the node " + this);
	}

	/**
	 * @return node's id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return node's physical id.
	 */
	public String getPhysId() {
		return phys_id;
	}

	/**
	 * @return Network this sensor belongs to.
	 */
	public Network getParentNet() {
		return parentNet;
	}

	/**
	 * Set parent network.
	 * 
	 * @param net
	 *            parent network or <code>null</code> for none.
	 */
	void setParentNet(Network net) {
		parentNet = net;
	}

	@Override
	public String toString() {
		return "{Id=" + id + ", Name=\"" + getName() + "\", alias=\"" + getAlias() + "\", physId=\"" + getPhysId()
		        + "\"}";
	}

}
