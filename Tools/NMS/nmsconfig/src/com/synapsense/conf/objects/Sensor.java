package com.synapsense.conf.objects;

public class Sensor extends NamedObject {

	private int id;

	private Node parentNode;

	private long nodeChannel;

	/**
	 * Creates new sensor with an empty alias and address.
	 * 
	 * @param id
	 *            sensor's id.
	 * @param name
	 *            sensor's name.
	 */
	public Sensor(int id, String name) {
		super(name);
		this.id = id;
	}

	/**
	 * Creates new sensor.
	 * 
	 * @param id
	 *            sensor's id.
	 * @param name
	 *            sensor's name.
	 * @param alias
	 *            sensor's description.
	 * @param address
	 *            sensor's address.
	 */
	public Sensor(int id, String name, String alias, String address) {
		super(name, alias);
		this.id = id;
	}

	/**
	 * @return sensor's id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return Sensor's parent node.
	 */
	public Node getParentNode() {
		return parentNode;
	}

	/**
	 * Set parent node.
	 * 
	 * @param node
	 *            parent node or <code>null</code> for none.
	 */
	void setParentNode(Node node) {
		parentNode = node;
	}

	@Override
	public String toString() {
		return "{Id=" + id + ", Name=\"" + getName() + "\", channel=" + nodeChannel + " alias=\"" + getAlias() + "\"}";
	}

	/**
	 * @return node-to-sensor channel number.
	 */
	public long getNodeChannel() {
		return nodeChannel;
	}

	/**
	 * Change node-to-sensor channel.
	 * 
	 * @param nodeChannel
	 *            new node-to-sensor channel number.
	 */
	public void setNodeChannel(long nodeChannel) {
		this.nodeChannel = nodeChannel;
	}

}
