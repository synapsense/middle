package com.synapsense.conf;

import java.io.IOException;
import java.util.Collection;
import org.apache.log4j.Logger;
import com.synapsense.GUI.guiFrame.BuildTask;
import com.synapsense.conf.objects.Configuration;
import com.synapsense.conf.objects.Network;
import com.synapsense.conf.objects.Node;
import com.synapsense.conf.objects.Sensor;
import com.synapsense.conf.snmp.SNMPConfigurationBuilder;
import com.synapsense.serializers.SerializersFactoryException;
import com.synapsense.serializers.TemplatedSerializer;
import com.synapsense.serializers.TemplatedSerializersFactory;

/**
 * The main class that glues up configuration builder and configuration
 * serializer.
 * 
 * @author Oleg Stepanov
 * 
 */
public class Configurator {
	private static final Logger log = Logger.getLogger(Configurator.class);

	private String confFileName;
	private String nagios_host;
	private String nagios_port;
	private String agent_host;
	private String agent_port;
	private String templateBase;
	private String outputBase;
	private SNMPConfigurationBuilder builder;
	private Configuration conf;

	/**
	 * Creates configurator basing on given command line parameters.
	 * 
	 * @param args
	 *            command line.
	 */
	public Configurator(String[] args) {
		parseArgs(args);
	}

	/**
	 * Parses command line parameters and options.
	 * 
	 * @param args
	 *            command line.
	 */
	private void parseArgs(String[] args) {
		ArgumentParser p = new ArgumentParser(args);
		if (p.hasOption("?") || p.hasOption("h") || p.hasOption("help")) {
			printUsage();
			System.exit(1);
		}
		templateBase = p.getOption("t");
		outputBase = p.getOption("o");
		confFileName = p.nextParam();
		nagios_host = p.nextParam();
		agent_host = p.nextParam();
		if (confFileName == null || nagios_host == null || agent_host == null) {
			printUsage();
			System.exit(1);

		}
		String[] parts = parseHostName(nagios_host);
		nagios_host = parts[0];
		nagios_port = parts.length > 1 ? parts[1] : "80";
		parts = parseHostName(agent_host);
		agent_host = parts[0];
		agent_port = parts.length > 1 ? parts[1] : "161";
	}

	/**
	 * Splits host name by ':' character
	 * 
	 * @param host
	 *            host name
	 * @return An array (not shorter than 1 and not longer than 2 items) with
	 *         host name as the first element and host port as the optional
	 *         second argument.
	 */
	private static String[] parseHostName(String host) {
		if (host == null)
			throw new IllegalArgumentException("host param must be not null");
		String[] parts = host.split(":");
		if (parts.length > 2) {
			printUsage();
			System.exit(1);
		}
		return parts;
	}

	/**
	 * Method calls builder to make a configuration, then calls all configured
	 * serializers to save it.
	 */
	public Configuration buildConfig(BuildTask listener) throws ConfigurationBuildException {
		// Create config builder

		builder = new SNMPConfigurationBuilder();
		builder.addProgressListener(listener);
		conf = builder.buildConfiguration(nagios_host, nagios_port, agent_host, agent_port);
		return conf;
	}

	/**
	 * Method calls all configured serializers to save the configuration
	 */
	public void saveConfig() throws SerializationException {
		// create serializers
		TemplatedSerializersFactory factory = new TemplatedSerializersFactory(templateBase, outputBase);
		Collection<TemplatedSerializer> serializers;
		try {
			serializers = factory.getSerializers(confFileName);
		} catch (SerializersFactoryException e) {
			throw new SerializationException("Cannot get serializers", e);
		}
		// serialize the configuration
		try {
			ser(conf, serializers);
		} finally {
			for (TemplatedSerializer serializer : serializers)
				serializer.close();
		}
	}

	/**
	 * Saves given configuration via serializers.
	 * 
	 * @param conf
	 *            configuration to be saved.
	 * @param serializers
	 *            serializers to be used for configuration saving.
	 * @throws SerializationException
	 *             if any serialization error occurs.
	 */
	private void ser(Configuration conf, Collection<TemplatedSerializer> serializers) throws SerializationException {
		// ok, let's just drive all serializers through the configuration
		// objects
		for (TemplatedSerializer serializer : serializers)
			serializer.confSaveBegin(conf);

		for (Network net : conf.getNetworks()) {
			for (TemplatedSerializer serializer : serializers)
				serializer.netSaveBegin(net);

			for (Node node : net.getNodes()) {
				for (TemplatedSerializer serializer : serializers)
					serializer.nodeSaveBegin(node);

				for (Sensor sensor : node.getSensors()) {
					for (TemplatedSerializer serializer : serializers) {
						serializer.sensorSaveBegin(sensor);
						serializer.sensorSaveEnd(sensor);
					}
				}

				for (TemplatedSerializer serializer : serializers)
					serializer.nodeSaveEnd(node);
			}

			for (TemplatedSerializer serializer : serializers)
				serializer.netSaveEnd(net);
		}
	}

	/**
	 * Main entry point.
	 * 
	 * @param args
	 *            command line.
	 */
	public static void main(String[] args) {
		Configurator configurer = new Configurator(args);
		try {
			configurer.buildConfig(null);
		} catch (ConfigurationBuildException e) {
			if (log.isDebugEnabled()) {
				log.debug("Configuration cannot be built", e);
			} else {
				log.fatal("Configuration cannot be built: " + e.getMessage());
			}
			System.exit(1);
			return;
		}
		try {
			configurer.saveConfig();
		} catch (SerializationException e) {
			if (log.isDebugEnabled()) {
				log.debug("Configuration cannot be serialized", e);
			} else {
				log.fatal("Configuration cannot be serialized: " + e.getMessage());
			}
			System.exit(1);
			return;
		}

	}

	private static String[] usage = new String[] { "",
	        "Usage: configurator [options] config-file nagios-address[:port]",
	        "                    agent-address[:port]", "", "  -?                Print this help and exit.",
	        "  -h[elp]           Print this help and exit.",
	        "  -t=templatesBase  Full or relative path to the nagios configuration",
	        "                    template files.",
	        "                    If specified, overwrites value of the templates_base",
	        "                    attribute provided in the config-file.",
	        "  -o=outputBase     Full or relative path of the folder to save",
	        "                    the resulting configuration files.",
	        "  config-file       Full or relative name to the serializers", "                    configuration file.",
	        "  nagios-address    DNS name or IP-address of the server with",
	        "                    Nagios manager running on.",
	        "  agent-address     DNS name or IP-address of the server with",
	        "                    Synapse SNMP agent running on.",
	        "  port              Port that the Nagios/agent is running on.",
	        "                    Defaults are 80 for Nagios and 161 for agent." };

	/**
	 * Prints usage string to the system console.
	 */
	private static void printUsage() {
		for (int i = 0; i < usage.length; i++) {
			System.out.println(usage[i]);
		}
	}

	public void setConfFileName(String confFileName) {
		this.confFileName = confFileName;
	}

	public void setTemplateBase(String templateBase) {
		this.templateBase = templateBase;
	}

	public void setOutputBase(String outputBase) {
		this.outputBase = outputBase;
	}
}
