package com.synapsense.conf;

import com.synapsense.conf.objects.Configuration;
import com.synapsense.conf.objects.Network;
import com.synapsense.conf.objects.Node;
import com.synapsense.conf.objects.Sensor;

/**
 * Interface for classes that save a configuration into any specific serialized
 * form.
 * 
 * @author stepanov
 * 
 */
public interface ConfigurationSerializer {

	/**
	 * Method is called when serialization of configuration begins.
	 * 
	 * @param conf
	 *            configuration being saved.
	 * @throws SerializationException
	 */
	void confSaveBegin(Configuration conf) throws SerializationException;

	/**
	 * Method is called when serialization of configuration is finished.
	 * 
	 * @param conf
	 *            configuration being saved.
	 * @throws SerializationException
	 */
	void confSaveEnd(Configuration conf) throws SerializationException;

	/**
	 * Method is called when serialization of network begins.
	 * 
	 * @param net
	 *            network being saved.
	 * @throws SerializationException
	 */
	void netSaveBegin(Network net) throws SerializationException;

	/**
	 * Method is called when serialization of network is finished.
	 * 
	 * @param net
	 *            network being saved.
	 * @throws SerializationException
	 */
	void netSaveEnd(Network net) throws SerializationException;

	/**
	 * Method is called when serialization of node begins.
	 * 
	 * @param node
	 *            node being saved.
	 * @throws SerializationException
	 */
	void nodeSaveBegin(Node node) throws SerializationException;

	/**
	 * Method is called when serialization of node is finished.
	 * 
	 * @param node
	 *            node being saved.
	 * @throws SerializationException
	 */
	void nodeSaveEnd(Node node) throws SerializationException;

	/**
	 * Method is called when serialization of sensor begins.
	 * 
	 * @param sensor
	 *            sensor being saved.
	 * @throws SerializationException
	 */
	void sensorSaveBegin(Sensor sensor) throws SerializationException;

	/**
	 * Method is called when serialization of sensor is finished.
	 * 
	 * @param sensor
	 *            sensor being saved.
	 * @throws SerializationException
	 */
	void sensorSaveEnd(Sensor sensor) throws SerializationException;

	/**
	 * When method is called, the underlaying implementation must free all
	 * associated resources.
	 */
	void close();
}
