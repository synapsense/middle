package com.synapsense.conf;

/**
 * Base class for all kinds of exceptions that can be thrown from serializers.
 * 
 * @author Oleg Stepanov
 * 
 */
public class SerializationException extends Exception {

	private static final long serialVersionUID = -1426523162011777552L;

	/**
	 * Creates exception with no description.
	 */
	public SerializationException() {
		super();
	}

	/**
	 * Creates exception with description.
	 * 
	 * @param message
	 *            exception description.
	 */
	public SerializationException(String message) {
		super(message);
	}

	/**
	 * Creates exception wrapping initial cause.
	 * 
	 * @param cause
	 *            root cause exception.
	 */
	public SerializationException(Throwable cause) {
		super(cause);
	}

	/**
	 * Creates exception using description and initial cause.
	 * 
	 * @param message
	 *            exception description.
	 * @param cause
	 *            root cause exception.
	 */
	public SerializationException(String message, Throwable cause) {
		super(message, cause);
	}

}
