package com.synapsense.conf;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Utility class for command line argument parsing.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ArgumentParser {

	/**
	 * Constructor.
	 * 
	 * @param args
	 *            command line arguments.
	 */
	public ArgumentParser(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("-")) { // || args[i].startsWith("/"))
				int loc = args[i].indexOf("=");
				String key = (loc > 0) ? args[i].substring(1, loc) : args[i].substring(1);
				String value = (loc > 0) ? args[i].substring(loc + 1) : "";
				options.put(key.toLowerCase(), value);
			} else {
				params.addElement(args[i]);
			}
		}
	}

	/**
	 * Checks if an option was specified.
	 * 
	 * @param opt
	 *            option key.
	 * @return
	 */
	public boolean hasOption(String opt) {
		return options.containsKey(opt.toLowerCase());
	}

	/**
	 * Returns a value of an option.
	 * 
	 * @param opt
	 *            option key.
	 * @return option value or null if option with given key was not specified.
	 */
	public String getOption(String opt) {
		return (String) options.get(opt.toLowerCase());
	}

	/**
	 * Method allows to iterate through command-line parameters.
	 * 
	 * @return the next parameter's value.
	 */
	public String nextParam() {
		if (paramIndex < params.size()) {
			return (String) params.elementAt(paramIndex++);
		}
		return null;
	}

	/** Parameters list. */
	private Vector<String> params = new Vector<String>();
	/** Options map. */
	private Map<String, String> options = new HashMap<String, String>();
	/** Current parameter index. */
	private int paramIndex = 0;
}
