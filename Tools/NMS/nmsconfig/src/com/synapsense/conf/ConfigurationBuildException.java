package com.synapsense.conf;

/**
 * Root class for exceptions occurred during configuration building.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ConfigurationBuildException extends Exception {

	private static final long serialVersionUID = 1572934503280574277L;

	public ConfigurationBuildException() {
	}

	public ConfigurationBuildException(String message) {
		super(message);
	}

	public ConfigurationBuildException(Throwable cause) {
		super(cause);
	}

	public ConfigurationBuildException(String message, Throwable cause) {
		super(message, cause);
	}

}
