package com.synapsense.conf.snmp;

public enum RetrievalEventStatus {

Exception("An exception occured during retrieval operation"), Ok("Retrieval operation was successfull"), Report(
        "A report has been received from the agent"), Timeout("A request to the agent timed out"), WrongOrder(
        "The agent failed to return the objects in lexicographic order"), Unknown("Unknown code returned");

private String descr;

private RetrievalEventStatus(String descr) {
	this.descr = descr;
}

public String getDescr() {
	return descr;
}

public static RetrievalEventStatus getForCode(int code) {
	switch (code) {
	case 0:
		return Ok;
	case -1:
		return Timeout;
	case -2:
		return WrongOrder;
	case -3:
		return Report;
	case -5:
		return Exception;
	default:
		return Unknown;
	}
}
}
