package com.synapsense.conf.snmp;

import java.io.IOException;

import org.snmp4j.smi.OID;
import org.snmp4j.util.TableListener;

/**
 * Interface that provides additional information on fields to be retrieved for
 * tables.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface TypedTableListener extends TableListener {
	/**
	 * @return OIDs of fields to be retrieved from the table.
	 */
	OID[] getColumns();

	/**
	 * @return any IOException occurred during filling or null otherwise.
	 */
	IOException getOccurredException();
}
