package com.synapsense.conf.snmp;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.OID;
import org.snmp4j.util.TableEvent;

import com.synapsense.conf.objects.Configuration;
import com.synapsense.conf.objects.Node;

/**
 * Class for adding nodes to the configuration.
 * 
 * @author Oleg Stepanov
 * 
 */
class NodesFiller implements TypedTableListener {

	private static final Logger log = Logger.getLogger(NodesFiller.class);

	private Configuration conf;

	private IOException e;

	/**
	 * Creates new nodes filler for a given configuration.
	 * 
	 * @param conf
	 *            configuration to add nodes.
	 * @throws IllegalArgumentException
	 *             if conf is null.
	 */
	public NodesFiller(Configuration conf) {
		if (conf == null)
			throw new IllegalArgumentException("conf must be not null");
		this.conf = conf;
	}

	@Override
	public void finished(TableEvent event) {
		RetrievalEventStatus status = RetrievalEventStatus.getForCode(event.getStatus());
		if (status != RetrievalEventStatus.Ok) {
			if (log.isEnabledFor(Level.ERROR))
				log.error("Error occured while processing nodes table: " + status.getDescr());
			e = new IOException(status.getDescr());
		} else {
			if (log.isInfoEnabled())
				log.info("Nodes table processed. Received " + event.getUserObject() + " rows.");
		}
		synchronized (event.getUserObject()) {
			event.getUserObject().notify();
		}
	}

	@Override
	public boolean next(TableEvent event) {
		if (log.isDebugEnabled())
			log.debug("Node row = " + event.getIndex() + ":");
		// get node ID
		int id = event.getColumns()[0].getVariable().toInt();
		// get parent net ID
		int net_id = (int) event.getColumns()[1].getVariable().toLong();
		// get node physical ID
		String phys_id = event.getColumns()[2].getVariable().toString();
		// get net name
		String name = event.getColumns()[3].getVariable().toString();
		// get net description
		String descr = event.getColumns()[4].getVariable().toString();

		Node node = new Node(id, name, descr, phys_id);
		try {
			conf.getNetwork(net_id).addNode(node);
		} catch (IndexOutOfBoundsException e) {
			log.warn("Found node for non-existing network, node " + node + " ignored", e);
		}

		if (log.isDebugEnabled())
			log.debug("Parsed node: " + node);

		((Counter32) event.getUserObject()).increment();
		return true;
	}

	@Override
	public OID[] getColumns() {
		return NodeOIDs.getOIDs();
	}

	@Override
	public IOException getOccurredException() {
		return e;
	}

}

/**
 * Holder class for Node fields OIDs.
 * 
 * @author Oleg Stepanov
 * 
 */
class NodeOIDs {
	private static final OID LOGICAL_ID = new OID("1.3.6.1.4.1.29078.1.1.3.1.1.1");
	private static final OID PARENT_NET_ID = new OID("1.3.6.1.4.1.29078.1.1.3.1.1.2");
	private static final OID PHYSICAL_ID = new OID("1.3.6.1.4.1.29078.1.1.3.1.1.3");
	private static final OID NAME = new OID("1.3.6.1.4.1.29078.1.1.3.1.1.4");
	private static final OID DESCR = new OID("1.3.6.1.4.1.29078.1.1.3.1.1.6");

	/**
	 * All OIDs used as columns. Warning: implementation of the NodesFiller
	 * depends on the order of columns!
	 */
	private static final OID[] allOIDs = { LOGICAL_ID, PARENT_NET_ID, PHYSICAL_ID, NAME, DESCR };

	/**
	 * @return OIDs of all fields retrieved for node.
	 */
	public static OID[] getOIDs() {
		return allOIDs;
	}
}
