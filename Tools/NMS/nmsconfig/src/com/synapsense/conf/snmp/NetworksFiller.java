package com.synapsense.conf.snmp;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.OID;
import org.snmp4j.util.TableEvent;

import com.synapsense.conf.objects.Configuration;
import com.synapsense.conf.objects.Network;

/**
 * Class for adding networks to the configuration.
 * 
 * @author Oleg Stepanov
 * 
 */
class NetworksFiller implements TypedTableListener {

	private static final Logger log = Logger.getLogger(NetworksFiller.class);

	private Configuration conf;

	private IOException e;

	/**
	 * Creates new network filler for a given configuration.
	 * 
	 * @param conf
	 *            configuration to add networks.
	 * @throws IllegalArgumentException
	 *             if conf is null.
	 */
	public NetworksFiller(Configuration conf) {
		if (conf == null)
			throw new IllegalArgumentException("conf must be not null");
		this.conf = conf;
	}

	@Override
	public void finished(TableEvent event) {
		RetrievalEventStatus status = RetrievalEventStatus.getForCode(event.getStatus());
		if (status != RetrievalEventStatus.Ok) {
			if (log.isEnabledFor(Level.ERROR))
				log.error("Error occured while processing networks table: " + status.getDescr());
			e = new IOException(status.getDescr());
		} else {
			if (log.isInfoEnabled())
				log.info("Networks table processed. Received " + event.getUserObject() + " rows.");
		}
		synchronized (event.getUserObject()) {
			event.getUserObject().notify();
		}
	}

	@Override
	public boolean next(TableEvent event) {
		if (log.isDebugEnabled())
			log.debug("Network row = " + event.getIndex() + ":");
		// get net ID
		int id = event.getColumns()[0].getVariable().toInt();
		// get net name
		String name = event.getColumns()[1].getVariable().toString();
		// get net description
		String descr = event.getColumns()[2].getVariable().toString();
		// get host
		String host = event.getColumns()[3].getVariable().toString();

		Network net = new Network(id, name, descr, host);
		conf.addNetwork(net);

		if (log.isDebugEnabled())
			log.debug("Parsed network: " + net);

		((Counter32) event.getUserObject()).increment();
		return true;
	}

	@Override
	public OID[] getColumns() {
		return NetworkOIDs.getOIDs();
	}

	@Override
	public IOException getOccurredException() {
		return e;
	}

}

/**
 * Holder class for Network fields OIDs.
 * 
 * @author Oleg Stepanov
 * 
 */
class NetworkOIDs {
	private static final OID ID = new OID("1.3.6.1.4.1.29078.1.1.1.1.1");
	private static final OID NAME = new OID("1.3.6.1.4.1.29078.1.1.1.1.2");
	private static final OID DESCR = new OID("1.3.6.1.4.1.29078.1.1.1.1.3");
	private static final OID HOST = new OID("1.3.6.1.4.1.29078.1.1.1.1.4");
	private static final OID PORT = new OID("1.3.6.1.4.1.29078.1.1.1.1.5");

	/**
	 * All OIDs used as columns. Warning: implementation of the NetworksFiller
	 * depends on the order of columns!
	 */
	private static final OID[] allOIDs = { ID, NAME, DESCR, HOST, PORT };

	/**
	 * @return OIDs of all fields retrieved for network.
	 */
	public static OID[] getOIDs() {
		return allOIDs;
	}
}
