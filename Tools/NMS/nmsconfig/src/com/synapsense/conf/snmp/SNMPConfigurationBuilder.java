package com.synapsense.conf.snmp;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.snmp4j.AbstractTarget;
import org.snmp4j.CommunityTarget;
import org.snmp4j.Snmp;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TableUtils;

import com.synapsense.GUI.guiFrame.BuildTask;
import com.synapsense.conf.ConfigurationBuildException;
import com.synapsense.conf.ConfigurationBuilder;
import com.synapsense.conf.objects.Configuration;

/**
 * Class that builds SynapSense configuration using information retrieved from
 * SNMP Agent. Only UDP transport protocol is currently supported.
 * 
 * @author Oleg Stepanov
 * 
 */
public class SNMPConfigurationBuilder implements ConfigurationBuilder {

	private Configuration conf;
	private BuildTask listener;
	private static final Logger log = Logger.getLogger(SNMPConfigurationBuilder.class);

	private Snmp snmp;
	private CommunityTarget target;

	/**
	 * Creates new builder object.
	 * 
	 * @throws IOException
	 */
	public SNMPConfigurationBuilder() throws ConfigurationBuildException {
		try {
			snmp = new Snmp(new DefaultUdpTransportMapping());
		} catch (IOException e) {
			throw new ConfigurationBuildException("Cannot get UDP transport", e);
		}
		target = new CommunityTarget();
		target.setCommunity(new OctetString("public"));
		target.setVersion(SnmpConstants.version2c);
		target.setRetries(2);
		target.setTimeout(10000);
	}

	/**
	 * returns Configuration
	 */
	public Configuration getConfiguration() throws ConfigurationBuildException {
		return conf;
	}

	/**
	 * Builds configurations.
	 * 
	 * @param nagios_host
	 *            address of Nagios server.
	 * @param nagios_port
	 *            Nagios server port.
	 * @param agent_host
	 *            address of SNMP agent server.
	 * @param agent_port
	 *            SNMP agent server port.
	 * @throws IOException
	 */
	public Configuration buildConfiguration(String nagios_host, String nagios_port, String agent_host, String agent_port)
	        throws ConfigurationBuildException {
		try {
			target.setAddress(new UdpAddress(agent_host + "/" + agent_port));
		} catch (IllegalArgumentException e) {
			throw new ConfigurationBuildException("Cannot connect to " + agent_host + ":" + agent_port);
		}

		conf = new Configuration(nagios_host, nagios_port, agent_host, agent_port);
		try {
			snmp.listen();

			if (log.isInfoEnabled())
				log.info("Retrieving configuration from \"" + agent_host + ":" + agent_port + "\"");
			if (listener != null)
				listener.changeProgress(1);
			parseRows(snmp, target, new NetworksFiller(conf));
			if (listener != null)
				listener.changeProgress(10);
			parseRows(snmp, target, new NodesFiller(conf));
			if (listener != null)
				listener.changeProgress(40);
			parseRows(snmp, target, new SensorsFiller(conf));
			if (listener != null)
				listener.changeProgress(100);
		} catch (IOException e) {
			throw new ConfigurationBuildException("Cannot read configuration", e);
		} finally {
			try {
				snmp.close();
			} catch (IOException e) {
				log.warn("Cannot close snmp listener", e);
			}
		}
		return conf;
	}

	public void addProgressListener(BuildTask listener) {
		this.listener = listener;
	}

	/**
	 * Retrieves all columns denoted by given filler.
	 * 
	 * @param snmp
	 *            object used to receive output from SNMP agent.
	 * @param target
	 *            object denoting SNMP agent target.
	 * @param filler
	 *            object used to parse SNMP agent output.
	 * @param timeout
	 *            timeout for parsing results for table.
	 */
	private static void parseRows(Snmp snmp, AbstractTarget target, TypedTableListener filler) throws IOException {
		TableUtils tableUtils = new TableUtils(snmp, new DefaultPDUFactory());
		tableUtils.setMaxNumRowsPerPDU(10);
		Counter32 counter = new Counter32();

		long startTime = System.currentTimeMillis();

		synchronized (counter) {
			tableUtils.getTable(target, filler.getColumns(), filler, counter, null, null);
			try {
				counter.wait();
			} catch (InterruptedException ex) {
			}
		}
		if (filler.getOccurredException() != null)
			throw filler.getOccurredException();
		if (log.isInfoEnabled())
			log.info("Table processing time: " + (System.currentTimeMillis() - startTime) + " milliseconds.");
	}

	/**
	 * Main function to test builder output.
	 * 
	 * @param args
	 *            - one argument is to be specified: SNMP agent address.
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		if (args.length == 0 || args.length > 2) {
			System.out.println("Supply SNMP agent <address>[/<port>] as the single parameter");
		}
		String host = args[0];
		String port = args.length > 1 ? args[1] : "161";
		SNMPConfigurationBuilder builder = new SNMPConfigurationBuilder();
		builder.buildConfiguration("localhost", "80", host, port);
		Configuration.printConfig(builder.getConfiguration());
	}

}
