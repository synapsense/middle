package com.synapsense.conf.snmp;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.OID;
import org.snmp4j.util.TableEvent;

import com.synapsense.conf.objects.Configuration;
import com.synapsense.conf.objects.Network;
import com.synapsense.conf.objects.Node;
import com.synapsense.conf.objects.Sensor;

/**
 * Class for adding sensors to the configuration.
 * 
 * @author Oleg Stepanov
 * 
 */
class SensorsFiller implements TypedTableListener {

	private static final Logger log = Logger.getLogger(SensorsFiller.class);

	private Collection<Node> nodes;

	private IOException e;

	/**
	 * Creates new nodes filler for a given configuration.
	 * 
	 * @param conf
	 *            configuration to add sensors.
	 * @throws IllegalArgumentException
	 *             if conf is null.
	 */
	public SensorsFiller(Configuration conf) {
		if (conf == null)
			throw new IllegalArgumentException("conf must be not null");
		nodes = new LinkedList<Node>();
		for (Network net : conf.getNetworks())
			nodes.addAll(net.getNodes());
	}

	@Override
	public void finished(TableEvent event) {
		RetrievalEventStatus status = RetrievalEventStatus.getForCode(event.getStatus());
		if (status != RetrievalEventStatus.Ok) {
			if (log.isEnabledFor(Level.ERROR))
				log.error("Error occured while processing sensors table: " + status.getDescr());
			e = new IOException(status.getDescr());
		} else {
			if (log.isInfoEnabled())
				log.info("Sensors table processed. Received " + event.getUserObject() + " rows.");
		}
		synchronized (event.getUserObject()) {
			event.getUserObject().notify();
		}
	}

	private static Node getNode(int id, Collection<Node> nodes) {
		for (Node node : nodes)
			if (node.getId() == id)
				return node;
		throw new IndexOutOfBoundsException("Cannot find a node with id=" + id);
	}

	@Override
	public boolean next(TableEvent event) {
		if (log.isDebugEnabled())
			log.debug("Sensor row = " + event.getIndex() + ":");
		// get sensor ID
		int id = event.getColumns()[0].getVariable().toInt();
		// get parent node ID
		int node_id = event.getColumns()[1].getVariable().toInt();
		// get parent node channel
		long node_channel = event.getColumns()[2].getVariable().toLong();
		// get sensor name
		String name = event.getColumns()[3].getVariable().toString();

		Sensor sensor = new Sensor(id, name);
		sensor.setNodeChannel(node_channel);
		try {
			getNode(node_id, nodes).addSensor(sensor);
		} catch (IndexOutOfBoundsException e) {
			log.warn("Found sensor for non-existing node, sensor " + sensor + " ignored", e);
		}

		if (log.isDebugEnabled())
			log.debug("Parsed sensor: " + sensor);

		((Counter32) event.getUserObject()).increment();
		return true;
	}

	@Override
	public OID[] getColumns() {
		return SensorOIDs.getOIDs();
	}

	@Override
	public IOException getOccurredException() {
		return e;
	}

}

/**
 * Holder class for Sensor fields OIDs.
 * 
 * @author Oleg Stepanov
 * 
 */
class SensorOIDs {
	private static final OID ID = new OID("1.3.6.1.4.1.29078.1.1.3.3.1.1.1");
	private static final OID PARENT_NODE_ID = new OID("1.3.6.1.4.1.29078.1.1.3.3.1.1.2");
	private static final OID PARENT_NODE_CH = new OID("1.3.6.1.4.1.29078.1.1.3.3.1.1.3");
	private static final OID NAME = new OID("1.3.6.1.4.1.29078.1.1.3.3.1.1.4");

	/**
	 * All OIDs used as columns. Warning: implementation of the SensorsFiller
	 * depends on the order of columns!
	 */
	private static final OID[] allOIDs = { ID, PARENT_NODE_ID, PARENT_NODE_CH, NAME };

	/**
	 * @return OIDs of all fields retrieved for sensor.
	 */
	public static OID[] getOIDs() {
		return allOIDs;
	}
}
