package com.synapsense.conf;

import com.synapsense.conf.objects.Configuration;

/**
 * Interface of an abstract configuration builder. Examples of configurations
 * builders are SNMP builder, BACnet builder etc.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface ConfigurationBuilder {
	/**
	 * @return built configuration.
	 */
	Configuration getConfiguration() throws ConfigurationBuildException;
}
