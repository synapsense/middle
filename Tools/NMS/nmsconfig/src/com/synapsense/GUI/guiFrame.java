package com.synapsense.GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.LayoutStyle;

import com.omniscient.log4jcontrib.swingappender.SwingAppender;
import com.synapsense.conf.ConfigurationBuildException;
import com.synapsense.conf.Configurator;
import com.synapsense.conf.SerializationException;
import com.synapsense.conf.objects.Configuration;
import com.synapsense.conf.objects.Network;
import com.synapsense.conf.objects.Node;
import com.synapsense.conf.objects.Sensor;

public class guiFrame extends javax.swing.JFrame implements ActionListener, PropertyChangeListener {
	private JButton buildButton;
	private JButton exitButton;
	private JPanel jPanel1;
	private JLabel addressData;
	private JLabel nameLabel;
	private JLabel aliasData;
	private JLabel nameData;
	private JLabel idData;
	private JLabel addressLabel;
	private JLabel aliasLabel;
	private JLabel idLabel;
	private JScrollPane jScrollPane1;
	private JTree confTree;
	private AbstractAction exitAction;
	private JButton saveButton;
	private String nagios_host;
	private String nagios_port;
	private String agent_host;
	private String agent_port;
	private File file;
	private Configuration conf;
	private DefaultMutableTreeNode top = new DefaultMutableTreeNode("Configuration");
	private AbstractAction saveActionButton;
	private JLabel statusLabel;
	private JProgressBar progBar;
	private JPanel commonPanel;
	private JFileChooser fc = new JFileChooser();;
	private JPanel jPanel2;
	private JButton btnConfigFile;
	private JButton btnOutputBrowse;
	private JButton btnTemplBrowse;
	private JTextField txtConfigFile;
	private JTextField txtOutputFolder;
	private JTextField txtTemplFolder;
	private JTextField txtAgentPort;
	private JTextField txtAgentAddress;
	private JTextField txtNagPort;
	private JTextField txtNagAddress;
	private JLabel lblConfigFilePath;
	private JLabel lblOutputFolder;
	private JLabel lblTemplFolder;
	private JLabel lblAgentPort;
	private JLabel lblAgentAddress;
	private JLabel lblNagPort;
	private JLabel lblNagAddress;
	private BuildTask buildTask;
	private static SwingAppender swingAppender;
	private JFrame appenderFrame;
	private JButton logButton;
	private Configurator configurer;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				guiFrame inst = new guiFrame();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
				swingAppender = new SwingAppender();
			}
		});
	}

	public guiFrame() {
		super();
		initGUI();
		setLocationRelativeTo(null);
		setVisible(true);

	}

	private void initGUI() {
		try {
			GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
			getContentPane().setLayout(thisLayout);
			thisLayout.setVerticalGroup(thisLayout.createSequentialGroup().add(getCommonPanel(), 0, 515,
			        Short.MAX_VALUE));
			thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup().addContainerGap()
			        .add(getCommonPanel(), 0, 720, Short.MAX_VALUE));
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			this.setTitle("Configurator");
			this.setPreferredSize(new java.awt.Dimension(767, 555));
			this.setResizable(false);

			pack();
			this.setSize(767, 555);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private AbstractAction getExitAction() {
		if (exitAction == null) {
			exitAction = new AbstractAction("Exit", null) {
				public void actionPerformed(ActionEvent evt) {
					System.exit(0);
				}
			};
		}
		return exitAction;
	}

	/**
	 * Returns id of the specified tree-node
	 * 
	 * @param DefaultMutableTreeNode
	 *            node
	 * @return id
	 */
	private int getNodeId(DefaultMutableTreeNode node) {
		int id;
		String nodeString = node.toString();
		int start = node.toString().indexOf(' ') + 1;
		int end = node.toString().length();
		id = Integer.parseInt(nodeString.substring(start, end));
		return id;
	}

	/**
	 * Response method to tree-node selection
	 * 
	 * @return
	 */
	private JTree getConfTree() {
		if (confTree == null) {
			// top node
			confTree = new JTree(top);
			confTree.addTreeSelectionListener(new TreeSelectionListener() {
				public void valueChanged(TreeSelectionEvent evt) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) confTree.getLastSelectedPathComponent();
					if (node == null)
						return;
					String nodeString = node.toString();
					if (nodeString == "Configuration")
						return;
					int id = getNodeId(node);
					if (nodeString.contains("Network")) {
						idData.setText(new Integer(id).toString());
						nameData.setText(conf.getNetwork(id).getName());
						aliasData.setText(conf.getNetwork(id).getAlias());
						addressData.setText(conf.getNetwork(id).getAddress());
					}
					if (nodeString.contains("Node")) {
						int networkId = getNodeId((DefaultMutableTreeNode) node.getParent());
						idData.setText(new Integer(id).toString());
						nameData.setText(conf.getNetwork(networkId).getNode(id).getName());
						aliasData.setText(conf.getNetwork(networkId).getNode(id).getAlias());
						addressData.setText("");
					}
					if (nodeString.contains("Sensor")) {
						int nodeId = getNodeId((DefaultMutableTreeNode) node.getParent());
						int networkId = getNodeId((DefaultMutableTreeNode) node.getParent().getParent());
						idData.setText(new Integer(id).toString());
						nameData.setText(conf.getNetwork(networkId).getNode(nodeId).getSensor(id).getName());
						aliasData.setText(conf.getNetwork(networkId).getNode(nodeId).getSensor(id).getAlias());
						addressData.setText("");
					}
				}
			});

		}
		return confTree;
	}

	private JTree showTree(Configuration config) {
		// if (confTree != null) {
		if (config != null) {
			// nodes
			DefaultMutableTreeNode networkNode = null;
			DefaultMutableTreeNode nodeNode = null;
			DefaultMutableTreeNode sensorNode = null;

			for (Network network : config.getNetworks()) {
				networkNode = new DefaultMutableTreeNode("Network " + network.getId());
				top.add(networkNode);
				for (Node node : network.getNodes()) {
					nodeNode = new DefaultMutableTreeNode("Node " + node.getId());
					networkNode.add(nodeNode);
					for (Sensor sensor : node.getSensors()) {
						sensorNode = new DefaultMutableTreeNode("Sensor " + sensor.getId());
						nodeNode.add(sensorNode);
					}
				}
			}
		}
		return confTree;
	}

	private JScrollPane getJScrollPane1() {
		if (jScrollPane1 == null) {
			jScrollPane1 = new JScrollPane();
			jScrollPane1.setPreferredSize(new java.awt.Dimension(3, 3));
			jScrollPane1.setBorder(BorderFactory.createTitledBorder("Configuration Tree"));
			jScrollPane1.setViewportView(getConfTree());
		}
		return jScrollPane1;
	}

	private JLabel getIdLabel() {
		if (idLabel == null) {
			idLabel = new JLabel();
			idLabel.setText("Id");
			idLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			idLabel.setForeground(new java.awt.Color(0, 0, 0));
		}
		return idLabel;
	}

	private JLabel getNameLabel() {
		if (nameLabel == null) {
			nameLabel = new JLabel();
			nameLabel.setText("Name");
			nameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return nameLabel;
	}

	private JLabel getAliasLabel() {
		if (aliasLabel == null) {
			aliasLabel = new JLabel();
			aliasLabel.setText("Alias");
			aliasLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return aliasLabel;
	}

	private JLabel getAddressLabel() {
		if (addressLabel == null) {
			addressLabel = new JLabel();
			addressLabel.setText("Address");
			addressLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return addressLabel;
	}

	private JLabel getIdData() {
		if (idData == null) {
			idData = new JLabel();
			idData.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return idData;
	}

	private JLabel getNameData() {
		if (nameData == null) {
			nameData = new JLabel();
			nameData.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return nameData;
	}

	private JLabel getAliasData() {
		if (aliasData == null) {
			aliasData = new JLabel();
			aliasData.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return aliasData;
	}

	private JLabel getAddressData() {
		if (addressData == null) {
			addressData = new JLabel();
			addressData.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return addressData;
	}

	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			GroupLayout jPanel1Layout = new GroupLayout((JComponent) jPanel1);
			jPanel1.setLayout(jPanel1Layout);
			jPanel1.setBorder(BorderFactory.createTitledBorder("Current Item Data"));
			jPanel1Layout.setVerticalGroup(jPanel1Layout
			        .createSequentialGroup()
			        .add(jPanel1Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getIdLabel(), GroupLayout.PREFERRED_SIZE, 20,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getIdData(), GroupLayout.PREFERRED_SIZE, 20,
			                        GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.UNRELATED)
			        .add(jPanel1Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getNameLabel(), GroupLayout.PREFERRED_SIZE, 20,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getNameData(), GroupLayout.PREFERRED_SIZE, 20,
			                        GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.UNRELATED)
			        .add(jPanel1Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getAliasLabel(), GroupLayout.PREFERRED_SIZE, 20,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getAliasData(), GroupLayout.PREFERRED_SIZE, 20,
			                        GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.UNRELATED)
			        .add(jPanel1Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getAddressLabel(), GroupLayout.PREFERRED_SIZE, 20,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getAddressData(), GroupLayout.PREFERRED_SIZE, 20,
			                        GroupLayout.PREFERRED_SIZE)));
			jPanel1Layout.setHorizontalGroup(jPanel1Layout
			        .createSequentialGroup()
			        .add(jPanel1Layout
			                .createParallelGroup()
			                .add(jPanel1Layout.createSequentialGroup().add(getIdLabel(), GroupLayout.PREFERRED_SIZE,
			                        60, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel1Layout.createSequentialGroup().add(getNameLabel(), GroupLayout.PREFERRED_SIZE,
			                        60, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel1Layout.createSequentialGroup().add(getAliasLabel(), GroupLayout.PREFERRED_SIZE,
			                        60, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel1Layout.createSequentialGroup().add(getAddressLabel(),
			                        GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
			        .add(17)
			        .add(jPanel1Layout
			                .createParallelGroup()
			                .add(jPanel1Layout.createSequentialGroup().add(getIdData(), GroupLayout.PREFERRED_SIZE,
			                        160, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel1Layout.createSequentialGroup().add(getNameData(), GroupLayout.PREFERRED_SIZE,
			                        160, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel1Layout.createSequentialGroup().add(getAliasData(), GroupLayout.PREFERRED_SIZE,
			                        160, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel1Layout.createSequentialGroup().add(getAddressData(),
			                        GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))));
		}
		return jPanel1;
	}

	private JButton getLogButton() {
		if (logButton == null) {
			logButton = new JButton();
			logButton.setText("Logs");
			logButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					if (appenderFrame == null) {
						appenderFrame = swingAppender.getAppenderUI().getJframe();
					}
					appenderFrame.setVisible(true);
				}
			});
			logButton.setEnabled(true);
		}
		return logButton;
	}

	private AbstractAction getSaveActionButton() {
		if (saveActionButton == null) {
			saveActionButton = new AbstractAction("Write Config", null) {
				public void actionPerformed(ActionEvent evt) {
					statusLabel.setText("");
					commonPanel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					configurer.setTemplateBase(txtTemplFolder.getText());
					configurer.setOutputBase(txtOutputFolder.getText());
					configurer.setConfFileName(txtConfigFile.getText());
					try {
						configurer.saveConfig();
					} catch (SerializationException e) {
						commonPanel.setCursor(null);
						statusLabel.setForeground(new Color(255, 0, 175));
						String msg = "Cannot write configuration: " + e.getMessage();
						statusLabel.setText(msg);
						Toolkit.getDefaultToolkit().beep();
						JOptionPane.showMessageDialog(null, msg);
						return;

					}
					statusLabel.setForeground(new Color(0, 0, 0));
					statusLabel.setText("Written successfully");
					JOptionPane.showMessageDialog(null, "Written successfully");
					commonPanel.setCursor(null);

				}
			};
		}
		return saveActionButton;
	}

	private JLabel getLblNagAddress() {
		if (lblNagAddress == null) {
			lblNagAddress = new JLabel();
			lblNagAddress.setText("Nagios Host");
			lblNagAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblNagAddress;
	}

	private JLabel getLblNagPort() {
		if (lblNagPort == null) {
			lblNagPort = new JLabel();
			lblNagPort.setText("Nagios Port");
			lblNagPort.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblNagPort;
	}

	private JLabel getLblAgentAddress() {
		if (lblAgentAddress == null) {
			lblAgentAddress = new JLabel();
			lblAgentAddress.setText("Agent Host");
			lblAgentAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblAgentAddress;
	}

	private JLabel getLblAgentPort() {
		if (lblAgentPort == null) {
			lblAgentPort = new JLabel();
			lblAgentPort.setText("Agent Port");
			lblAgentPort.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblAgentPort;
	}

	private JLabel getLblTemplFolder() {
		if (lblTemplFolder == null) {
			lblTemplFolder = new JLabel();
			lblTemplFolder.setText("Templates Folder");
			lblTemplFolder.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblTemplFolder;
	}

	private JLabel getLblOutputFolder() {
		if (lblOutputFolder == null) {
			lblOutputFolder = new JLabel();
			lblOutputFolder.setText("Output Folder");
			lblOutputFolder.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblOutputFolder;
	}

	private JLabel getLblConfigFilePath() {
		if (lblConfigFilePath == null) {
			lblConfigFilePath = new JLabel();
			lblConfigFilePath.setText("Config File");
			lblConfigFilePath.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblConfigFilePath;
	}

	private JTextField getTxtNagAddress() {
		if (txtNagAddress == null) {
			txtNagAddress = new JTextField();
			txtNagAddress.setHorizontalAlignment(SwingConstants.LEFT);
			txtNagAddress.setText("localhost");
			txtNagAddress.setToolTipText("DNS name or IP-address of the server with  Nagios manager running on it");

		}
		return txtNagAddress;
	}

	private JTextField getTxtNagPort() {
		if (txtNagPort == null) {
			txtNagPort = new JTextField();
			txtNagPort.setHorizontalAlignment(SwingConstants.RIGHT);
			txtNagPort.setText("80");
			txtNagPort.setToolTipText("Port that the Nagios  is running on.  Default is 80.");

		}
		return txtNagPort;
	}

	private JTextField getTxtAgentAddress() {
		if (txtAgentAddress == null) {
			txtAgentAddress = new JTextField();
			txtAgentAddress.setHorizontalAlignment(SwingConstants.LEFT);
			txtAgentAddress.setToolTipText("DNS name or IP-address of the server with, Synapse SNMP agent running on.");

		}
		return txtAgentAddress;
	}

	private JTextField getTxtAgentPort() {
		if (txtAgentPort == null) {
			txtAgentPort = new JTextField();
			txtAgentPort.setHorizontalAlignment(SwingConstants.RIGHT);
			txtAgentPort.setText("161");
			txtAgentPort.setToolTipText("Port that the Agent is running on.  Default is 161.");

		}
		return txtAgentPort;
	}

	private JTextField getTxtTemplFolder() {
		if (txtTemplFolder == null) {
			txtTemplFolder = new JTextField();
			txtTemplFolder.setHorizontalAlignment(SwingConstants.LEFT);
			txtTemplFolder.setText("templates/Nagios");
			txtTemplFolder.setToolTipText("Full or relative path to the nagios configuration  template files.");

		}
		return txtTemplFolder;
	}

	private JTextField getTxtOutputFolder() {
		if (txtOutputFolder == null) {
			txtOutputFolder = new JTextField();
			txtOutputFolder.setHorizontalAlignment(SwingConstants.LEFT);
			txtOutputFolder.setText("output/Nagios");
			txtOutputFolder
			        .setToolTipText("Full or relative path of the folder to save  the resulting configuration files.");

		}
		return txtOutputFolder;
	}

	private JTextField getTxtConfigFile() {
		if (txtConfigFile == null) {
			txtConfigFile = new JTextField();
			txtConfigFile.setHorizontalAlignment(SwingConstants.LEFT);
			txtConfigFile.setText("conf/serconf.xml");
			txtConfigFile.setToolTipText("Full or relative name to the serializers  configuration file.");

		}
		return txtConfigFile;
	}

	/**
	 * This method opens a chooseFile dialog window and returns the selected
	 * file(or folder depending on isDir) path or null if canceled. Also it
	 * indicates the selection in the appropriate textField.
	 * 
	 * @param isDir
	 *            - specifies whether file or directory required.
	 * @param textField
	 * @return
	 */
	private String chooseFile(Boolean isDir, JTextField textField) {
		if (isDir) {
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		} else {
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		}
		int returnVal = fc.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
			if (file == null)
				return null;
			String abPath = file.getAbsolutePath();
			textField.setText(abPath);
			return abPath;
		} else {
			return null;
		}

	}

	/**
	 * Creates a button to choose Templates
	 * 
	 * @return
	 */
	private JButton getBtnTemplBrowse() {
		if (btnTemplBrowse == null) {
			btnTemplBrowse = new JButton();
			btnTemplBrowse.setText("Browse");
			btnTemplBrowse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					chooseFile(true, txtTemplFolder);
				}
			});
		}
		return btnTemplBrowse;
	}

	/**
	 * Creates a button to choose an Output
	 * 
	 * @return
	 */
	private JButton getBtnOutputBrowse() {
		if (btnOutputBrowse == null) {
			btnOutputBrowse = new JButton();
			btnOutputBrowse.setText("Browse");
			btnOutputBrowse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					chooseFile(true, txtOutputFolder);
				}
			});
		}
		return btnOutputBrowse;
	}

	/**
	 * Creates a button to choose a ConfigFile
	 * 
	 * @return
	 */
	private JButton getBtnConfigFile() {
		if (btnConfigFile == null) {
			btnConfigFile = new JButton();
			btnConfigFile.setText("Browse");
			btnConfigFile.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					chooseFile(false, txtConfigFile);
				}
			});
		}
		return btnConfigFile;
	}

	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			jPanel2 = new JPanel();
			GroupLayout jPanel2Layout = new GroupLayout((JComponent) jPanel2);
			jPanel2.setLayout(jPanel2Layout);
			jPanel2.setBorder(BorderFactory.createTitledBorder("Settings"));
			jPanel2Layout.setVerticalGroup(jPanel2Layout
			        .createSequentialGroup()
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getLblNagAddress(), GroupLayout.PREFERRED_SIZE, 21,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getTxtNagAddress(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.RELATED)
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getLblNagPort(), GroupLayout.PREFERRED_SIZE, 21,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getTxtNagPort(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.RELATED)
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getLblAgentAddress(), GroupLayout.PREFERRED_SIZE, 21,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getTxtAgentAddress(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.RELATED)
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getLblAgentPort(), GroupLayout.PREFERRED_SIZE, 21,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getTxtAgentPort(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.RELATED)
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getLblTemplFolder(), GroupLayout.PREFERRED_SIZE, 21,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getTxtTemplFolder(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getBtnTemplBrowse(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.RELATED)
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getLblOutputFolder(), GroupLayout.PREFERRED_SIZE, 21,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getTxtOutputFolder(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getBtnOutputBrowse(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.RELATED)
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(GroupLayout.BASELINE, getLblConfigFilePath(), GroupLayout.PREFERRED_SIZE, 21,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getTxtConfigFile(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getBtnConfigFile(), GroupLayout.PREFERRED_SIZE,
			                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
			jPanel2Layout.setHorizontalGroup(jPanel2Layout
			        .createSequentialGroup()
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(jPanel2Layout.createSequentialGroup().add(getLblNagAddress(),
			                        GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getLblNagPort(), GroupLayout.PREFERRED_SIZE,
			                        115, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getLblAgentAddress(),
			                        GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getLblAgentPort(),
			                        GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getLblTemplFolder(),
			                        GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getLblOutputFolder(),
			                        GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getLblConfigFilePath(),
			                        GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)))
			        .addPreferredGap(LayoutStyle.RELATED)
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(jPanel2Layout.createSequentialGroup().add(getTxtNagAddress(),
			                        GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getTxtNagPort(), GroupLayout.PREFERRED_SIZE,
			                        59, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getTxtAgentAddress(),
			                        GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getTxtAgentPort(),
			                        GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getTxtTemplFolder(),
			                        GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getTxtOutputFolder(),
			                        GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getTxtConfigFile(),
			                        GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)))
			        .addPreferredGap(LayoutStyle.RELATED)
			        .add(jPanel2Layout
			                .createParallelGroup()
			                .add(jPanel2Layout.createSequentialGroup().add(getBtnTemplBrowse(),
			                        GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getBtnOutputBrowse(),
			                        GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
			                .add(jPanel2Layout.createSequentialGroup().add(getBtnConfigFile(),
			                        GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))));
		}
		return jPanel2;
	}

	// start JPanel
	private JPanel getCommonPanel() {
		if (commonPanel == null) {
			commonPanel = new JPanel();
			GroupLayout jPanel3Layout = new GroupLayout((JComponent) commonPanel);
			commonPanel.setLayout(jPanel3Layout);
			{
				exitButton = new JButton();
				exitButton.setText("Exit");
				exitButton.setAction(getExitAction());
				exitButton.setSize(50, 21);
			}
			{
				buildButton = new JButton();
				buildButton.setText("Build Config");
				buildButton.addActionListener(this);
				buildButton.setIconTextGap(1);
				buildButton.setFont(new java.awt.Font("Tahoma", 1, 11));
			}
			{
				saveButton = new JButton();
				saveButton.setText("Write Config");
				saveButton.setAction(getSaveActionButton());
				saveButton.setFont(new java.awt.Font("Tahoma", 1, 11));
				saveButton.setEnabled(false);
			}
			jPanel3Layout.setHorizontalGroup(jPanel3Layout
			        .createSequentialGroup()
			        .add(jPanel3Layout
			                .createParallelGroup()
			                .add(jPanel3Layout
			                        .createSequentialGroup()
			                        .add(jPanel3Layout
			                                .createParallelGroup()
			                                .add(GroupLayout.LEADING, getJScrollPane1(), GroupLayout.PREFERRED_SIZE,
			                                        245, GroupLayout.PREFERRED_SIZE)
			                                .add(GroupLayout.LEADING,
			                                        jPanel3Layout
			                                                .createSequentialGroup()
			                                                .add(buildButton, GroupLayout.PREFERRED_SIZE, 112,
			                                                        GroupLayout.PREFERRED_SIZE)
			                                                .addPreferredGap(LayoutStyle.UNRELATED)
			                                                .add(saveButton, GroupLayout.PREFERRED_SIZE, 112,
			                                                        GroupLayout.PREFERRED_SIZE).add(9)))
			                        .add(jPanel3Layout
			                                .createParallelGroup()
			                                .add(GroupLayout.LEADING,
			                                        jPanel3Layout
			                                                .createSequentialGroup()

			                                                .add(getLogButton(), GroupLayout.PREFERRED_SIZE, 112,
			                                                        GroupLayout.PREFERRED_SIZE)
			                                                .addPreferredGap(LayoutStyle.RELATED, 0, Short.MAX_VALUE)
			                                                .add(getProgBar(), GroupLayout.PREFERRED_SIZE, 206,
			                                                        GroupLayout.PREFERRED_SIZE)
			                                                .addPreferredGap(LayoutStyle.UNRELATED)
			                                                .add(exitButton, GroupLayout.PREFERRED_SIZE, 112,
			                                                        GroupLayout.PREFERRED_SIZE))
			                                .add(jPanel3Layout
			                                        .createSequentialGroup()
			                                        .add(18)
			                                        .add(jPanel3Layout
			                                                .createParallelGroup()
			                                                .add(GroupLayout.LEADING,
			                                                        jPanel3Layout
			                                                                .createSequentialGroup()
			                                                                .add(getJPanel1(),
			                                                                        GroupLayout.PREFERRED_SIZE, 272,
			                                                                        GroupLayout.PREFERRED_SIZE)
			                                                                .add(157))
			                                                .add(GroupLayout.LEADING, getJPanel2(),
			                                                        GroupLayout.PREFERRED_SIZE, 429,
			                                                        GroupLayout.PREFERRED_SIZE)))))
			                .add(GroupLayout.LEADING, getStatusLabel(), GroupLayout.PREFERRED_SIZE, 692,
			                        GroupLayout.PREFERRED_SIZE)).addContainerGap(54, 54));
			jPanel3Layout.linkSize(new Component[] { buildButton, saveButton, getLogButton(), exitButton },
			        GroupLayout.HORIZONTAL);
			jPanel3Layout.setVerticalGroup(jPanel3Layout
			        .createSequentialGroup()
			        .add(jPanel3Layout
			                .createParallelGroup()
			                .add(GroupLayout.LEADING, getJScrollPane1(), GroupLayout.PREFERRED_SIZE, 412,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.LEADING,
			                        jPanel3Layout
			                                .createSequentialGroup()
			                                .add(12)
			                                .add(getJPanel1(), GroupLayout.PREFERRED_SIZE, 144,
			                                        GroupLayout.PREFERRED_SIZE)
			                                .add(31)
			                                .add(getJPanel2(), GroupLayout.PREFERRED_SIZE, 225,
			                                        GroupLayout.PREFERRED_SIZE)))
			        .add(43)
			        .add(jPanel3Layout
			                .createParallelGroup(GroupLayout.BASELINE)
			                .add(GroupLayout.BASELINE, getLogButton(), GroupLayout.PREFERRED_SIZE, 22,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, exitButton, GroupLayout.PREFERRED_SIZE, 22,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, buildButton, GroupLayout.PREFERRED_SIZE, 22,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, saveButton, GroupLayout.PREFERRED_SIZE, 22,
			                        GroupLayout.PREFERRED_SIZE)
			                .add(GroupLayout.BASELINE, getProgBar(), GroupLayout.PREFERRED_SIZE, 17,
			                        GroupLayout.PREFERRED_SIZE))
			        .addPreferredGap(LayoutStyle.UNRELATED, 1, GroupLayout.PREFERRED_SIZE)
			        .add(getStatusLabel(), 0, 20, Short.MAX_VALUE).addContainerGap());
			jPanel3Layout.linkSize(new Component[] { buildButton, saveButton, getLogButton(), exitButton },
			        GroupLayout.VERTICAL);
		}
		return commonPanel;
	}

	private JProgressBar getProgBar() {
		if (progBar == null) {
			progBar = new JProgressBar(0, 100);
			progBar.setValue(0);
			progBar.setStringPainted(true);
			progBar.setVisible(false);
		}
		return progBar;
	}

	/**
	 * Starts after Build Config button pushed
	 * 
	 */
	public void actionPerformed(ActionEvent evt) {
		nagios_host = txtNagAddress.getText();
		nagios_port = txtNagPort.getText();
		agent_host = txtAgentAddress.getText();
		agent_port = txtAgentPort.getText();
		buildButton.setEnabled(false);
		statusLabel.setForeground(new Color(0, 0, 0));
		statusLabel.setText("");
		saveButton.setEnabled(false);
		logButton.setEnabled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		// progBar.setVisible(true);
		buildTask = new BuildTask();
		buildTask.addPropertyChangeListener(this);
		buildTask.execute();

	}

	/**
	 * Processes changes in progress when configuration building
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		// System.out.println("Property changed to ");
		if ("progress" == evt.getPropertyName()) {
			int progress = (Integer) evt.getNewValue();
			progBar.setValue(progress);
			switch (progress) {
			case 1:
				statusLabel.setText("Networks retrieving...");
				break;
			case 10:
				statusLabel.setText("Nodes retrieving...");
				break;
			case 40:
				statusLabel.setText("Sensors retrieving...");
				break;
			}

		}
	}

	private JLabel getStatusLabel() {
		if (statusLabel == null) {
			statusLabel = new JLabel();
			statusLabel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return statusLabel;
	}

	/**
	 * this is the class for Configuration building task
	 * 
	 * @author mochalov
	 * 
	 */
	public class BuildTask extends SwingWorker<Void, Void> implements ProgressListener {
		private boolean built = false;

		public BuildTask() {
		}

		public Void doInBackground() {
			progBar.setVisible(true);
			setProgress(0);
			String[] arg = new String[5];
			arg[0] = "-t=" + txtTemplFolder.getText();
			arg[1] = "-o=" + txtOutputFolder.getText();
			arg[2] = txtConfigFile.getText();
			arg[3] = nagios_host + ":" + nagios_port;
			arg[4] = agent_host + ":" + agent_port;
			configurer = new Configurator(arg);
			try {
				conf = configurer.buildConfig(buildTask);
				built = true;
			} catch (ConfigurationBuildException e) {
				statusLabel.setForeground(new Color(255, 0, 175));
				String msg = e.getMessage();
				if (e.getCause() != null)
					msg += ": " + e.getCause().getMessage();
				statusLabel.setText(msg);
				Toolkit.getDefaultToolkit().beep();
				JOptionPane.showMessageDialog(null, msg);
				logButton.setEnabled(true);
				return null;
			}
			statusLabel.setForeground(new Color(0, 0, 0));
			progBar.setVisible(true);
			statusLabel.setText("Built successfully");
			JOptionPane.showMessageDialog(null, "Built successfully");
			return null;
		}

		public void done() {
			if (built) {
				showTree(conf);
				saveButton.setEnabled(true);
				logButton.setEnabled(true);
			}
			progBar.setVisible(false);
			buildButton.setEnabled(true);
			setCursor(null);
			setProgress(0);
		}

		@Override
		public void changeProgress(int prog) {
			setProgress(prog);

		}

	}
}
