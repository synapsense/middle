Name "Synapsense NMS Configurator"

SetCompressor lzma

# Defines
!define REGKEY "SOFTWARE\$(^Name)"
!define VERSION "$SYNAPVER$ - $SYNAPREV$"
!define COMPANY SynapSense
!define URL http://www.synapsense.com/

# MUI defines
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_FINISHPAGE_SHOWREADME $INSTDIR\Readme.txt
!define MUI_UNFINISHPAGE_NOAUTOCLOSE

# Included files
!include Sections.nsh
!include MUI.nsh

# Variables
Var StartMenuGroup

# Installer pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE License.txt
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

# Installer languages
!insertmacro MUI_LANGUAGE English
!insertmacro MUI_LANGUAGE German
!insertmacro MUI_LANGUAGE French
!insertmacro MUI_LANGUAGE Russian

# Installer attributes
OutFile nmsconf_setup.exe
InstallDir "$PROGRAMFILES\SynapNMSConf"
CRCCheck on
XPStyle on
ShowInstDetails show
VIProductVersion 1.0.0.0
VIAddVersionKey /LANG=${LANG_ENGLISH} ProductName "Synapsense NMS Configurator"
VIAddVersionKey /LANG=${LANG_ENGLISH} ProductVersion "${VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} CompanyName "${COMPANY}"
VIAddVersionKey /LANG=${LANG_ENGLISH} CompanyWebsite "${URL}"
VIAddVersionKey /LANG=${LANG_ENGLISH} FileVersion "${VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} FileDescription ""
VIAddVersionKey /LANG=${LANG_ENGLISH} LegalCopyright ""
InstallDirRegKey HKLM "${REGKEY}" Path
ShowUninstDetails hide

# Installer sections
Section "-NMS Configurator" SEC0000
    SetOutPath $INSTDIR\bin
    SetOverwrite on
    File /r bin\*.bat
    File /r bin\*.sh
    SetOutPath $INSTDIR\conf
    File /r conf\*.xml
    SetOutPath $INSTDIR\lib
    File /r lib\*.jar
    File /r lib\log4j.properties
    SetOutPath $INSTDIR\logs
    SetOutPath $INSTDIR\Nagios\logos
    File /r ..\Nagios\logos\*.gd2
    File /r ..\Nagios\logos\*.gif
    File /r ..\Nagios\logos\*.png
    SetOutPath $INSTDIR\Nagios\plugins\cgi
    File /r ..\Nagios\plugins\cgi\*.pl
    SetOutPath $INSTDIR\Nagios\plugins\libexec
    File /r ..\Nagios\plugins\libexec\a_check*
    File /r ..\Nagios\plugins\libexec\b_check*
    SetOutPath $INSTDIR\output\nagios
    SetOutPath $INSTDIR\output\notes
    SetOutPath $INSTDIR\templates\Nagios
    File /r templates\Nagios\*.tpl
    SetOutPath $INSTDIR
    File License.txt
    File Readme.txt
    WriteRegStr HKLM "${REGKEY}\Components" "NMS Configurator" 1
SectionEnd

Section -post SEC0001
    WriteRegStr HKLM "${REGKEY}" Path $INSTDIR
    SetOutPath $INSTDIR
    WriteUninstaller $INSTDIR\nmsconf_uninstall.exe
    SetOutPath $SMPROGRAMS\$StartMenuGroup
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\$(^StartNMSConfig).lnk" $INSTDIR\bin\nmsConfGui.bat
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\$(^EditConfiguration).lnk" "$WINDIR\notepad.exe" "$INSTDIR\conf\serconf.xml"
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\$(^ViewReadmeFile).lnk" "$WINDIR\notepad.exe" "$INSTDIR\Readme.txt"
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\$(^ViewLogFiles).lnk" "$WINDIR\explorer.exe" "$INSTDIR\logs\"
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\$(^UninstallLink).lnk" $INSTDIR\nmsconf_uninstall.exe
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayName "$(^Name)"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayVersion "${VERSION}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" Publisher "${COMPANY}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" URLInfoAbout "${URL}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayIcon $INSTDIR\nmsconf_uninstall.exe
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" UninstallString $INSTDIR\nmsconf_uninstall.exe
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoModify 1
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoRepair 1
SectionEnd

# Macro for selecting uninstaller sections
!macro SELECT_UNSECTION SECTION_NAME UNSECTION_ID
    Push $R0
    ReadRegStr $R0 HKLM "${REGKEY}\Components" "${SECTION_NAME}"
    StrCmp $R0 1 0 next${UNSECTION_ID}
    !insertmacro SelectSection "${UNSECTION_ID}"
    GoTo done${UNSECTION_ID}
next${UNSECTION_ID}:
    !insertmacro UnselectSection "${UNSECTION_ID}"
done${UNSECTION_ID}:
    Pop $R0
!macroend

# Uninstaller sections
Section /o "-un.NMS Configurator" UNSEC0000
    Delete /REBOOTOK $INSTDIR\Readme.txt
    Delete /REBOOTOK $INSTDIR\License.txt
    RmDir /r /REBOOTOK $INSTDIR\templates
    RmDir /r /REBOOTOK $INSTDIR\output
    RmDir /r /REBOOTOK $INSTDIR\Nagios
    RmDir /r /REBOOTOK $INSTDIR\logs
    RmDir /r /REBOOTOK $INSTDIR\lib
    RmDir /r /REBOOTOK $INSTDIR\conf
    RmDir /r /REBOOTOK $INSTDIR\bin
    DeleteRegValue HKLM "${REGKEY}\Components" "NMS Configurator"
SectionEnd

Section -un.post UNSEC0001
    DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)"
    Delete /REBOOTOK "$SMPROGRAMS\$StartMenuGroup\$(^StartNMSConfig).lnk"
    Delete /REBOOTOK "$SMPROGRAMS\$StartMenuGroup\$(^EditConfiguration).lnk"
    Delete /REBOOTOK "$SMPROGRAMS\$StartMenuGroup\$(^ViewReadmeFile).lnk"
    Delete /REBOOTOK "$SMPROGRAMS\$StartMenuGroup\$(^ViewLogFiles).lnk"
    Delete /REBOOTOK "$SMPROGRAMS\$StartMenuGroup\$(^UninstallLink).lnk"
    Delete /REBOOTOK $INSTDIR\nmsconf_uninstall.exe
    DeleteRegValue HKLM "${REGKEY}" Path
    DeleteRegKey /IfEmpty HKLM "${REGKEY}\Components"
    DeleteRegKey /IfEmpty HKLM "${REGKEY}"
    RmDir /REBOOTOK $SMPROGRAMS\$StartMenuGroup
    RmDir /REBOOTOK $INSTDIR
SectionEnd

# Installer functions
Function .onInit
    InitPluginsDir
    StrCpy $StartMenuGroup "Synapsense NMS Configurator"
FunctionEnd

# Uninstaller functions
Function un.onInit
    ReadRegStr $INSTDIR HKLM "${REGKEY}" Path
    StrCpy $StartMenuGroup "Synapsense NMS Configurator"
    !insertmacro SELECT_UNSECTION "NMS Configurator" ${UNSEC0000}
FunctionEnd

# Installer Language Strings
# TODO Update the Language Strings with the appropriate translations.

LangString ^StartNMSConfig ${LANG_ENGLISH} "Start NMS Configurator"
LangString ^StartNMSConfig ${LANG_GERMAN} "Start NMS Configurator"
LangString ^StartNMSConfig ${LANG_FRENCH} "Start NMS Configurator"
LangString ^StartNMSConfig ${LANG_RUSSIAN} "Start NMS Configurator"

LangString ^EditConfiguration ${LANG_ENGLISH} "Edit Configuration"
LangString ^EditConfiguration ${LANG_GERMAN} "Edit Configuration"
LangString ^EditConfiguration ${LANG_FRENCH} "Edit Configuration"
LangString ^EditConfiguration ${LANG_RUSSIAN} "Edit Configuration"

LangString ^ViewReadmeFile ${LANG_ENGLISH} "View Readme File"
LangString ^ViewReadmeFile ${LANG_GERMAN} "View Readme File"
LangString ^ViewReadmeFile ${LANG_FRENCH} "View Readme File"
LangString ^ViewReadmeFile ${LANG_RUSSIAN} "View Readme File"

LangString ^ViewLogFiles ${LANG_ENGLISH} "View Log Files"
LangString ^ViewLogFiles ${LANG_GERMAN} "View Log Files"
LangString ^ViewLogFiles ${LANG_FRENCH} "View Log Files"
LangString ^ViewLogFiles ${LANG_RUSSIAN} "View Log Files"

LangString ^UninstallLink ${LANG_ENGLISH} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_GERMAN} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_FRENCH} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_RUSSIAN} "Uninstall $(^Name)"
