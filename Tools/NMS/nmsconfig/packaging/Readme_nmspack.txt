Packages are built using dpkg-buildpackage utility. When started it searches 
"debian" directory in the current path. "debian" directory should contain the 
files with instructions to build the packages. For more information about the 
process of packages creation in Debian Linux see:
		man dpkg-buildpackage
		man debhelper
		http://www.debian.org/doc/

For packages management and update see:
		man dpkg		
		man dselect
		man apt-get
		man sources.list
		http://www.debian.org/doc/

==============================================================================
=    Files description in packaging directory                                =
==============================================================================

------------------------------------------------------------------------------
-    Build scripts                                                           -
------------------------------------------------------------------------------

debian.build  
	This script starts build process for the package with a given name.
	
	Usage: debian.build pack-name [ parameters for dpkg-buildpackage ]

	1) Symbolic link "debian" for directory "debian-pack-name" is created 
	2) dpkg-buildpackage script is started with certain parameters
	3) All previously created symlinks are deleted

debian-all.build
	Runs scripts:
	debian-synapnms-configurator.build
	
debian-synapnms-configurator.build
	Runs debian.build with the 1st parameter = synapnms-configurator

------------------------------------------------------------------------------
-   Files required for assembling packages                                   -
-                                                                            -
-  {pack-name} - package name                                                -
-       (synapsnmp, synapsnmp-service, synapsnmp-samples)                    -
-                                                                            -
-  {sub-pack-name} - current package name in multipack                       -
-       or main packange name in singlepack (synapsnmp, synapsnmp-doc,       -
-       synapsnmp-service, synapsnmp-samples)                                -
------------------------------------------------------------------------------

debian-{pack-name}/compat
	Defines compatibility level for current files.
	See: man debhelper

debian-{pack-name}/changelog
	Contains current package version and chamges description.
	
debian-{pack-name}/control
	Contains names, descriptions and packages dependencies.

debian-{pack-name}/copyright
	Contains license.

debian-{pack-name}/{sub-pack-name}.conffiles
	Contains list of package configuration files.

debian-{pack-name}/{sub-pack-name}.dirs
	Contains list of directories required for package.
	These directories are created in package assembly directory 
	before starting rules.

debian-synapnms-configurator/rules
	Makefile for assembling and isnatllation of packages. Targets 
	description see in documentation.
	1) Copies source files in package assembly directories.
	2) Replaces '\' symbols by '/' in all files listed in conf/w2u-path.tmpl.
	3) Converts all files listed in conf/w2u.tmpl from Windows to Unix format.
	4) Removes SVN-specific directories with names listed in conf/svn.tmpl 
	   from package assembly directories (if any exist).

debian-synapnms-configurator/synapnms-configurator.postinst
	Shell-script running just after synapnms package installation.
	Creates /usr/bin/synapnms-configurator symlink for 
	/opt/synapnms-configurator/bin/configurator.sh file.

debian-synapsnmp/synapsnmp.prerm
	Shell-script starting just before removing of synapnms package.
	Removes previously created symlink from /usr/bin.

------------------------------------------------------------------------------
-    Auxiliary scripts used in packages building process                     -
------------------------------------------------------------------------------

tools/w2u-path
	Replaces all '\' symbols by '/' in given files.
	
	Usage: w2u-path file1 ... fileN
	
tools/execbyname
	Executes the following command for each given template:

	find $PTH -type $TYPE -name "$1" -exec $UTIL {} \;
	   
	Usage: execbyname [options] util [name1 ... nameN]

    -? -h[elp]        Print this help and exit.
    -p=path           Full or relative paths for recursive scanning and processing
                      default: current directory
    -t=file           File whis filenames or name templates. 
                      (Each template in a file should begin with the new line
    -f                Find only files
    -d                Find only directories
    util              Processing Util
    name              Filenames or name templates


tools/w2u
	Coverts given files from cp1251 to utf8 code page and replaces DOS-like 
	line ends by Unix-like.
	
	Usage: w2u file1 ... fileN

------------------------------------------------------------------------------
-    Templates used by execbyname utility                                    -
------------------------------------------------------------------------------

conf/w2u.tmpl
	Templates for text files conversion (changing code page).

conf/w2u-path.tmpl
	Templates for path conversion (delimiter replacement).
	
conf/svn.tmpl
	Templates for SVN-directories removal.
