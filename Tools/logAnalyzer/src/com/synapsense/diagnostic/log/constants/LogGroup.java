package com.synapsense.diagnostic.log.constants;

import java.util.HashMap;
import java.util.Map;

public class LogGroup {
	public static final String LOGGER = "LOGGER";
	public static final String MESSAGE = "MESSAGE";
	public static final String TIMESTAMP = "TIMESTAMP";
	public static final String NDC = "NDC";
	public static final String LEVEL = "LEVEL";
	public static final String THREAD = "THREAD";
	public static final String CLASS = "CLASS";
	public static final String FILE = "FILE";
	public static final String LINE = "LINE";
	public static final String METHOD = "METHOD";
	public static final String RELATIVETIME = "RELATIVETIME";
	public static final String EMPTY = "";
	public static final String THROWABLE = "THROWABLE";

	public static final Map<String, String> groups = new HashMap<String, String>();
	static {
		groups.put("c", LOGGER);
		groups.put("C", CLASS);
		groups.put("d", TIMESTAMP);
		groups.put("l", FILE);
		groups.put("F", FILE);
		groups.put("L", LINE);
		groups.put("m", MESSAGE);
		groups.put("M", METHOD);
		groups.put("p", LEVEL);
		groups.put("r", RELATIVETIME);
		groups.put("t", THREAD);
		groups.put("x", NDC);
		groups.put("T", THROWABLE);
		groups.put("n", LogGroup.EMPTY);
	}

	public static String getGroup(String key) {
		return groups.get(key);
	}
}
