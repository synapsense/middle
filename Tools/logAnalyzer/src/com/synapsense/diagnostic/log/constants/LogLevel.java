package com.synapsense.diagnostic.log.constants;

public class LogLevel {
	public static final String INFO = "INFO";
	public static final String DEBUG = "DEBUG";
	public static final String TRACE = "TRACE";
	public static final String WARN = "WARN";
}
