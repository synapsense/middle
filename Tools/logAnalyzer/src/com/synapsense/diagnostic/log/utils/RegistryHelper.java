package com.synapsense.diagnostic.log.utils;

import java.io.InputStreamReader;

public class RegistryHelper {
	private static final String REGQUERY_UTIL = "reg query ";

	public static final String REGEXPANDSZ_TOKEN = "REG_EXPAND_SZ";
	public static final String REGDWORD_TOKEN = "REG_DWORD";
	public static final String REGSZ_TOKEN = "REG_SZ";
	public static final String REGMULTISZ_TOKEN = "REG_MULTI_SZ";

	public static String readValue(String key, String value, String token) {
		String result = null;
		try {
			StringBuilder command = new StringBuilder();
			command.append(REGQUERY_UTIL);
			command.append(key);
			command.append(" /v ");
			command.append(value);

			Process process = Runtime.getRuntime().exec(command.toString());
			InputStreamReader reader = new InputStreamReader(process.getInputStream());

			char[] buffer = new char[1000];
			int len = reader.read(buffer);

			if (len != -1) {
				StringBuffer strbuf = new StringBuffer();
				strbuf.append(buffer, 0, len);

				String temp = strbuf.toString();
				int pos = temp.indexOf(token);
				if (pos != -1) {
					result = temp.substring(pos + token.length()).trim();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
}
