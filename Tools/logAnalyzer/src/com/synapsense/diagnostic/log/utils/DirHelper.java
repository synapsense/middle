package com.synapsense.diagnostic.log.utils;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Utility class that helps to work with directories
 */
public class DirHelper {

	/**
	 * Returns an array of files with specified extension
	 * 
	 * @param path
	 *            Directory path
	 * @param ext
	 *            Files extension
	 * 
	 * @return Array of files with specified extension
	 */
	public static String[] getFileListing(String path, String ext) {
		File dir = new File(path);
		return dir.list(new Filter(ext));
	}

	private static class Filter implements FilenameFilter {
		private final String ext;

		public Filter(String ext) {
			this.ext = ext;
		}

		@Override
		public boolean accept(File dir, String name) {
			boolean result = false;
			if (ext == null) {
				result = true;
			} else {
				int pos = name.indexOf("." + ext);
				if (pos != -1)
					result = true;
			}
			return result;
		}
	}
}
