package com.synapsense.diagnostic.log.utils;

import org.apache.oro.text.perl.Perl5Util;
import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Perl5Compiler;

/**
 * Utility class that helps to work with log4j patterns
 */
public class PerlHelper {

	public static final PerlHelper instance = new PerlHelper();
	private final Perl5Util util = new Perl5Util();
	private Perl5Compiler compiler = new Perl5Compiler();

	public static String replace(String pattern, String replacement, String input) {
		return instance.util.substitute(
		        "s/" + Perl5Compiler.quotemeta(pattern) + "/" + Perl5Compiler.quotemeta(replacement) + "/g", input);
	}

	public static String replaceMetaChars(String input) {
		input = replace("(", "\\(", input);
		input = replace(")", "\\)", input);
		input = replace("[", "\\[", input);
		input = replace("]", "\\]", input);
		input = replace("{", "\\{", input);
		input = replace("}", "\\}", input);
		input = replace("#", "\\#", input);
		return input;
	}

	/**
	 * Compiles a string pattern
	 * 
	 * @param pattern
	 *            a <code>String</code> pattern
	 * 
	 * @return Compiled pattern
	 */
	public static Pattern compile(String pattern) {
		Pattern result = null;
		try {
			result = instance.compiler.compile(pattern);
		} catch (MalformedPatternException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String substitute(String pattern, String input) {
		return instance.util.substitute(pattern, input);
	}
}
