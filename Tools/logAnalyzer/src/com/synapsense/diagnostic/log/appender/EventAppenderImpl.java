package com.synapsense.diagnostic.log.appender;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.synapsense.diagnostic.log.interpreter.FilterInterpreter;
import com.synapsense.diagnostic.log.interpreter.MessageInterpreter;
import com.synapsense.diagnostic.log.model.LogEvent;
import com.synapsense.diagnostic.messages.Display;
import com.synapsense.diagnostic.messages.If;
import com.synapsense.diagnostic.messages.LogAnalyzer;

public class EventAppenderImpl implements EventAppender {
	private Map<String, Collection<LogEvent>> messages = new HashMap<String, Collection<LogEvent>>();
	private Collection<LogEvent> events = new HashSet<LogEvent>();
	private final LogAnalyzer config;

	/**
	 * Creates an instance of EventAppenderImpl
	 * 
	 * @param config
	 *            a <code>LogAnalyzer</code> instance
	 */
	public EventAppenderImpl(LogAnalyzer config) {
		this.config = config;
	}

	@Override
	public void doAppend(LogEvent logEvent) {
		events.add(logEvent);
		processEvent(logEvent);
	}

	public Collection<LogEvent> getEvents() {
		return events;
	}

	/**
	 * Events collection in <code>messages</code> map
	 * 
	 * @param logEvent
	 *            a <code>LogEvent</code> instance
	 */
	public void processEvent(LogEvent logEvent) {
		for (Display display : config.getDisplay()) {
			String msgPattern = display.getMessage();
			for (If condition : display.getIf()) {
				Map<String, String> filterGroups = FilterInterpreter.getInterpretedGroups(condition.getFilter());
				int count = 0;
				for (String group : filterGroups.keySet()) {
					String logMessage = logEvent.getMessage(group);
					if (logMessage != null) {
						String regExFilter = filterGroups.get(group);
						Pattern pattern = Pattern.compile(regExFilter);
						Matcher matcher = pattern.matcher(logMessage);
						if (matcher.find()) {
							count++;
						}
					}
				}

				if (count == filterGroups.keySet().size()) {
					String interpretedMsg = MessageInterpreter.getInterpretedMessage(msgPattern, logEvent);
					Collection<LogEvent> logEvents = messages.get(interpretedMsg);
					if (logEvents == null) {
						logEvents = new HashSet<LogEvent>();
					}
					logEvents.add(logEvent);
					messages.put(interpretedMsg, logEvents);
				}
			}
		}
	}

	public Collection<String> getMessages() {
		return messages.keySet();
	}

	public Collection<LogEvent> getEvents(String message) {
		return messages.get(message);
	}
}
