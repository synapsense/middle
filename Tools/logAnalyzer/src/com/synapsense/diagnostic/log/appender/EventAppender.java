package com.synapsense.diagnostic.log.appender;

import java.util.Collection;

import com.synapsense.diagnostic.log.model.LogEvent;

public interface EventAppender {
	public void doAppend(LogEvent logEvent);

	public Collection<LogEvent> getEvents();

	public Collection<LogEvent> getEvents(String message);

	public Collection<String> getMessages();
}
