package com.synapsense.diagnostic.log;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.diagnostic.log.utils.DirHelper;
import com.synapsense.diagnostic.log.utils.RegistryHelper;

/**
 * Utility class that is looking for log files in Windows registry
 */
public class LogFinder {
	private static final String SERVICE_KEY = "HKLM\\SYSTEM\\ControlSet001\\Services\\";
	private static final Collection<String> services = new ArrayList<String>();

	static {
		services.add("hpieappsvr");
		services.add("hpiedevmgr");
		services.add("hpiembgw");
		services.add("hpieimgsvr");

		services.add("synapserver");
		services.add("synapdm");
		services.add("synapmbgw");
		services.add("synapviz");
		services.add("synapsnmp");
		services.add("SynapBACnet");
	}

	private static final String value = "ImagePath";
	private static final LogFinder instance = new LogFinder();
	private Collection<String> paths = new ArrayList<String>();

	private LogFinder() {
		init();
	}

	public static Collection<String> getLogFiles() {
		return instance.paths;
	}

	/**
	 * Fills <code>paths</code> collection with paths of found log files
	 */
	public void init() {
		for (String svc : services) {
			String imagePath = RegistryHelper.readValue(SERVICE_KEY + svc, value, RegistryHelper.REGEXPANDSZ_TOKEN);
			if (imagePath != null) {
				String logsDir = getDir(imagePath);
				if ("synapserver".equals(svc) || "hpieappsvr".equals(svc)) {
					logsDir = logsDir.substring(0, logsDir.indexOf("bin")) + "server/default/log/";
				} else {
					logsDir = logsDir + "logs/";
				}
				fillPaths(logsDir);
			}
		}
	}

	private void fillPaths(String logsDir) {
		String[] logFiles = DirHelper.getFileListing(logsDir, "log");
		for (int i = 0; i < logFiles.length; i++) {
			logFiles[i] = logFiles[i].replace('\\', '/');
			File file = new File(logsDir + logFiles[i]);
			if (file.isFile()) {
				paths.add(logsDir + logFiles[i]);
			}
		}
	}

	private String getDir(String imagePath) {
		imagePath = imagePath.replace('"', ' ').trim();
		int pos = imagePath.lastIndexOf("\\");
		if (pos != -1) {
			imagePath = imagePath.replace('\\', '/');
			return imagePath.substring(0, pos + 1);
		}
		return null;
	}
}
