package com.synapsense.diagnostic.log.interpreter;

import java.util.StringTokenizer;

import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Perl5Matcher;

import com.synapsense.diagnostic.log.constants.LogGroup;
import com.synapsense.diagnostic.log.model.LogEvent;
import com.synapsense.diagnostic.log.utils.PerlHelper;

/**
 * Utility class for interpreting messages from config file
 */
public class MessageInterpreter {
	private static final String PATTERN1 = "^([cCdlFLmMprtxT]{)[-1-9].+\\}.*";
	private static final String PERCENT = "%";
	private static final char BRACKET_START = '{';
	private static final char BRACKET_END = '}';

	/**
	 * Returns an user-friendly message
	 * 
	 * @param msgPattern
	 *            a message pattern
	 * 
	 * @param logEvent
	 *            a <code>LogEvent</code> instance
	 * 
	 * @return an user-friendly message
	 */
	public static String getInterpretedMessage(String msgPattern, LogEvent logEvent) {
		StringTokenizer tokenizer = new StringTokenizer(msgPattern, PERCENT);
		Perl5Matcher eventMatcher = new Perl5Matcher();
		StringBuilder result = new StringBuilder();

		Pattern pattern = PerlHelper.compile(PATTERN1);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (eventMatcher.matches(token, pattern)) {
				String key = token.substring(0, token.indexOf(BRACKET_START));
				String group = LogGroup.getGroup(key);
				String grMessage = logEvent.getMessage(group);
				String expression = token.substring(key.length() + 1, token.indexOf(BRACKET_END));

				grMessage = applyExpression(expression, grMessage);
				token = token.substring(token.indexOf(BRACKET_END) + 1);
				result.append(grMessage);
			}
			result.append(token);
		}

		return result.toString();
	}

	private static String applyExpression(String expression, String message) {
		String result = null;
		String[] expr = expression.split(",");

		String[] parts = expr[0].split("\\|");

		// Process start of the line
		int pos = Integer.parseInt(parts[0]);
		if (pos != -1) {
			String[] msg = message.split(parts[1]);
			StringBuffer buf = new StringBuffer();
			for (int i = pos; i < msg.length; i++) {
				if (i > pos)
					buf.append(getChar(parts[1]));
				buf.append(msg[i]);
			}
			result = buf.toString();

			// Process end of the line
			if (expr.length == 2) {
				parts = expr[1].split("\\|");
				pos = Integer.parseInt(parts[0]);
				msg = result.split(parts[1]);
				result = msg[pos - 1];
			}
		} else {
			result = message;
		}

		return result;
	}

	private static char getChar(String buf) {
		Character result = null;
		if (buf.equalsIgnoreCase("\\n")) {
			result = new Character('\n');
		} else if (buf.equalsIgnoreCase("\\t")) {
			result = new Character('\t');
		} else if (buf.equalsIgnoreCase("\\r")) {
			result = new Character('\r');
		} else {
			result = buf.charAt(0);
		}
		return result;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
}
