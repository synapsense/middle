package com.synapsense.diagnostic.log.interpreter;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.synapsense.diagnostic.log.constants.LogGroup;
import com.synapsense.diagnostic.log.model.ReceiverFormat;

/**
 * Utility class for interpreting log4j patterns
 */
public class LogPatternInterpeter {
	private static LogPatternInterpeter instance = new LogPatternInterpeter();
	private static final String PERCENT = "%";
	private static final char BRACKET_START = '{';
	private static final char BRACKET_END = '}';
	private List<String> patterns = new ArrayList<String>();

	/**
	 * Adds a new log4j pattern
	 * 
	 * @param pattern
	 *            a log4j pattern
	 */
	public static void addRawPattern(String pattern) {
		if (!instance.patterns.contains(pattern))
			instance.patterns.add(pattern);
	}

	private ReceiverFormat getInterpretedFormat(String log4jPattern) {
		StringBuffer interpretedPattern = new StringBuffer();
		String timeFormat = null;
		StringTokenizer tokenizer = new StringTokenizer(log4jPattern, PERCENT);
		try {
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();

				for (String key : LogGroup.groups.keySet()) {
					int keyCharIndex = token.indexOf(key);
					if (keyCharIndex > -1) {
						token = LogGroup.groups.get(key) + token.substring(keyCharIndex + 1, token.length());
						int bracketIndex = token.indexOf(BRACKET_START);
						if (bracketIndex > -1) {
							if ("d".equalsIgnoreCase(key)) { // Timestamp format
															 // exists
								timeFormat = token.substring(bracketIndex + 1, token.indexOf(BRACKET_END));
							}
							token = token.substring(0, bracketIndex)
							        + token.substring(token.indexOf(BRACKET_END) + 1, token.length());
						}
						break;
					}
				}
				interpretedPattern.append(token);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ReceiverFormat("", "", "");
		}
		return new ReceiverFormat(log4jPattern, interpretedPattern.toString().trim(), timeFormat);
	}

	/**
	 * @return Array of interpreted log4j patterns
	 */
	public static ReceiverFormat[] getInterpretedRecieverFormat() {
		int count = instance.patterns.size();
		ReceiverFormat[] result = new ReceiverFormat[count];
		for (int i = 0; i < count; i++) {
			result[i] = instance.getInterpretedFormat(instance.patterns.get(i));
		}
		return result;
	}

	/**
	 * For testing purpose only
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LogPatternInterpeter.addRawPattern("%d{ddMMM yyyy HH:mm:ss,SSS} %t %-5p %c{2} - %m%n");
		LogPatternInterpeter.addRawPattern("####<%d{MMM dd, yyyy hh:mm:ss a z}> %m%n");

		for (ReceiverFormat receiver : LogPatternInterpeter.getInterpretedRecieverFormat()) {
			System.out.println(receiver.toString());
		}
	}
}