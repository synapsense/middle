package com.synapsense.diagnostic.log.interpreter;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Perl5Matcher;
import com.synapsense.diagnostic.log.constants.LogGroup;
import com.synapsense.diagnostic.log.utils.PerlHelper;

/**
 * Utility class for interpreting filters
 */
public class FilterInterpreter {
	private static final String PATTERN = "^([cCdlFLmMprtxT]{).+}$";
	private static final String PERCENT = "%";
	private static final char BRACKET_START = '{';
	private static final char BRACKET_END = '}';

	/**
	 * Parses a filter from config file
	 * 
	 * @param filter
	 * @return a <code>Map</code> instance that contains from (GROUP -> RegEx)
	 */
	public static Map<String, String> getInterpretedGroups(String filter) {
		Map<String, String> result = new HashMap<String, String>();
		StringTokenizer tokenizer = new StringTokenizer(filter, PERCENT);
		Perl5Matcher eventMatcher = new Perl5Matcher();

		Pattern regexpPattern = PerlHelper.compile(PATTERN);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (eventMatcher.matches(token, regexpPattern)) {
				String key = token.substring(0, token.indexOf(BRACKET_START));
				String regEx = token.substring(key.length() + 1, token.indexOf(BRACKET_END));
				String group = LogGroup.getGroup(key);
				if (group != null)
					result.put(group, regEx);
			}
		}
		return result;
	}
}
