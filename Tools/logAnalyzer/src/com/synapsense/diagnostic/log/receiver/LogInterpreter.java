package com.synapsense.diagnostic.log.receiver;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

import com.synapsense.diagnostic.log.constants.LogGroup;
import com.synapsense.diagnostic.log.model.LogEvent;
import com.synapsense.diagnostic.log.model.LogNamespace;
import com.synapsense.diagnostic.log.model.ReceiverFormat;
import com.synapsense.diagnostic.log.utils.PerlHelper;

/**
 * The main class for log interpreter
 */
public class LogInterpreter {
	private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss,SSS";
	private static final String EXCEPTION_PATTERN = "\tat.*";
	private static final String REGEXP_DEFAULT_WILDCARD = ".*?";
	private static final String REGEXP_GREEDY_WILDCARD = ".+";
	private static final String PATTERN_WILDCARD = "*";
	private static final String DEFAULT_GROUP = "(" + REGEXP_DEFAULT_WILDCARD + ")";
	private static final String GREEDY_GROUP = "(" + REGEXP_GREEDY_WILDCARD + ")";
	private static final String VALID_DATEFORMAT_CHAR_PATTERN = "[GyMwWDdFEaHkKhmsSzZ]";
	private static final String PROP_START = "PROP(";
	private static final String PROP_END = ")";

	private static final int MAX_UNMATCHED_LINE_COUNT = 1000;
	private final Set<String> keywords = new HashSet<String>();
	private List<String>[] matchingKeywords = null;
	private final String newLine = System.getProperty("line.separator");
	private final String[] emptyException = new String[] { "" };
	private SimpleDateFormat[] dateFormat;

	private final Perl5Compiler exceptionCompiler = new Perl5Compiler();
	private final Perl5Matcher exceptionMatcher = new Perl5Matcher();
	private Perl5Compiler compiler = new Perl5Compiler();

	private Pattern[] regexpPattern = null;
	private Perl5Matcher eventMatcher = new Perl5Matcher();

	private final Map<String, String> currentMap = new HashMap<String, String>();
	private final List<String> additionalLines = new ArrayList<String>();

	/**
	 * Regular expression to match
	 */
	private String regexp;

	private Set<String> greedyKeywords = new HashSet<String>();
	private String timestampPatternText;
	private final LogNamespace logNamespace;

	@SuppressWarnings("unchecked")
	public LogInterpreter(LogNamespace logNamespace) {
		this.logNamespace = logNamespace;
		int count = logNamespace.getReceiverFormat().length;
		this.regexpPattern = new Pattern[count];
		this.matchingKeywords = new List[count];
		this.dateFormat = new SimpleDateFormat[count];

		keywords.add(LogGroup.TIMESTAMP);
		keywords.add(LogGroup.LOGGER);
		keywords.add(LogGroup.LEVEL);
		keywords.add(LogGroup.THREAD);
		keywords.add(LogGroup.CLASS);
		keywords.add(LogGroup.FILE);
		keywords.add(LogGroup.LINE);
		keywords.add(LogGroup.METHOD);
		keywords.add(LogGroup.MESSAGE);
		keywords.add(LogGroup.NDC);
		keywords.add(LogGroup.RELATIVETIME);

		greedyKeywords.add(LogGroup.MESSAGE);

		initializePatternParsing();
	}

	/**
	 * Parses a log file line
	 * 
	 * @param line
	 *            Line from log file
	 * @return Array of events
	 */
	public LogEvent[] parseLogMessage(String line) {

		if (line == null) {
			LogEvent event = buildEvent();
			if (event != null) {
				LogEvent[] lastEvent = new LogEvent[1];
				lastEvent[0] = event;
				return lastEvent;
			}
			return new LogEvent[0];
		}

		ArrayList<LogEvent> eventsList = new ArrayList<LogEvent>();
		int validReceiverFormat = -1;
		boolean isValidRegexpPattern = false;
		for (int i = 0; i < regexpPattern.length; i++) {
			if (eventMatcher.matches(line, regexpPattern[i])) {
				validReceiverFormat = i;

				LogEvent event = buildEvent();
				if (event != null) {
					eventsList.add(event);
				}
				currentMap.putAll(processEvent(eventMatcher.getMatch(), validReceiverFormat));
				isValidRegexpPattern = true;
				break;
			}
			validReceiverFormat = -1;
		}
		if (!isValidRegexpPattern) {
			additionalLines.add(line);

			if (additionalLines.size() > MAX_UNMATCHED_LINE_COUNT) {
				additionalLines.clear();
			}
		}
		return (LogEvent[]) eventsList.toArray(new LogEvent[0]);
	}

	/**
	 * @return line number from an exception block
	 */
	private int getExceptionLine() {
		try {
			Pattern exceptionPattern = exceptionCompiler.compile(EXCEPTION_PATTERN);
			for (int i = 0; i < additionalLines.size(); i++) {
				if (exceptionMatcher.matches((String) additionalLines.get(i), exceptionPattern)) {
					return i - 1;
				}
			}
		} catch (MalformedPatternException mpe) {
			mpe.printStackTrace();
		}
		return -1;
	}

	/**
	 * 
	 * @param firstMessageLine
	 *            First message line
	 * @param exceptionLine
	 *            line number of exception block
	 * 
	 * @return a Message
	 */
	private String buildMessage(String firstMessageLine, int exceptionLine) {

		if (additionalLines.size() == 0 || exceptionLine == 0) {
			return firstMessageLine;
		}
		StringBuffer message = new StringBuffer(firstMessageLine);
		int linesToProcess = (exceptionLine == -1 ? additionalLines.size() : exceptionLine);

		for (int i = 0; i < linesToProcess; i++) {
			message.append(newLine);
			message.append(additionalLines.get(i));
		}
		return message.toString();
	}

	/**
	 * @param exceptionLine
	 *            line number from exception block
	 * 
	 * @return Array of exceptions
	 */
	private String[] buildException(int exceptionLine) {

		if (exceptionLine == -1) {
			return emptyException;
		}
		String[] exception = new String[additionalLines.size() - exceptionLine];
		for (int i = 0; i < additionalLines.size() - exceptionLine; i++) {
			exception[i] = (String) additionalLines.get(i + exceptionLine);
		}
		return exception;
	}

	/**
	 * @return a <code>LogEvent</code> instance
	 */
	private LogEvent buildEvent() {

		LogEvent event = null;

		if (currentMap.size() == 0) {
			return event;
		}
		int exceptionLine = getExceptionLine();
		String[] exception = buildException(exceptionLine);

		if (additionalLines.size() > 0 && exceptionLine != 0) {
			currentMap.put(LogGroup.MESSAGE, buildMessage((String) currentMap.get(LogGroup.MESSAGE), exceptionLine));
		}

		if (currentMap.size() > 0) {
			event = convertToEvent(currentMap, exception);
		}
		this.currentMap.clear();
		this.additionalLines.clear();

		return event;
	}

	/**
	 * 
	 * @param result
	 *            a <code>MatchResult</code> instance
	 * @param validReceiverFormat
	 *            an index
	 * @return
	 */
	private Map<String, String> processEvent(MatchResult result, int validReceiverFormat) {

		Map<String, String> map = new HashMap<String, String>(); // Maps GROUP
																 // --> result
																 // values
		for (int i = 1; i < result.groups(); i++) {
			map.put(matchingKeywords[validReceiverFormat].get(i - 1), result.group(i));
		}
		return map;
	}

	/**
	 * 
	 * @param receiverFormat
	 *            a <code>ReceiverFormat</code> instance
	 * @return Converted timeStamp format
	 */
	private String convertTimestamp(ReceiverFormat receiverFormat) {
		String timestampFormat = receiverFormat.getTimeStampPattern() == null ? TIMESTAMP_FORMAT : receiverFormat
		        .getTimeStampPattern();
		return PerlHelper.substitute("s/" + VALID_DATEFORMAT_CHAR_PATTERN + "/\\\\w/g", timestampFormat);
	}

	/**
	 * 
	 */
	private void initializePatternParsing() {
		ReceiverFormat[] receiverFormat = logNamespace.getReceiverFormat();
		for (int i = 0; i < receiverFormat.length; i++) {
			String timestampFormat = receiverFormat[i].getTimeStampPattern() == null ? TIMESTAMP_FORMAT
			        : receiverFormat[i].getTimeStampPattern();
			if (timestampFormat != null) {
				dateFormat[i] = new SimpleDateFormat(timestampFormat);
				timestampPatternText = convertTimestamp(receiverFormat[i]);
			}

			Map<Integer, String> keywordMap = new TreeMap<Integer, String>();
			String newPattern = receiverFormat[i].getLogPattern();

			int index = 0;
			String current = newPattern;
			while (index > -1) {
				index = current.indexOf(PROP_START);
				if (index > -1) {
					String currentProp = current.substring(current.indexOf(PROP_START));
					String prop = currentProp.substring(0, currentProp.indexOf(PROP_END) + 1);
					current = current.substring(current.indexOf(currentProp) + 1);
					String shortProp = prop.substring(PROP_START.length(), prop.length() - 1);
					keywordMap.put(new Integer(index), shortProp);
					newPattern = PerlHelper.replace(prop, shortProp, newPattern);
				}
			}

			newPattern = PerlHelper.replaceMetaChars(newPattern);
			newPattern = PerlHelper.replace(PATTERN_WILDCARD, REGEXP_DEFAULT_WILDCARD, newPattern);

			Iterator<String> iter = keywords.iterator();
			while (iter.hasNext()) {
				String keyword = (String) iter.next();
				int index2 = newPattern.indexOf(keyword);
				if (index2 > -1) {
					keywordMap.put(new Integer(index2), keyword);
				}
			}

			matchingKeywords[i] = new ArrayList<String>();
			matchingKeywords[i].addAll(keywordMap.values());

			String currentPattern = newPattern;
			Iterator<String> iter2 = matchingKeywords[i].iterator();
			while (iter2.hasNext()) {
				String keyword = (String) iter2.next();
				if (LogGroup.TIMESTAMP.equals(keyword)) {
					currentPattern = PerlHelper.replace(keyword, "(" + timestampPatternText + ")", currentPattern);
				} else {
					currentPattern = PerlHelper.replace(keyword, greedyKeywords.contains(keyword) ? GREEDY_GROUP
					        : DEFAULT_GROUP, currentPattern);
				}
			}
			this.regexp = currentPattern;
			try {
				this.regexpPattern[i] = compiler.compile(regexp);
			} catch (MalformedPatternException mpe) {
				throw new RuntimeException("Bad pattern: " + regexp);
			}
		}
	}

	/**
	 * Converts internal structures into event
	 * 
	 * @param fieldMap
	 * @param exception
	 * @return a <code>LogEvent</code> instance
	 */
	private LogEvent convertToEvent(Map<String, String> fieldMap, String[] exception) {

		if (fieldMap == null) {
			return null;
		}

		if (!fieldMap.containsKey(LogGroup.LOGGER)) {
			fieldMap.put(LogGroup.LOGGER, "Unknown");
		}
		if (exception == null) {
			exception = emptyException;
		}

		StringBuilder throwable = new StringBuilder();
		for (int i = 0; i < (exception.length > 10 ? 10 : exception.length); i++) {
			if (i != 0)
				throwable.append('\n');
			throwable.append(exception[i]);
		}
		fieldMap.put(LogGroup.THROWABLE, throwable.toString());

		return new LogEvent(fieldMap);
	}

	public LogNamespace getLogNamespace() {
		return logNamespace;
	}
}
