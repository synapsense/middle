package com.synapsense.diagnostic.log.receiver;

public class ReceiverInitializationException extends Exception {

	private static final long serialVersionUID = 7433764477768473189L;

	public ReceiverInitializationException() {
		super();
	}

	public ReceiverInitializationException(String s) {
		super(s);
	}

	public ReceiverInitializationException(Throwable cause) {
		super(cause);
	}

	public ReceiverInitializationException(String message, Throwable cause) {
		super(message, cause);
	}
}
