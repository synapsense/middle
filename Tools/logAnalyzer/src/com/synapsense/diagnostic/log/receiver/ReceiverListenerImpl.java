package com.synapsense.diagnostic.log.receiver;

import com.synapsense.diagnostic.log.model.LogNamespace;

public class ReceiverListenerImpl implements ReceiverListener {
	@Override
	public void endLoadEventNotification(LogNamespace[] namespaces, int infoFlag) {
		for (LogNamespace namespace : namespaces) {
			synchronized (namespace) {
				namespace.notify();
			}
		}
	}

	@Override
	public void startLoadEventNotification(LogNamespace[] namespaces) {
	}

}
