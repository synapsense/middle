package com.synapsense.diagnostic.log.receiver;

public interface ReceiverInterpreter {
	public static final int SUCCESS = 0;
	public static final int FAILURE = 1;

	public void addReceiverListener(ReceiverListener receiverListener);

	public void removeReceiverListener(ReceiverListener receiverListener);
}
