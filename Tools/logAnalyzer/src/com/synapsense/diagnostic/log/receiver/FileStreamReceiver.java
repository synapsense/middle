package com.synapsense.diagnostic.log.receiver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import com.synapsense.diagnostic.log.model.LogEvent;
import com.synapsense.diagnostic.log.model.LogNamespace;

/**
 * This class is responsible for reading a log file
 */
public class FileStreamReceiver extends AbstractReceiver {
	private LogNamespace logNamespace;
	private boolean isTailing;
	private LogInterpreter logInterpreter = null;

	private BufferedReader reader;

	private boolean hasReachedEOS = false;

	public FileStreamReceiver(LogNamespace namespace, boolean isTailing) throws ReceiverInitializationException {
		initialize(namespace, isTailing);
	}

	public LogNamespace[] getNamespaces() {
		return new LogNamespace[] { this.logNamespace };
	}

	public boolean isTailing() {
		return this.isTailing;
	}

	protected void initialize(LogNamespace namespace, boolean isTailingReceiver) throws ReceiverInitializationException {

		this.logNamespace = namespace;
		this.isTailing = isTailingReceiver;
		logInterpreter = new LogInterpreter(namespace);

		String url = namespace.getLocalSourceString();
		url = url.replaceAll("#", "%23");

		try {
			reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
		} catch (IOException ioe) {
			throw new ReceiverInitializationException("Unable to load : " + (url == null ? null : url.toString()), ioe);
		}
	}

	protected void deInitialize() {
		try {
			if (reader != null) {
				reader.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	protected LogEvent[] getNextEvents() {
		LogEvent[] events = null;
		String line = null;
		try {
			line = reader.readLine();
		} catch (IOException ioe) {
			// no events to process if error occurs
			return events;
		}
		return this.logInterpreter.parseLogMessage(line);
	}

	protected boolean hasMoreEvents() {
		boolean hasData = false;
		try {
			hasData = reader.ready();
			if (!hasData) {
				// check to see if the logical hasReachedEOS has been set
				// already
				if (hasReachedEOS) {
					return hasData;
				} else {
					hasReachedEOS = true;
					return true;
				}
			}
		} catch (IOException ioe) {
		}
		return hasData;
	}
}