package com.synapsense.diagnostic.log.receiver;

import java.util.ArrayList;
import java.util.List;

import com.synapsense.diagnostic.log.appender.EventAppender;
import com.synapsense.diagnostic.log.model.LogEventModelMutator;
import com.synapsense.diagnostic.log.model.MutatorListener;
import com.synapsense.diagnostic.log.model.LogEvent;
import com.synapsense.diagnostic.log.model.LogNamespace;

/**
 * Abstract class
 */
public abstract class AbstractReceiver extends Thread implements ReceiverInterpreter, LogEventModelMutator {
	private static final int IDLE = 0;
	private static final int ACTIVE = 1;
	private static final int EXIT = 2;
	private static final long SLEEP_DURATION = 250;
	private volatile int state = IDLE;
	protected long interpretedEventCount = 0L;
	private List<EventAppender> appenders = new ArrayList<EventAppender>();
	private List<ReceiverListener> receiverListeners = new ArrayList<ReceiverListener>();
	private List<MutatorListener> mutatorListeners = new ArrayList<MutatorListener>();

	public void addAppender(EventAppender appender) {
		appenders.add(appender);
	}

	public void removeAppender(EventAppender appender) {
		appenders.remove(appender);
	}

	public void addReceiverListener(ReceiverListener receiverListener) {
		receiverListeners.add(receiverListener);
	}

	public void removeReceiverListener(ReceiverListener receiverListener) {
		receiverListeners.remove(receiverListener);
	}

	public void addMutatorListener(MutatorListener mutatorListener) {
		mutatorListeners.add(mutatorListener);
	}

	public void removeMutatorListener(MutatorListener mutatorListener) {
		mutatorListeners.remove(mutatorListener);
	}

	public void startup() {
		if (this.state == ACTIVE) {
			return;
		}
		this.state = ACTIVE;
		this.setPriority(Thread.NORM_PRIORITY);
		this.start();
	}

	public void run() {
		this.notifyStartup();
		while (hasMoreEvents() || isTailing()) {
			if (this.state == EXIT || this.state == IDLE) {
				break;
			}
			LogEvent[] events = getNextEvents();
			if (events != null) {
				for (int i = 0; i < events.length; i++) {
					fireEventReceived(events[i]);
				}
			} else if (isTailing()) {
				synchronized (this) {
					try {
						wait(SLEEP_DURATION);
					} catch (InterruptedException ie) {
						// do nothing
					}
				}
			}
		}
		this.notifyShutdown();
	}

	public void shutdown() {
		if (this.state == EXIT) {
			return;
		}
		this.state = EXIT;
		synchronized (this) {
			notifyAll();
		}
	}

	public abstract LogNamespace[] getNamespaces();

	public abstract boolean isTailing();

	protected abstract void initialize(LogNamespace namespace, boolean isTailing)
	        throws ReceiverInitializationException;

	protected abstract void deInitialize();

	protected abstract LogEvent[] getNextEvents();

	protected abstract boolean hasMoreEvents();

	private void fireEventReceived(LogEvent event) {
		for (EventAppender appender : appenders) {
			appender.doAppend(event);
		}
		this.interpretedEventCount += 1;
	}

	private void notifyStartup() {
		synchronized (AbstractReceiver.class) {
			for (ReceiverListener receiver : receiverListeners) {
				receiver.startLoadEventNotification(getNamespaces());
			}
			for (MutatorListener mutator : mutatorListeners) {
				mutator.startMutating(this.isTailing() ? LogEventModelMutator.TAILING_MUTATOR
				        : LogEventModelMutator.NON_TAILING_MUTATOR);
			}
		}
	}

	private void notifyShutdown() {
		synchronized (AbstractReceiver.class) {
			int receiverInfoFlag = interpretedEventCount == 0L ? ReceiverInterpreter.FAILURE
			        : ReceiverInterpreter.SUCCESS;
			for (ReceiverListener receiver : receiverListeners) {
				receiver.endLoadEventNotification(getNamespaces(), receiverInfoFlag);
			}
			int mutatorInfoFlag = interpretedEventCount == 0L ? LogEventModelMutator.FAILURE
			        : LogEventModelMutator.SUCCESS;
			for (MutatorListener mutator : mutatorListeners) {
				mutator.endMutating(mutatorInfoFlag);
			}
			this.deInitialize();
		}
	}
}
