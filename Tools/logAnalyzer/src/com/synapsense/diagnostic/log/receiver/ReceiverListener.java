package com.synapsense.diagnostic.log.receiver;

import com.synapsense.diagnostic.log.model.LogNamespace;

public interface ReceiverListener {
	public void startLoadEventNotification(LogNamespace[] namespaces);

	public void endLoadEventNotification(LogNamespace[] namespaces, int infoFlag);
}
