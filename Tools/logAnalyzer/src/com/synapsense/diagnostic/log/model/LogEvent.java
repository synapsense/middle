package com.synapsense.diagnostic.log.model;

import java.util.HashMap;
import java.util.Map;

public class LogEvent {
	Map<String, String> group = new HashMap<String, String>();

	public LogEvent() {
	}

	public LogEvent(Map<String, String> group) {
		this.group.putAll(group);
	}

	public void addGroup(String group, String message) {
		this.group.put(group, message);
	}

	public String getMessage(String key) {
		return group.get(key);
	}
}
