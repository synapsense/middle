package com.synapsense.diagnostic.log.model;

public interface LogEventModelMutator {

	public static final int NON_TAILING_MUTATOR = 0;
	public static final int TAILING_MUTATOR = 1;

	public static final int SUCCESS = 0;
	public static final int FAILURE = 1;

	public void addMutatorListener(MutatorListener mutatorListener);

	public void removeMutatorListener(MutatorListener mutatorListener);

}
