package com.synapsense.diagnostic.log.model;

import java.io.Serializable;

/**
 * Framework for log file
 */
public class LogNamespace implements Serializable {

	/**
	 * Used for object serialization
	 */
	private static final long serialVersionUID = 1148642272921507002L;

	private String sourceString;
	private String localSourceString;
	private ReceiverFormat[] receiverFormat;
	private String nodeId;

	/**
	 * Creates a <code>LogNamespace</code> instance
	 * 
	 * @param sourceString
	 *            a path to log file
	 * @param localSourceString
	 *            a path to log file in URL format
	 * @param receiverFormat
	 *            an interpreted log4j pattern
	 * @param nodeId
	 *            a hostname
	 */
	public LogNamespace(String sourceString, String localSourceString, ReceiverFormat[] receiverFormat, String nodeId) {
		this.sourceString = sourceString;
		this.localSourceString = localSourceString;
		this.receiverFormat = receiverFormat;
		this.nodeId = nodeId;
	}

	public String getNamespaceAsString() {
		return "[" + this.nodeId + "] " + this.sourceString;
	}

	public boolean equals(Object namespace) {
		if (this == namespace) {
			return true;
		}
		if (namespace instanceof LogNamespace) {
			LogNamespace otherNamespace = (LogNamespace) namespace;
			if ((this.sourceString != null && otherNamespace.sourceString == null)
			        || (otherNamespace.sourceString != null && this.sourceString == null)) {
				return false;
			}
			if (this.sourceString != null && otherNamespace.sourceString != null
			        && !this.sourceString.equals(otherNamespace.sourceString)) {
				return false;
			}
			if ((this.nodeId != null && otherNamespace.nodeId == null)
			        || (otherNamespace.nodeId != null && this.nodeId == null)) {
				return false;
			}
			if (this.nodeId != null && otherNamespace.nodeId != null && !this.nodeId.equals(otherNamespace.nodeId)) {
				return false;
			}
		}
		return true;
	}

	public String getNodeId() {
		return nodeId;
	}

	public ReceiverFormat[] getReceiverFormat() {
		return receiverFormat;
	}

	public String getSourceString() {
		return sourceString;
	}

	public String getLocalSourceString() {
		return localSourceString;
	}
}
