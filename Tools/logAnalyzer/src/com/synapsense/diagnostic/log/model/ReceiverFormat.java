package com.synapsense.diagnostic.log.model;

import java.io.Serializable;

/**
 * This class is holder for log4j patterns
 */
public class ReceiverFormat implements Serializable {

	private static final long serialVersionUID = -6792660326250944136L;

	private String rawPattern;
	private String logPattern;
	private String timeStampPattern;

	/**
	 * Creates a <code>ReceiverFormat</code> instance
	 * 
	 * @param rawPattern
	 *            log4j pattern
	 * @param logPattern
	 *            interpreted log4j pattern
	 * @param timeStampPattern
	 *            interpreted timeStamp pattern
	 */
	public ReceiverFormat(String rawPattern, String logPattern, String timeStampPattern) {
		this.rawPattern = rawPattern;
		this.logPattern = logPattern;
		this.timeStampPattern = timeStampPattern;
	}

	public String getLogPattern() {
		return logPattern;
	}

	public void setLogPattern(String logPattern) {
		this.logPattern = logPattern;
	}

	public String getTimeStampPattern() {
		return timeStampPattern;
	}

	public void setTimeStampPattern(String timeStampPattern) {
		this.timeStampPattern = timeStampPattern;
	}

	public String toString() {
		return "**Receiver Format**\n" + logPattern + '\n' + timeStampPattern;
	}

	public String getRawPattern() {
		return rawPattern;
	}
}
