package com.synapsense.diagnostic.log.model;

public interface MutatorListener {
	public void startMutating(int mutatorType);

	public void endMutating(int infoFlag);
}
