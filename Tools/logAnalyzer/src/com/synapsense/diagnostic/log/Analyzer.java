package com.synapsense.diagnostic.log;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.JAXBException;
import com.synapsense.diagnostic.messages.Log4J;
import com.synapsense.diagnostic.messages.LogAnalyzer;
import com.synapsense.diagnostic.log.appender.EventAppender;
import com.synapsense.diagnostic.log.appender.EventAppenderImpl;
import com.synapsense.diagnostic.log.interpreter.LogPatternInterpeter;
import com.synapsense.diagnostic.log.model.LogNamespace;
import com.synapsense.diagnostic.log.receiver.FileStreamReceiver;
import com.synapsense.diagnostic.log.receiver.ReceiverInitializationException;
import com.synapsense.diagnostic.log.receiver.ReceiverListenerImpl;
import com.synapsense.diagnostic.log.utils.XmlConfigUtils;

/**
 * Main class
 */
public class Analyzer {
	public static final String config = "conf/loganalyzer.xml";
	public static final String FILE_PROTOCOL_PREFIX = "file:///";
	private static XmlConfigUtils xmlBuilder = null;
	private LogAnalyzer analyzer = null;

	static {
		try {
			xmlBuilder = new XmlConfigUtils("conf", LogAnalyzer.class);
		} catch (JAXBException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public Analyzer() {
		try {
			analyzer = xmlBuilder.read(new File(config), LogAnalyzer.class);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Starts the analyzing
	 */
	public void start() {
		initLogInterpreter();

		String nodeId = "";
		try {
			nodeId = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		Collection<String> logFiles = LogFinder.getLogFiles();
		for (String log : logFiles) {
			LogNamespace namespace = new LogNamespace(log, FILE_PROTOCOL_PREFIX + log,
			        LogPatternInterpeter.getInterpretedRecieverFormat(), nodeId);
			analyze(namespace);
		}
	}

	/**
	 * Analyzes a particular log file
	 * 
	 * @param namespace
	 *            a <code>LogNamespace</code> instance
	 */
	public void analyze(LogNamespace namespace) {
		try {
			System.out.print("Processing " + namespace.getSourceString() + " file...");
			FileStreamReceiver receiver = new FileStreamReceiver(namespace, false);
			EventAppender appender = new EventAppenderImpl(analyzer);
			receiver.addAppender(appender);
			receiver.addReceiverListener(new ReceiverListenerImpl());
			receiver.startup();
			synchronized (namespace) {
				try {
					namespace.wait();
				} catch (InterruptedException e) {
				}
				;
			}
			displayResult(appender, namespace);
		} catch (ReceiverInitializationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Outputs the results
	 * 
	 * @param appender
	 *            a <code>EventAppender</code> instance
	 * @param namespace
	 *            Namespace of a particular log file
	 */
	public void displayResult(EventAppender appender, LogNamespace namespace) {
		System.out.println("\t\tFound " + appender.getMessages().size() + " problem(s).");
		List<String> messages = new ArrayList<String>();
		messages.addAll(appender.getMessages());
		Collections.sort(messages);
		for (String message : messages) {
			System.out.println("\t" + message);
		}
	}

	/**
	 * Initializes a log interpreter by log4j patterns
	 */
	public void initLogInterpreter() {
		for (Log4J log4j : analyzer.getPatterns().getLog4J()) {
			LogPatternInterpeter.addRawPattern(log4j.getPattern());
		}
	}

	/**
	 * 
	 * @param args
	 *            This argument isn't used
	 */
	public static void main(String[] args) {
		Analyzer analyzer = new Analyzer();
		analyzer.start();
	}
}
