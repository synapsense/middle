package com.synapsense.util.algo;

public class ForEach<T> {
	private Iterable<T> collection;

	public ForEach() {
		collection = null;
	}

	public ForEach(Iterable<T> collection) {
		this.collection = collection;
	}

	public void process(Iterable<T> collection, UnaryFunctor<T> functor) {
		for (T element : collection) {
			functor.process(element);
		}
	}

	public void process(UnaryFunctor<T> functor) {
		if (collection == null)
			throw new IllegalStateException("Collection to iterate over is not set at ForEach ctor");
		process(collection, functor);
	}
}
