package com.synapsense.util.algo;

public interface Predicate<T> {
	boolean evaluate(T value);
}
