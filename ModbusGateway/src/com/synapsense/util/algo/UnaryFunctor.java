package com.synapsense.util.algo;

public interface UnaryFunctor<T> {
	void process(T value);
}
