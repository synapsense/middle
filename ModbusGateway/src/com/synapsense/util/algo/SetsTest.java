package com.synapsense.util.algo;

import com.synapsense.util.algo.SetsIntersector.AllValues;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SetsTest {
	private final static class MyComp implements Comparator<Integer> {
		@Override
		public int compare(Integer o1, Integer o2) {
			if (o1 < o2)
				return -1;
			if (o1 > o2)
				return 1;
			return 0;
		}
	}

	private final static Comparator<Integer> comp = new MyComp();

	private static void printResult(int[] first, int[] second, AllValues<Integer> data) {
		System.out.println("--------------------------");
		System.out.println("srcs : " + toC(first) + ", " + toC(second));
		System.out.println("Inter: " + data.getFirst());
		System.out.println("A - B: " + data.getSecond().getFirst());
		System.out.println("B - A: " + data.getSecond().getSecond());
	}

	private static List<Integer> toC(int[] from) {
		List<Integer> result = new ArrayList<Integer>();
		for (int i : from) {
			result.add(i);
		}
		return result;
	}

	public static void main(String[] args) {
		int[] first;
		int[] second;

		first = new int[] {};
		second = new int[] {};
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] { 1 };
		second = new int[] {};
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] {};
		second = new int[] { 2 };
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] { 1, 2, 3 };
		second = new int[] { 4, 5 };
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] { 4, 5 };
		second = new int[] { 1, 2, 3 };
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] { 1, 2, 3 };
		second = new int[] { 2, 3 };
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] { 3, 4, 5 };
		second = new int[] { 2, 3 };
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] { 1, 2 };
		second = new int[] { 2, 3, 4 };
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] { 1, 2, 3, 7, 8, 9 };
		second = new int[] { 3, 4, 5, 6, 7 };
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));

		first = new int[] { 1, 2 };
		second = new int[] { 1, 2 };
		printResult(first, second, new SetsIntersector<Integer>().doAllJob(toC(first), toC(second), comp));
		return;
	}
}
