package com.synapsense.util;

public class TimeSampleFormatException extends Exception {
	public TimeSampleFormatException(String desc) {
		super(desc);
	}

	public TimeSampleFormatException(String desc, Throwable cause) {
		super(desc, cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4217719360956048699L;

}
