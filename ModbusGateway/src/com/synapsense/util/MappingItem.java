package com.synapsense.util;

import com.synapsense.util.algo.Pair;

import java.io.Serializable;

/**
 * A item of MappingList list. Each MappingItem consists of Identity of child
 * entity (device or object)
 * 
 * @author anechaev
 * 
 */
public final class MappingItem extends Pair<Identity, String> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9098463779912697594L;

	public MappingItem(Identity identity, String envyObjectTypename) {
		super(identity, envyObjectTypename);
	}

	public Identity getIdentity() {
		return first;
	}

	public String getEnvyObjectTypename() {
		return second;
	}
}
