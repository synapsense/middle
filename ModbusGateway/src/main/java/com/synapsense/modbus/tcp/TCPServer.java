package com.synapsense.modbus.tcp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.synapsense.modbus.api.CmdProcessor;
import com.synapsense.modbus.api.RequestCmd;
import com.synapsense.modbus.api.ResponseCmd;
import com.synapsense.modbus.api.ResponseListener;
import com.synapsense.modbus.api.exceptions.ParsingException;
import com.synapsense.modbus.api.exceptions.TimeoutException;
import com.synapsense.modbus.util.AsyncThreadedQueue;
import com.synapsense.modbus.util.UnaryFunctor;
import com.synapsense.modbus.util.Pair;

public class TCPServer extends TCPTransport {
	private final static Logger logger = Logger.getLogger(TCPServer.class);
	private final static int CAPACITY = 1000; // maximum size of the incoming
											  // packets queue
	private final static int BACKLOG = 10; // maximum number of connections to
										   // the server

	private final CmdProcessor processor;

	private AsyncThreadedQueue<Pair<Connection, TCPPacket<RequestCmd>>> packets;

	private class ServerThread extends Thread {
		private ServerSocket servSock;

		private ExecutorService executors;

		ServerThread(InetAddress address, int port) throws IOException {
			super("Modbus GW TCP Server - " + address.getHostAddress() + ":" + port);
			setDaemon(true);
			logger.info("Binding to " + address.getHostAddress() + ":" + port);
			servSock = new ServerSocket(port, BACKLOG, address);
			logger.info("successfully bound");
			executors = Executors.newFixedThreadPool(BACKLOG);
		}

		@Override
		public void run() {
			while (true) {
				try {
					executors.execute(new Connection(servSock.accept(), packets));
				} catch (IOException e) {
					logger.warn("couldn't accept a connection: ", e);
				}
			}
		}
	}

	public TCPServer(String address, CmdProcessor processor) throws IOException {
		this.processor = processor;

		packets = new AsyncThreadedQueue<Pair<Connection, TCPPacket<RequestCmd>>>(
		        new UnaryFunctor<Pair<Connection, TCPPacket<RequestCmd>>>() {
			        @Override
			        public void process(Pair<Connection, TCPPacket<RequestCmd>> value) {
				        processRequest(value);
			        }
		        }, CAPACITY, true);

		Pair<InetAddress, Integer> addr;
		try {
			addr = IPAddressSplitter.parse(address);
		} catch (ParsingException e) {
			throw new IllegalArgumentException("Incorrect ip address:port pair - " + address);
		}

		new ServerThread(addr.getFirst(), addr.getSecond()).start();
	}

	@Override
	public ResponseCmd request(RequestCmd cmd) throws IOException, TimeoutException {
		throw new UnsupportedOperationException("Server-side transport cannot send requests");
	}

	@Override
	public void request(RequestCmd cmd, ResponseListener listener) {
		throw new UnsupportedOperationException("Server-side transport cannot send requests");
	}

	@Override
	public void shutdown() {
	}

	private void processRequest(Pair<Connection, TCPPacket<RequestCmd>> request) {
		ResponseCmd cmd = request.getSecond().getCmd().execute(processor);
		request.getFirst().send(new TCPPacket<ResponseCmd>(request.getSecond(), cmd));
	}
}
