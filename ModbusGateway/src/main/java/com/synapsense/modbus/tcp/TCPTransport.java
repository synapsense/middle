package com.synapsense.modbus.tcp;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.regex.Pattern;

import com.synapsense.modbus.util.Pair;
import com.synapsense.modbus.api.Transport;
import com.synapsense.modbus.api.exceptions.ParsingException;

public abstract class TCPTransport implements Transport {
	protected static class IPAddressSplitter {
		private final static Pattern splitter = Pattern.compile(":");

		static Pair<InetAddress, Integer> parse(String source) throws ParsingException {
			try {
				String[] pieces = splitter.split(source);
				return new Pair<InetAddress, Integer>(InetAddress.getByName(pieces[0]), Integer.parseInt(pieces[1]));
			} catch (Exception e) {
				throw new ParsingException("Cannot parse IP Address:port pair " + source, e);
			}
		}
	}

	public static int readInt(DataInputStream in) throws IOException {
		byte hi, lo;
		hi = in.readByte();
		lo = in.readByte();
		return ((hi << 8) | lo);
	}
}
