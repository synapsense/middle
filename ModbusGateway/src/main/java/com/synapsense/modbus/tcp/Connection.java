package com.synapsense.modbus.tcp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.apache.log4j.Logger;

import com.synapsense.modbus.util.Pair;
import com.synapsense.modbus.api.Command;
import com.synapsense.modbus.api.RequestCmd;
import com.synapsense.modbus.api.commands.AbstractCommand;
import com.synapsense.modbus.api.commands.CommandsFactory;
import com.synapsense.modbus.api.exceptions.ParsingException;
import com.synapsense.modbus.util.AsyncThreadedQueue;

class Connection implements Runnable {
	private final static Logger logger = Logger.getLogger(Connection.class);
	private final Socket client;
	private final AsyncThreadedQueue<Pair<Connection, TCPPacket<RequestCmd>>> queue;
	private Object singleCmdLock = new Object();

	public Connection(Socket client, AsyncThreadedQueue<Pair<Connection, TCPPacket<RequestCmd>>> queue) {
		logger.info("Established connection with " + client.getRemoteSocketAddress().toString());
		this.client = client;
		this.queue = queue;
	}

	private void close() {
		logger.info("Closing the connection with " + client.getRemoteSocketAddress().toString());
		try {
			client.close();
		} catch (IOException e) {
			logger.debug("an error occured while closing a connection", e);
		}
	}

	@Override
	public void run() {
		byte[] transID;
		int protoID;
		byte[] payload;
		AbstractCommand cmd;
		while (true) {
			try {
				DataInputStream in = new DataInputStream(client.getInputStream());
				transID = new byte[2];
				transID[0] = in.readByte();
				transID[1] = in.readByte();
				protoID = TCPTransport.readInt(in);
				int payloadLength = TCPTransport.readInt(in);
				if (payloadLength < 3) {
					logger.info("Non-ModbusTCP packet was received - wrong payload length (" + payloadLength + ")");
					close();
					return;
				}
				payload = new byte[payloadLength];
				if (in.read(payload) != payloadLength) {
					logger.info("Non-ModbusTCP packet was received (" + AbstractCommand.packetToHexStr(payload) + ")");
					close();
					return;
				}
			} catch (IOException e) {
				logger.debug("Cannot read data", e);
				close();
				return;
			}

			try {
				cmd = CommandsFactory.getCommand(payload);
				int code;
				if ((code = cmd.parseRequestPacket(payload)) < 0) {
					throw new ParsingException("Cannot process the payload (error code: " + code + ")");
				}
			} catch (ParsingException e) {
				logger.info("Non-ModbusTCP packet was received (" + AbstractCommand.packetToHexStr(payload) + ")");
				close();
				return;
			}

			TCPPacket<RequestCmd> packet = new TCPPacket<RequestCmd>(transID, protoID, cmd);
			logger.info("Got a mudbus-tcp command [" + packet.toString() + "]");
			queue.add(new Pair<Connection, TCPPacket<RequestCmd>>(this, packet));

			try {
				synchronized (singleCmdLock) {
					singleCmdLock.wait();
				}
			} catch (InterruptedException e) {
				logger.warn("Interrupted while waiting for a response");
			}
		}
		// close();
	}

	public void send(TCPPacket<? extends Command> command) {
		logger.info("Sending the command [" + command.toString() + "]");
		byte[] payload = command.getCmd().getBytes();
		int len = payload.length;
		byte[] data = new byte[6 + len]; // 6 -> transID + protoID + length
		data[0] = command.getTransID()[0];
		data[1] = command.getTransID()[1];
		data[2] = AbstractCommand.hiInt16(command.getProtoID());
		data[3] = AbstractCommand.loInt16(command.getProtoID());
		data[4] = AbstractCommand.hiInt16(len);
		data[5] = AbstractCommand.loInt16(len);
		for (int i = 0; i < len; i++) {
			data[6 + i] = payload[i];
		}
		try {
			try {
				DataOutputStream out = new DataOutputStream(client.getOutputStream());
				out.write(data);
				out.flush();
			} catch (IOException e) {
				logger.warn("Cannot send a response", e);
			}
		} finally {
			synchronized (singleCmdLock) {
				singleCmdLock.notify();
			}
		}
	}
}
