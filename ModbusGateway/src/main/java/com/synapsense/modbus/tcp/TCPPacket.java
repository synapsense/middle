package com.synapsense.modbus.tcp;

import com.synapsense.modbus.api.Command;
import com.synapsense.modbus.api.commands.AbstractCommand;

class TCPPacket<T extends Command> {
	private byte[] transID;
	private int protoID; // protocol id
	private T cmd;

	public TCPPacket(TCPPacket<?> request, T cmd) {
		this(request.transID, request.protoID, cmd);
	}

	public TCPPacket(byte[] transID, int protoID, T cmd) {
		this.transID = transID;
		this.protoID = protoID;
		this.cmd = cmd;
	}

	public byte[] getTransID() {
		return transID;
	}

	public int getProtoID() {
		return protoID;
	}

	public T getCmd() {
		return cmd;
	}

	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("TRANSACTION ID: " + String.format("%x:%x", transID[0], transID[1]) + "; ");
		bld.append("PROTOCOL ID: " + protoID + "; ");
		bld.append("PAYLOAD: " + AbstractCommand.packetToHexStr(cmd.getBytes()));
		return bld.toString();
	}
}
