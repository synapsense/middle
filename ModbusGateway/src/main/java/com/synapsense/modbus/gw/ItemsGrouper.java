package com.synapsense.modbus.gw;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.synapsense.modbusgw.config.Register;

/**
 * The modbus protocol has an ability to read multiple (up to 125 regs) per one
 * request; So, for a given set of known registers (or coils or something else)
 * we need to group them into a sets of continuation-indexed groups. For an
 * example, the registers 40001, 40002...40009, 40015,40016 should be divided
 * into the groups: [40001-40009][40015-40016]. If a client asks for registers
 * [40005-40006], the checker should return true, but if a client asks for
 * [40007-400012], the checker should return false;
 * 
 * @author anechaev
 * 
 */
public class ItemsGrouper {
	private final static Comparator<Register> comparator = new Comparator<Register>() {
		@Override
		public int compare(Register o1, Register o2) {
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
	};

	private List<Register> regs;

	public ItemsGrouper(Collection<Register> sourceProps) {
		regs = new ArrayList<Register>(sourceProps);
		Collections.sort(regs, comparator);
	}

	/**
	 * returns a group of registers satisfying the region given
	 * 
	 * @param startingAddress
	 *            start of region
	 * @param count
	 *            number of sequential registers to read
	 * @return collection of registers or null if the request cannot be executed
	 */
	public Collection<Register> getGroup(int startingAddress, int count) {
		Register fakeReg = new Register();
		fakeReg.setId(startingAddress);

		int idx = Collections.binarySearch(regs, fakeReg, comparator);
		if (idx < 0)
			return null; // starting register not found
		if ((idx + count) > regs.size())
			return null; // too many regs are requested

		if ((regs.get(idx + count - 1).getId() - regs.get(idx).getId()) != (count - 1))
			return null; // not a continuation

		return regs.subList(idx, idx + count);
	}
}
