package com.synapsense.modbus.gw;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import javax.naming.InitialContext;
import org.apache.log4j.Logger;

import com.synapsense.service.Environment;
import com.synapsense.util.ServerLoginModule;

public class ServiceLocator {
	private final static Logger logger = Logger.getLogger(ServiceLocator.class);

	private final static String PROPS_FILENAME = "modbusgw.conf";
	private Environment env;
	private static Properties connectProperties = new Properties();
	private volatile boolean connected = false;
	static {
        try (InputStream jbossEjbIs = ServiceLocator.class.getClassLoader().getResourceAsStream(PROPS_FILENAME)) {
            connectProperties.load(jbossEjbIs);
        } catch (Exception e) {
            logger.fatal("Cannot load \"" + PROPS_FILENAME + "\" file", e);
            throw new RuntimeException("Cannot load \"" + PROPS_FILENAME + "\" file");
        }
	}

	private static ServiceLocator inst = new ServiceLocator();

	private Object envLock = new Object();

	private class Connector implements Runnable {
		private final static int SLEEP_DELAY = 60000;

		@Override
		public void run() {

			logger.info("Connecting to the ES...");
			while (!connected) {
				synchronized (envLock) {
					try {
						InitialContext ctx = new InitialContext(connectProperties);
						env = ServerLoginModule.getService(ctx, "Environment", Environment.class);
                        env.getObjectTypes();
						connected = true;
						continue;
					} catch (Exception e) {
						logger.warn("Couldn't connect to the Environment Server", e);
					}
				}
				try {
					Thread.sleep(SLEEP_DELAY);
				} catch (InterruptedException e) {
				}
			}
			logger.info("Successfully connected to the ES");
		}

	}

	private ServiceLocator() {
		Thread thread = new Thread(new Connector(), "Modbus GW to ES connection");
		thread.setDaemon(true);
		thread.start();
	}

	public static ServiceLocator getInst() {
		return inst;
	}

	public Environment getEnv() throws NotConnectedException {
		if (!connected) {
			throw new NotConnectedException("Not connected to the ES");
		}
		try {
			InitialContext ctx = new InitialContext(connectProperties);
			env = ServerLoginModule.getService(ctx, "Environment", Environment.class);
		} catch (Exception e) {
			logger.warn("Couldn't connect to the Environment Server", e);
		}
		return env;
	}
}
