package com.synapsense.modbus.gw;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.dto.TOFactory;
import com.synapsense.modbus.api.RequestCmd;
import com.synapsense.modbus.api.ResponseCmd;
import com.synapsense.modbus.api.commands.Cmd03Response;
import com.synapsense.modbus.api.commands.Command01;
import com.synapsense.modbus.api.commands.Command02;
import com.synapsense.modbus.api.commands.Command03;
import com.synapsense.modbus.api.commands.Command04;
import com.synapsense.modbus.api.commands.Command05;
import com.synapsense.modbus.api.commands.Command06;
import com.synapsense.modbus.api.commands.Command0F;
import com.synapsense.modbus.api.commands.Command10;
import com.synapsense.modbus.api.commands.Command17;
import com.synapsense.modbus.api.commands.ErrorResponse;
import com.synapsense.modbusgw.config.Modbusdevice;
import com.synapsense.modbusgw.config.Modbusnetwork;
import com.synapsense.modbusgw.config.Register;

public class ServerCmdProcessor implements com.synapsense.modbus.api.CmdProcessor {
	private final static Logger logger = Logger.getLogger(ServerCmdProcessor.class);

	private final static int DEFAULT_ERROR_VALUE = -3000;
	private final Map<Integer, ItemsGrouper> devices;

	public ServerCmdProcessor(Modbusnetwork servedNetwork) {
		devices = new HashMap<Integer, ItemsGrouper>();
		for (Modbusdevice dev : servedNetwork.getModbusdevice()) {
			devices.put(dev.getId(), new ItemsGrouper(dev.getRegister()));
		}
	}

	private final static ResponseCmd unsupported(RequestCmd cmd) {
		logger.warn("Unsupported command: 0x" + Integer.toHexString(cmd.getCode()));
		return new ErrorResponse(cmd, ErrorResponse.EXCEPTION_CODE.FUNCTION_NOT_SUPPORTED);
	}

	@Override
	public ResponseCmd process(RequestCmd cmd) {
		return unsupported(cmd);
	}

	@Override
	public ResponseCmd process(Command01 cmd) {
		return unsupported(cmd);
	}

	@Override
	public ResponseCmd process(Command02 cmd) {
		return unsupported(cmd);
	}

	@Override
	public ResponseCmd process(Command03 cmd) {
		logger.info("Got command " + cmd.toString());

		ItemsGrouper grouper = devices.get(cmd.getSlaveAddress());
		if (grouper == null) {
			logger.warn("Unknown device id: " + cmd.getSlaveAddress());
			return new ErrorResponse(cmd, ErrorResponse.EXCEPTION_CODE.CANNOT_PROCESS_REQEST);
		}

		Collection<Register> regs = grouper.getGroup(cmd.getReadingAddress(), cmd.getRDataCounter());
		if (regs == null) {
			logger.warn("Register(s) not found (" + cmd.getReadingAddress() + ".."
			        + (cmd.getReadingAddress() + cmd.getRDataCounter() - 1) + ")");
			return new ErrorResponse(cmd, ErrorResponse.EXCEPTION_CODE.WRONG_STARTING_ADDRESS);
		}

		int[] result = new int[regs.size()];
		int i = 0;
		for (Register reg : regs) {
			logger.info("Reading the property - " + reg.getTo() + ":" + reg.getProperty());
			try {
				Double value = ServiceLocator.getInst().getEnv()
				        .getPropertyValue(TOFactory.getInstance().loadTO(reg.getTo()), reg.getProperty(), Double.class);
				if (value == null) {
					// If we didn't get any value (for example it might not be
					// calculated yet), we return default error value (-3000)
					result[i++] = DEFAULT_ERROR_VALUE;
				} else {
					result[i++] = (int) (value * reg.getScale());
				}
			} catch (Exception e) {
				try {
					Integer ivalue = ServiceLocator
					        .getInst()
					        .getEnv()
					        .getPropertyValue(TOFactory.getInstance().loadTO(reg.getTo()), reg.getProperty(),
					                Integer.class);
					if (ivalue == null) {
						result[i++] = DEFAULT_ERROR_VALUE;
					} else {
						result[i++] = (int) (ivalue * reg.getScale());
					}
				} catch (Exception e2) {
					logger.warn("Couldn't read the property " + reg.getTo() + "; " + reg.getProperty(),e2);
					return new ErrorResponse(cmd, ErrorResponse.EXCEPTION_CODE.CANNOT_PROCESS_REQEST);
				}
			}
		}

		return new Cmd03Response(cmd, result);
	}

	@Override
	public ResponseCmd process(Command04 cmd) {
		return unsupported(cmd);
	}

	@Override
	public ResponseCmd process(Command05 cmd) {
		return unsupported(cmd);
	}

	@Override
	public ResponseCmd process(Command06 cmd) {
		return unsupported(cmd);
	}

	@Override
	public ResponseCmd process(Command0F cmd) {
		return unsupported(cmd);
	}

	@Override
	public ResponseCmd process(Command10 cmd) {
		return unsupported(cmd);
	}

	@Override
	public ResponseCmd process(Command17 cmd) {
		return unsupported(cmd);
	}

}
