package com.synapsense.modbus.gw;

import org.apache.log4j.Logger;

import com.synapsense.modbusgw.config.Modbusgateway;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;

public class Launcher {
    private final static Logger logger = Logger.getLogger(Launcher.class);
    private final static String CONFIG_FILE = "modbusgw.xml";
    private static Object lock = new Object();

    public static void main(String[] args) {
        ServiceLocator.getInst();
        Modbusgateway config = readConfig(CONFIG_FILE);
        new GWCore(config).start();

        synchronized (lock) {
            try {
                lock.wait();
            } catch (InterruptedException e) {
            }
        }
    }

    private static Modbusgateway readConfig(String fileName) {
        Modbusgateway conf = null;
        try (InputStream is = Launcher.class.getClassLoader().getResourceAsStream(fileName)) {
            JAXBContext context = JAXBContext.newInstance(Modbusgateway.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            conf = (Modbusgateway) unmarshaller.unmarshal(is);
        } catch (Exception e) {
            logger.error("Couldn't read the config file " + fileName + " due to: ");
            logger.debug(e);
            System.exit(1);
        }
        return conf;
    }

    public static void stop() {
        synchronized (lock) {
            lock.notifyAll();
        }
        System.exit(0);
    }

}
