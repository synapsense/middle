package com.synapsense.modbus.gw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.modbus.tcp.TCPServer;
import com.synapsense.modbusgw.config.Modbusgateway;
import com.synapsense.modbusgw.config.Modbusnetwork;

public class GWCore {
	private final static Logger logger = Logger.getLogger(GWCore.class);
	private final Modbusgateway config;

	private Collection<TCPServer> transports;

	public GWCore(Modbusgateway config) {
		this.config = config;
	}

	public void start() {
		logger.info("Starting the modbus gateway");
		transports = new ArrayList<TCPServer>();
		for (Modbusnetwork netConfig : config.getModbusnetwork()) {
			logger.info("Initializing the network " + netConfig.getAddress());
			try {
				transports.add(new TCPServer(netConfig.getAddress(), new ServerCmdProcessor(netConfig)));
			} catch (IOException e) {
				logger.warn("Cannot create a server network on " + netConfig.getAddress(), e);
			}
		}
	}
}
