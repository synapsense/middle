package com.synapsense.modbus.gw;

public class NotConnectedException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8308426089021183759L;

	public NotConnectedException(String desc) {
		super(desc);
	}

	public NotConnectedException(String desc, Throwable cause) {
		super(desc, cause);
	}
}
