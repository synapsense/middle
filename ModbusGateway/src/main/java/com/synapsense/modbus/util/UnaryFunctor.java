package com.synapsense.modbus.util;

public interface UnaryFunctor<T> {
	void process(T value);
}
