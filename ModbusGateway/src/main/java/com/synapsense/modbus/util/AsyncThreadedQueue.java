package com.synapsense.modbus.util;

public class AsyncThreadedQueue<T> {

	private final AsyncQueue<T> queue;
	private final Thread thread;

	public AsyncThreadedQueue(UnaryFunctor<T> functor, boolean isDaemon) {
		this(functor, Integer.MAX_VALUE, isDaemon);
	}

	public AsyncThreadedQueue(UnaryFunctor<T> functor, int capacity, boolean isDaemon) {
		this(new AsyncQueue<T>(functor, capacity), isDaemon);
	}

	public AsyncThreadedQueue(AsyncQueue<T> queue, boolean isDaemon) {
		this.queue = queue;
		thread = new Thread(queue);
		thread.setDaemon(isDaemon);
		thread.start();
	}

	public void add(T element) {
		queue.add(element);
	}

	public void interrupt() {
		thread.interrupt();
		queue.interrupt();
	}
}
