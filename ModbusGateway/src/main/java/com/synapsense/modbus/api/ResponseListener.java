package com.synapsense.modbus.api;

import java.io.IOException;
import java.util.EventListener;

/**
 * Listener interface for non-blocking client calls
 * 
 * @author anechaev
 * 
 */
public interface ResponseListener extends EventListener {
	/**
	 * @param initial
	 *            - the original request
	 * @param result
	 *            - the slave's response
	 */
	void onResponse(RequestCmd initial, ResponseCmd result);

	/**
	 * indicates that the request causes a timeout
	 * 
	 * @param initial
	 *            - the request command
	 */
	void onTimeout(RequestCmd initial);

	/**
	 * indicates that an io exception occured when a transport was trying to
	 * send a command
	 * 
	 * @param initial
	 * @param exception
	 */
	void onIOException(RequestCmd initial, IOException exception);
}
