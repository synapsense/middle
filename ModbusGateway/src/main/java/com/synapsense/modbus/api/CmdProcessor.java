package com.synapsense.modbus.api;

import com.synapsense.modbus.api.commands.Command01;
import com.synapsense.modbus.api.commands.Command02;
import com.synapsense.modbus.api.commands.Command03;
import com.synapsense.modbus.api.commands.Command04;
import com.synapsense.modbus.api.commands.Command05;
import com.synapsense.modbus.api.commands.Command06;
import com.synapsense.modbus.api.commands.Command0F;
import com.synapsense.modbus.api.commands.Command10;
import com.synapsense.modbus.api.commands.Command17;

/**
 * Visitor pattern interface for modbus commands processing
 * 
 * @author anechaev
 * 
 */
public interface CmdProcessor {
	ResponseCmd process(RequestCmd cmd);

	ResponseCmd process(Command01 cmd);

	ResponseCmd process(Command02 cmd);

	ResponseCmd process(Command03 cmd);

	ResponseCmd process(Command04 cmd);

	ResponseCmd process(Command05 cmd);

	ResponseCmd process(Command06 cmd);

	ResponseCmd process(Command0F cmd);

	ResponseCmd process(Command10 cmd);

	ResponseCmd process(Command17 cmd);
}
