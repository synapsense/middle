package com.synapsense.modbus.api.exceptions;

public class ParsingException extends Exception {
	public ParsingException(String desc) {
		super(desc);
	}

	public ParsingException(String desc, Throwable cause) {
		super(desc, cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7263394208833666138L;
}
