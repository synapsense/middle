package com.synapsense.modbus.api.commands;

import com.synapsense.modbus.api.CmdProcessor;
import com.synapsense.modbus.api.ResponseCmd;
import com.synapsense.modbus.api.exceptions.ParsingException;

/**
 * @author Dmitry Koryshev
 * 
 *         Class extends {@link AbstractCommand}
 * 
 */
public class Command17 extends AbstractCommand {

	public Command17() {
		super();
		commandType = 0x17;
	}

	@Override
	protected void assembleRequestData() throws ParsingException {
		if (data == null) {
			throw new ParsingException("data is null.");
		}
		wDataCounter = data.length;
		byteCounter = data.length * 2;
		packet = new byte[1 + 1 + 2 + 2 + 2 + 2 + 1 + byteCounter + 2];
		if (writtingAddress == null) {
			throw new ParsingException("writtingAddress is null.");
		}
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			throw new ParsingException("Illegal writtingAddress.");
		}
		packet[2] = hiInt16(readingAddress - 40000);
		packet[3] = loInt16(readingAddress - 40000);
		packet[4] = hiInt16(rDataCounter);
		packet[5] = loInt16(rDataCounter);
		packet[6] = hiInt16(writtingAddress - 40000);
		packet[7] = loInt16(writtingAddress - 40000);
		packet[8] = hiInt16(wDataCounter);
		packet[9] = loInt16(wDataCounter);
		packet[10] = loInt16(byteCounter);
		for (int j = 0; j < data.length; ++j) {
			packet[11 + 2 * j] = hiInt16(data[j]);
			packet[11 + 2 * j + 1] = loInt16(data[j]);
		}

	}

	@Override
	protected void assembleResponseData() throws ParsingException {
		if (data == null) {
			throw new ParsingException("data is null.");
		}
		byteCounter = data.length * 2;
		packet = new byte[2 + 1 + byteCounter + 2];
		packet[2] = loInt16(byteCounter);

		for (int i = 0; i < data.length; ++i) {
			packet[3 + 2 * i] = hiInt16(data[i]);
			packet[3 + 2 * i + 1] = loInt16(data[i]);
		}
	}

	@Override
	protected int parseRequestData() {
		int len = packet.length;
		int n = 2; // index
		if (len < 4) {
			return -2;
		}
		readingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		readingAddress += 40000;
		if (readingAddress < 40000 || readingAddress > 49999) {
			return -4;
		}
		if (len < 6) {
			return -2;
		}
		rDataCounter = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (len < 8) {
			return -2;
		}
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		writtingAddress += 40000;
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			return -4;
		}
		if (len < 10) {
			return -2;
		}
		wDataCounter = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (len < 11) {
			return -2;
		}
		byteCounter = bToi(packet[n]);
		++n;
		if (len < 13 + byteCounter) {
			return -2;
		}

		data = new Integer[byteCounter / 2];
		int i; // index
		for (i = 0; i < data.length; ++i) {
			data[i] = dbToi(packet[n + 2 * i], packet[n + 2 * i + 1]);
		}
		n += 2 * i;
		return n;
	}

	@Override
	protected int parseResponseData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:byteCounter?
		// len<1+1+1?
		if (len < 3) {
			return -2;
		}
		byteCounter = bToi(packet[n]);
		++n;
		// len<slaveAddess:functionCode:byteCounter:data:crc16?
		// len<1+1+1+byteCounter+2?
		if (len < 5 + byteCounter) {
			return -2;
		}
		data = new Integer[byteCounter / 2];
		int i; // index
		for (i = 0; i < data.length; ++i) {
			data[i] = dbToi(packet[3 + 2 * i], packet[3 + 2 * i + 1]);
		}
		n += i * 2;
		return n;
	}

	@Override
	public ResponseCmd execute(CmdProcessor processor) {
		return processor.process(this);
	}
}
