package com.synapsense.modbus.api.commands;

import java.util.List;

import com.synapsense.modbus.api.RequestCmd;
import com.synapsense.modbus.api.exceptions.ParsingException;

/**
 * @author Dmitry Koryshev
 * 
 *         Abstract class that implements common functionality
 *         (parsing/assembling)
 * 
 */
public abstract class AbstractCommand implements RequestCmd {
	protected transient Integer commandType = null;

	protected Integer slaveAddress = null;
	protected Integer functionCode = null;
	protected Integer readingAddress = null;
	protected Integer writtingAddress = null;
	protected Integer rDataCounter = null;
	protected Integer wDataCounter = null;
	protected Integer byteCounter = null;
	protected Integer[] data = null;
	protected Integer errorCode = null;

	protected transient byte[] packet = null;

	/**
	 * 
	 * Assembles requests packets
	 * 
	 * @return Returns request packet as bytes array.
	 * @throws ModbusException
	 */
	public byte[] assembleRequestPacket() throws ParsingException {
		assembleRequestData();
		if (slaveAddress == null) {
			throw new ParsingException("slaveAddress is null.");
		}
		packet[0] = loInt16(slaveAddress);
		packet[1] = loInt16(commandType);
		int crc16 = countCRC16(packet, packet.length - 2);
		packet[packet.length - 2] = loInt16(crc16);
		packet[packet.length - 1] = hiInt16(crc16);
		return packet.clone();
	}

	/**
	 * 
	 * Assembles responses packets
	 * 
	 * @return Returns response packet as bytes array.
	 * @throws ModbusException
	 */
	public byte[] assembleResponsePacket() throws ParsingException {
		assembleResponseData();
		if (slaveAddress == null) {
			throw new ParsingException("slaveAddress is null.");
		}
		packet[0] = loInt16(slaveAddress);
		packet[1] = loInt16(commandType);
		int crc16 = countCRC16(packet, packet.length - 2);
		packet[packet.length - 2] = loInt16(crc16);
		packet[packet.length - 1] = hiInt16(crc16);
		return packet.clone();
	}

	/**
	 * Parses a head of request packet
	 * 
	 * @param pac
	 *            is bytes buffer
	 * @return state States: OK -- >=0 (value equals packet length) null -- -1
	 *         short length -- -2 bad slave address -- -3 bad address (reg/coil)
	 *         -- -4 bad CRC -- -5 unknown function code -- -6
	 */
	public int parseRequestPacket(final byte[] data) {
		if (data == null) {
			return -1;
		}
		packet = data.clone();
		int len = packet.length;
		// len<slaveAddress:functionCode?
		if (len < 2) {
			return -2;
		}
		slaveAddress = bToi(packet[0]);
		if (slaveAddress < 0 || slaveAddress > 247) {
			return -3;
		}
		functionCode = bToi(packet[1]);
		if (!functionCode.equals(commandType)) {
			return -6;
		}

		int n = parseRequestData(); // index
		if (n < 0) {
			return n;
		}

		return n;
	}

	/**
	 * Parses a head of response packet
	 * 
	 * @param pac
	 *            is bytes buffer
	 * @return state States: OK -- >=0 (value equals packet length) null -- -1
	 *         short length -- -2 bad slave address -- -3 bad address (reg/coil)
	 *         -- -4 bad CRC -- -5 unknown function code -- -6
	 */
	public int parseResponsePacket(final List<Byte> pac) {
		int i; // index
		if (pac == null) {
			return -1;
		}
		packet = new byte[pac.size()];
		for (i = 0; i < pac.size(); ++i) {
			packet[i] = pac.get(i).byteValue();
		}
		int len = packet.length;
		// len<slaveAddress:functionCode?
		if (len < 2) {
			return -2;
		}
		slaveAddress = bToi(packet[0]);
		if (slaveAddress < 0 || slaveAddress > 247) {
			return -3;
		}
		functionCode = bToi(packet[1]);
		if (!functionCode.equals(commandType)) {
			return -6;
		}

		int n = parseResponseData(); // index
		if (n < 0) {
			return n;
		}

		return n;
	}

	/**
	 * 
	 * Parses a message body
	 * 
	 * @return Returns a state
	 */
	protected abstract int parseRequestData();

	protected abstract int parseResponseData();

	protected abstract void assembleRequestData() throws ParsingException;

	protected abstract void assembleResponseData() throws ParsingException;

	/**
	 * Transforms byte to int
	 * 
	 * @param byteValue
	 *            is a byte value
	 * @return value as int
	 */
	public static int bToi(final byte byteValue) {
		return (int) (byteValue & 0xFF);
	}

	/**
	 * 
	 * Transforms two bytes to int
	 * 
	 * @param hiByte
	 *            is a Hi-byte value
	 * @param loByte
	 *            is a Lo-byte value
	 * @return value as int
	 */
	public static int dbToi(final byte hiByte, final byte loByte) {
		int result = bToi(hiByte) << 8;
		result += bToi(loByte);
		return result;
	}

	/**
	 * Extract bits from 8 to 15 as byte
	 * 
	 * @param value
	 *            is a integer value
	 * @return byte value
	 */
	public static byte hiInt16(final Integer value) {
		return (Integer.valueOf((value & 0xFF00) >> 8)).byteValue();
	}

	/**
	 * Extract bits from 0 to 7 as byte
	 * 
	 * @param value
	 *            is a integer value
	 * @return byte value
	 */
	public static byte loInt16(final Integer value) {
		return (Integer.valueOf(value & 0xFF)).byteValue();
	}

	/**
	 * 
	 * Calculates CRC16 summ
	 * 
	 * @param p
	 *            is a byte array
	 * @param len
	 *            is number of bytes which method uses
	 * @return CRC16 summ
	 */
	public static int countCRC16(final byte[] p, final int len) {
		/* Table of CRC values for high-order byte */
		int[] crc16H = { 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
		        0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00,
		        0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
		        0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
		        0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
		        0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00,
		        0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
		        0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80,
		        0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00,
		        0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
		        0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
		        0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
		        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00,
		        0xC1, 0x81, 0x40 };

		/* Table of CRC values for low-order byte */

		int[] crc16L = { 0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
		        0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8,
		        0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14,
		        0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3, 0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30,
		        0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD,
		        0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
		        0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27,
		        0xE7, 0xE6, 0x26, 0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3,
		        0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E,
		        0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
		        0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5, 0x77, 0xB7, 0xB6, 0x76, 0x72,
		        0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56,
		        0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B,
		        0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F,
		        0x8D, 0x4D, 0x4C, 0x8C, 0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41,
		        0x81, 0x80, 0x40

		};
		int i;
		int cksum_high = 0xFF;
		int cksum_low = 0xFF;
		for (int j = 0; j < len; ++j) {
			int p_j = ((int) p[j]) & 0xFF;
			i = cksum_high ^ p_j;
			cksum_high = cksum_low ^ crc16H[i];
			cksum_low = crc16L[i];
		}
		return (cksum_low << 8) | cksum_high;
	}

	/**
	 * 
	 * Translates a byte array to string that will be contain hex values for
	 * bytes
	 * 
	 * @param p
	 *            is a byte array
	 * @return translated string
	 */
	public static String packetToHexStr(final byte[] p) {
		StringBuffer tmpBufer = new StringBuffer();
		for (int i = 0; i < p.length; ++i) {
			String tt = Integer.toHexString(bToi(p[i]));
			tmpBufer.append(':');
			if (tt.length() < 2) {
				tmpBufer.append('0');
			}
			tmpBufer.append(tt);
		}
		tmpBufer.append(':');
		return tmpBufer.substring(0, tmpBufer.length());
	}

	public Integer getSlaveAddress() {
		return slaveAddress;
	}

	public void setSlaveAddress(final Integer slaveAddress) {
		this.slaveAddress = slaveAddress;
	}

	public Integer getFunctionCode() {
		return functionCode;
	}

	public void setFunctionCode(final Integer functionCode) {
		this.functionCode = functionCode;
	}

	public Integer getReadingAddress() {
		return readingAddress;
	}

	public void setReadingAddress(final Integer readingAddress) {
		this.readingAddress = readingAddress;
	}

	public Integer getWrittingAddress() {
		return writtingAddress;
	}

	public void setWrittingAddress(final Integer writtingAddress) {
		this.writtingAddress = writtingAddress;
	}

	public Integer getRDataCounter() {
		return rDataCounter;
	}

	public void setRDataCounter(final Integer dataCounter) {
		rDataCounter = dataCounter;
	}

	public Integer getWDataCounter() {
		return wDataCounter;
	}

	public void setWDataCounter(final Integer dataCounter) {
		wDataCounter = dataCounter;
	}

	public Integer getByteCounter() {
		return byteCounter;
	}

	public void setByteCounter(final Integer byteCounter) {
		this.byteCounter = byteCounter;
	}

	public Integer[] getData() {
		return data.clone();
	}

	public void setData(final Integer[] data) {
		this.data = data.clone();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(final Integer errorCode) {
		this.errorCode = errorCode;
	}

	public Integer getCommandType() {
		return commandType;
	}

	@Override
	public byte getCode() {
		return (commandType.byteValue());
	}

	@Override
	public byte[] getBytes() {
		try {
			return assembleRequestPacket();
		} catch (ParsingException e) {
			throw new IllegalStateException("Something wrong happened...", e);
		}
	}

	@Override
	public byte getSlaveAddr() {
		return loInt16(slaveAddress);
	}
}
