package com.synapsense.modbus.api;

/**
 * Modbus response command abstraction
 * 
 * @author anechaev
 * 
 */
public interface ResponseCmd extends Command {

}
