package com.synapsense.modbus.api;

/**
 * Modbus requests abstraction
 * 
 * @author anechaev
 * 
 */
public interface RequestCmd extends Command {
	/**
	 * Visitor's visit method
	 * 
	 * @param processor
	 * @return command result; both normal & exceptional results;
	 */
	ResponseCmd execute(CmdProcessor processor);

	/**
	 * @return functional code of the command
	 */
	byte getCode();

	/**
	 * @return slave address (aka unit ID)
	 */
	byte getSlaveAddr();
}
