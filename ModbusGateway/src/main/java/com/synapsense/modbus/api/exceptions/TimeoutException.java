package com.synapsense.modbus.api.exceptions;

/**
 * This exception is thrown by a transport when a slave device does not respond
 * for a time period
 * 
 * @author anechaev
 * 
 */
public class TimeoutException extends Exception {

	public TimeoutException(String desc) {
		super(desc);
	}

	public TimeoutException(String desc, Throwable cause) {
		super(desc, cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4875271831759985641L;
}
