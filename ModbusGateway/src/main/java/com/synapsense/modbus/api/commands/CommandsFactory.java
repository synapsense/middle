package com.synapsense.modbus.api.commands;

import com.synapsense.modbus.api.exceptions.ParsingException;

/**
 * @author Dmitry Koryshev
 * 
 *         Creates commands
 * 
 */
public class CommandsFactory {
	public static AbstractCommand getCommand(final int functionCode) throws ParsingException {
		switch (functionCode) {
		case 0x01:
			return new Command01();
		case 0x02:
			return new Command02();
		case 0x03:
			return new Command03();
		case 0x04:
			return new Command04();
		case 0x05:
			return new Command05();
		case 0x06:
			return new Command06();
		case 0x0F:
			return new Command0F();
		case 0x10:
			return new Command10();
		case 0x17:
			return new Command17();
		default:
			throw new ParsingException("Unsupported function code - " + functionCode + ".");
		}
	}

	public static AbstractCommand getCommand(final byte[] pac) throws ParsingException {
		if (pac.length < 2)
			throw new ParsingException("It isn't Modbus packet.");
		int functionCode = AbstractCommand.bToi(pac[1]);
		return getCommand(functionCode);
	}

}
