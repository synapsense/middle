package com.synapsense.modbus.api;

import java.io.IOException;

import com.synapsense.modbus.api.exceptions.TimeoutException;

/**
 * Transport abstraction Transport is responsible both for sending requests and
 * 
 * @author anechaev
 * 
 */
public interface Transport {
	/**
	 * Master-mode blocking call.
	 * 
	 * @param cmd
	 * @return
	 * @throws IOException
	 *             when a transport is down
	 * @throws TimeoutException
	 *             when a callee does not answer for a time period
	 */
	ResponseCmd request(RequestCmd cmd) throws IOException, TimeoutException;

	/**
	 * Master-mode non-blocking call
	 * 
	 * @param cmd
	 * @param listener
	 *            - a listener who will get a result (either a positive or
	 *            timeout)
	 */
	void request(RequestCmd cmd, ResponseListener listener);

	/**
	 * Closes the transport
	 * 
	 */
	void shutdown();
}
