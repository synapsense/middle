package com.synapsense.modbus.api.commands;

import java.util.Arrays;

import com.synapsense.modbus.api.CmdProcessor;
import com.synapsense.modbus.api.ResponseCmd;
import com.synapsense.modbus.api.exceptions.ParsingException;

/**
 * @author Dmitry Koryshev
 * 
 *         Class extends {@link AbstractCommand}
 * 
 */
public class Command0F extends AbstractCommand {

	public Command0F() {
		super();
		commandType = 0x0F;
	}

	@Override
	protected void assembleRequestData() throws ParsingException {
		if (data == null) {
			throw new ParsingException("data is null.");
		}
		wDataCounter = data.length;
		byteCounter = data.length / 8;
		if (data.length % 8 != 0) {
			++byteCounter;
		}
		Integer[] ldata = new Integer[byteCounter];
		Arrays.fill(ldata, Integer.valueOf(0));
		int b = 0; // index
		int j = 0; // index
		for (int i = 0; i < data.length; ++i) {
			j = i / 8;
			b = i % 8;
			if (data[i] != 0) {
				ldata[j] |= 1 << b;
			}
		}
		packet = new byte[1 + 1 + 2 + 2 + 1 + byteCounter + 2];
		if (writtingAddress == null) {
			throw new ParsingException("writtingAddress is null.");
		}
		if (writtingAddress < 0 || writtingAddress > 9999) {
			throw new ParsingException("Illegal writtingAddress.");
		}
		packet[2] = hiInt16(writtingAddress);
		packet[3] = loInt16(writtingAddress);
		packet[4] = hiInt16(wDataCounter);
		packet[5] = loInt16(wDataCounter);
		packet[6] = loInt16(byteCounter);
		for (j = 0; j < ldata.length; ++j) {
			packet[7 + j] = loInt16(ldata[j]);
		}

	}

	@Override
	protected void assembleResponseData() throws ParsingException {
		if (writtingAddress == null) {
			throw new ParsingException("writtingAddress is null.");
		}
		if (writtingAddress < 0 || writtingAddress > 9999) {
			throw new ParsingException("Illegal writtingAddress.");
		}
		if (wDataCounter == null) {
			throw new ParsingException("wDataCounter is null.");
		}
		packet = new byte[8];
		packet[2] = hiInt16(writtingAddress);
		packet[3] = loInt16(writtingAddress);
		packet[4] = hiInt16(wDataCounter);
		packet[5] = loInt16(wDataCounter);

	}

	@Override
	protected int parseRequestData() {
		int len = packet.length;
		if (len < 4) {
			return -2;
		}
		int n = 2; // index
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (len < 6) {
			return -2;
		}
		wDataCounter = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (len < 7) {
			return -2;
		}
		byteCounter = bToi(packet[n]);
		++n;
		if (len < 9 + byteCounter) {
			return -2;
		}

		int b = 0; // index
		int j = 0; // index
		data = new Integer[byteCounter * 8];
		Integer[] tmp = new Integer[byteCounter];
		int i; // index
		for (i = 0; i < byteCounter; ++i) {
			tmp[i] = bToi(packet[n + i]);
		}
		for (i = 0; i < data.length; ++i) {
			j = i / 8;
			b = i % 8;
			if ((tmp[j] & (1 << b)) != 0) {
				data[i] = 0xFF00;
			} else {
				data[i] = 0x0000;
			}
		}
		n += i / 8;
		return n;
	}

	@Override
	protected int parseResponseData() {
		int n = 2; // index
		int len = packet.length;
		if (len < 8) {
			return -2;
		}
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (writtingAddress < 0 || writtingAddress > 9999) {
			return -4;
		}
		wDataCounter = dbToi(packet[n], packet[n + 1]);
		n += 2;
		return n;
	}

	/**
	 * 
	 * Wraps assembleRequestPacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param writtingAddress
	 *            is a writing address
	 * @param data
	 *            is a data
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleRequestPacket(final Integer slaveAddress, final Integer writtingAddress, final Integer[] data)
	        throws ParsingException {
		this.slaveAddress = slaveAddress;
		this.writtingAddress = writtingAddress;
		this.data = data.clone();
		return super.assembleRequestPacket();
	}

	/**
	 * 
	 * Wraps assembleResponsePacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param writtingAddress
	 *            is a writing address
	 * @param wDataCounter
	 *            is a data counter
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleResponsePacket(final Integer slaveAddress, final Integer writtingAddress,
	        final Integer wDataCounter) throws ParsingException {
		this.slaveAddress = slaveAddress;
		this.writtingAddress = writtingAddress;
		this.wDataCounter = wDataCounter;
		return super.assembleResponsePacket();
	}

	@Override
	public ResponseCmd execute(CmdProcessor processor) {
		return processor.process(this);
	}
}
