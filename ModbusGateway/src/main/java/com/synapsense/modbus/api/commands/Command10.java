package com.synapsense.modbus.api.commands;

import com.synapsense.modbus.api.CmdProcessor;
import com.synapsense.modbus.api.ResponseCmd;
import com.synapsense.modbus.api.exceptions.ParsingException;

/**
 * @author Dmitry Koryshev
 * 
 *         Class extends {@link AbstractCommand}
 * 
 */
public class Command10 extends AbstractCommand {

	public Command10() {
		super();
		commandType = 0x10;
	}

	@Override
	protected void assembleRequestData() throws ParsingException {
		if (data == null) {
			throw new ParsingException("data is null.");
		}
		wDataCounter = data.length;
		byteCounter = data.length * 2;
		packet = new byte[1 + 1 + 2 + 2 + 1 + byteCounter + 2];
		if (writtingAddress == null) {
			throw new ParsingException("writtingAddress is null.");
		}
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			throw new ParsingException("Illegal writtingAddress.");
		}
		packet[2] = hiInt16(writtingAddress - 40000);
		packet[3] = loInt16(writtingAddress - 40000);
		packet[4] = hiInt16(wDataCounter);
		packet[5] = loInt16(wDataCounter);
		packet[6] = loInt16(byteCounter);
		for (int j = 0; j < data.length; ++j) {
			packet[7 + 2 * j] = hiInt16(data[j]);
			packet[7 + 2 * j + 1] = loInt16(data[j]);
		}

	}

	@Override
	protected void assembleResponseData() throws ParsingException {
		if (writtingAddress == null) {
			throw new ParsingException("writtingAddress is null.");
		}
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			throw new ParsingException("Illegal writtingAddress.");
		}
		if (wDataCounter == null) {
			throw new ParsingException("wDataCounter is null.");
		}
		packet = new byte[8];
		packet[2] = hiInt16(writtingAddress - 40000);
		packet[3] = loInt16(writtingAddress - 40000);
		packet[4] = hiInt16(wDataCounter);
		packet[5] = loInt16(wDataCounter);

	}

	@Override
	protected int parseRequestData() {
		int len = packet.length;
		if (len < 4) {
			return -2;
		}
		int n = 2; // index
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		writtingAddress += 40000;
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			return -4;
		}
		if (len < 6) {
			return -2;
		}
		wDataCounter = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (len < 7) {
			return -2;
		}
		byteCounter = bToi(packet[n]);
		++n;
		if (len < 9 + byteCounter) {
			return -2;
		}

		data = new Integer[byteCounter / 2];
		int i; // index
		for (i = 0; i < data.length; ++i) {
			data[i] = dbToi(packet[n + 2 * i], packet[n + 2 * i + 1]);
		}
		n += 2 * i;
		return n;
	}

	@Override
	protected int parseResponseData() {
		int n = 2; // index
		int len = packet.length;
		if (len < 8) {
			return -2;
		}
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		writtingAddress += 40000;
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			return -4;
		}
		wDataCounter = dbToi(packet[n], packet[n + 1]);
		n += 2;
		return n;
	}

	/**
	 * 
	 * Wraps assembleRequestPacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param writtingAddress
	 *            is a writing address
	 * @param data
	 *            is a data
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleRequestPacket(final Integer slaveAddress, final Integer writtingAddress, final Integer[] data)
	        throws ParsingException {
		this.slaveAddress = slaveAddress;
		this.writtingAddress = writtingAddress;
		this.data = data.clone();
		return super.assembleRequestPacket();
	}

	/**
	 * 
	 * Wraps assembleResponsePacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param writtingAddress
	 *            is a writing address
	 * @param wDataCounter
	 *            is a data counter
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleResponsePacket(final Integer slaveAddress, final Integer writtingAddress,
	        final Integer wDataCounter) throws ParsingException {
		this.slaveAddress = slaveAddress;
		this.writtingAddress = writtingAddress;
		this.wDataCounter = wDataCounter;
		return super.assembleResponsePacket();
	}

	@Override
	public ResponseCmd execute(CmdProcessor processor) {
		return processor.process(this);
	}
}
