package com.synapsense.modbus.api.commands;

import com.synapsense.modbus.api.RequestCmd;
import com.synapsense.modbus.api.ResponseCmd;

public class ErrorResponse implements ResponseCmd {
	public enum EXCEPTION_CODE {
	FUNCTION_NOT_SUPPORTED((byte) 1), BAD_QUANTITY_OF_ITEMS((byte) 3), WRONG_STARTING_ADDRESS((byte) 2), CANNOT_PROCESS_REQEST(
	        (byte) 4);

	private final byte code;

	private EXCEPTION_CODE(byte code) {
		this.code = code;
	}

	public byte getCode() {
		return code;
	}
	}

	private final byte[] data;

	public ErrorResponse(RequestCmd request, EXCEPTION_CODE reason) {
		data = new byte[3];
		data[0] = request.getSlaveAddr();
		data[1] = (byte) (0x80 | request.getCode());
		data[2] = reason.getCode();
	}

	@Override
	public byte[] getBytes() {
		return data.clone();
	}
}
