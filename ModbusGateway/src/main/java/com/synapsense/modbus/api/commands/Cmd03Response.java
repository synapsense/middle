package com.synapsense.modbus.api.commands;

import com.synapsense.modbus.api.ResponseCmd;

public class Cmd03Response implements ResponseCmd {
	private final byte[] bytes;

	public Cmd03Response(Command03 request, int[] data) {
		if (request.getRDataCounter() != data.length) {
			throw new IllegalArgumentException("Actual data size doesn't equal requested one");
		}
		bytes = new byte[data.length * 2 + 3];
		bytes[0] = request.getSlaveAddr();
		bytes[1] = 0x03;
		bytes[2] = (byte) (data.length * 2);
		for (int i = 0; i < data.length; i++) {
			bytes[2 * i + 3] = AbstractCommand.hiInt16(data[i]);
			bytes[2 * i + 4] = AbstractCommand.loInt16(data[i]);
		}
	}

	@Override
	public byte[] getBytes() {
		return bytes.clone();
	}
}
