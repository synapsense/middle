package com.synapsense.modbus.api.commands;

import java.util.Arrays;

import com.synapsense.modbus.api.CmdProcessor;
import com.synapsense.modbus.api.ResponseCmd;
import com.synapsense.modbus.api.exceptions.ParsingException;

/**
 * @author Dmitry Koryshev
 * 
 *         Class extends {@link AbstractCommand}
 * 
 */
public class Command01 extends AbstractCommand {

	public Command01() {
		super();
		commandType = 0x01;
	}

	@Override
	protected int parseRequestData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:readingAddress:rDataCounter:crc16?
		// len<1+1+2+2+2?
		if (len < 6) {
			return -2;
		}
		readingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (readingAddress < 0 || readingAddress > 9999) {
			return -4;
		}
		rDataCounter = dbToi(packet[n], packet[n + 1]);
		n += 2;
		return n;
	}

	@Override
	protected int parseResponseData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:byteCounter?
		// len<1+1+1?
		if (len < 3) {
			return -2;
		}
		byteCounter = bToi(packet[n]);
		++n;
		// len<slaveAddess:functionCode:byteCounter:data:crc16?
		// len<1+1+1+byteCounter+2?
		if (len < 5 + byteCounter) {
			return -2;
		}

		int b; // index
		int j; // index
		data = new Integer[byteCounter * 8];
		Integer[] tmp = new Integer[byteCounter];
		int i; // index
		for (i = 0; i < byteCounter; ++i) {
			tmp[i] = bToi(packet[n + i]);
		}
		for (i = 0; i < data.length; ++i) {
			j = i / 8;
			b = i % 8;
			if ((tmp[j] & (1 << b)) != 0) {
				data[i] = 0xFF00;
			} else {
				data[i] = 0x0000;
			}
		}
		n += i / 8;

		return n;
	}

	@Override
	protected void assembleRequestData() throws ParsingException {
		if (readingAddress == null) {
			throw new ParsingException("readingAddress is null.");
		}
		if (readingAddress < 0 || readingAddress > 9999) {
			throw new ParsingException("Illegal readingAddress.\n" + "\tNow address is " + readingAddress + "\n"
			        + "\tbut address must be between 0 and 9999");
		}
		if (rDataCounter == null) {
			throw new ParsingException("rDataCounter is null.");
		}
		packet = new byte[8];
		packet[2] = hiInt16(readingAddress);
		packet[3] = loInt16(readingAddress);
		packet[4] = hiInt16(rDataCounter);
		packet[5] = loInt16(rDataCounter);
	}

	@Override
	protected void assembleResponseData() throws ParsingException {
		if (data == null) {
			throw new ParsingException("data is null.");
		}
		byteCounter = data.length / 8;
		if (data.length % 8 != 0) {
			++byteCounter;
		}
		Integer[] ldata = new Integer[byteCounter];
		Arrays.fill(ldata, Integer.valueOf(0));

		int b = 0;
		int j = 0;
		for (int i = 0; i < data.length; ++i) {
			j = i / 8;
			b = i % 8;
			if (data[i] != 0) {
				ldata[j] |= 1 << b;
			}
		}
		packet = new byte[2 + 1 + ldata.length + 2];
		packet[2] = loInt16(byteCounter);
		for (j = 0; j < ldata.length; ++j) {
			packet[3 + j] = loInt16(ldata[j]);
		}
	}

	/**
	 * Wraps assembleRequestPacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param readingAddress
	 *            is a reading address
	 * @param rDataCounter
	 *            is a reading data counter
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleRequestPacket(final Integer slaveAddress, final Integer readingAddress,
	        final Integer rDataCounter) throws ParsingException {
		this.slaveAddress = slaveAddress;
		this.readingAddress = readingAddress;
		this.rDataCounter = rDataCounter;
		return super.assembleRequestPacket();
	}

	/**
	 * 
	 * Wraps assembleResponsePacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param data
	 *            is a data
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleResponsePacket(final Integer slaveAddress, final Integer[] data) throws ParsingException {
		this.slaveAddress = slaveAddress;
		this.data = data.clone();
		return super.assembleResponsePacket();
	}

	@Override
	public ResponseCmd execute(CmdProcessor processor) {
		return processor.process(this);
	}
}
