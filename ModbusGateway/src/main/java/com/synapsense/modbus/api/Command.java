package com.synapsense.modbus.api;

public interface Command {
	/**
	 * gets packed command
	 * 
	 * @return
	 */
	byte[] getBytes();
}
