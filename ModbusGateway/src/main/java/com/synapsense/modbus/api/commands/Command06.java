package com.synapsense.modbus.api.commands;

import com.synapsense.modbus.api.CmdProcessor;
import com.synapsense.modbus.api.ResponseCmd;
import com.synapsense.modbus.api.exceptions.ParsingException;

/**
 * @author Dmitry Koryshev
 * 
 *         Class extends {@link AbstractCommand}
 * 
 */
public class Command06 extends AbstractCommand {

	public Command06() {
		super();
		commandType = 0x06;
	}

	@Override
	protected void assembleRequestData() throws ParsingException {
		if (writtingAddress == null) {
			throw new ParsingException("writtingAddress is null.");
		}
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			throw new ParsingException("Illegal writtingAddress.");
		}
		if (data == null) {
			throw new ParsingException("data is null.");
		}
		if (data.length != 1) {
			throw new ParsingException("data length isn't equals 1.");
		}
		packet = new byte[8];
		packet[2] = hiInt16(writtingAddress - 40000);
		packet[3] = loInt16(writtingAddress - 40000);
		packet[4] = hiInt16(data[0]);
		packet[5] = loInt16(data[0]);
	}

	@Override
	protected void assembleResponseData() throws ParsingException {
		if (writtingAddress == null) {
			throw new ParsingException("writtingAddress is null.");
		}
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			throw new ParsingException("Illegal writtingAddress.");
		}
		if (data == null) {
			throw new ParsingException("data is null.");
		}
		if (data.length != 1) {
			throw new ParsingException("data length isn't equals 1.");
		}
		packet = new byte[8];
		packet[2] = hiInt16(writtingAddress - 40000);
		packet[3] = loInt16(writtingAddress - 40000);
		packet[4] = hiInt16(data[0]);
		packet[5] = loInt16(data[0]);
	}

	@Override
	protected int parseRequestData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:writtingAddress:data:crc16?
		// len<1+1+2+2+2?
		if (len < 6) {
			return -2;
		}
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		writtingAddress += 40000;
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			return -4;
		}
		data = new Integer[1];
		data[0] = dbToi(packet[n], packet[n + 1]);
		n += 2;
		return n;
	}

	@Override
	protected int parseResponseData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:writtingAddress:data:crc16?
		// len<1+1+2+2+2?
		if (len < 6) {
			return -2;
		}
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		writtingAddress += 40000;
		if (writtingAddress < 40000 || writtingAddress > 49999) {
			return -4;
		}
		data = new Integer[1];
		data[0] = dbToi(packet[n], packet[n + 1]);
		n += 2;
		return n;
	}

	/**
	 * 
	 * Wraps assembleRequestPacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param writtingAddress
	 *            is a writing address
	 * @param data
	 *            is a data
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleRequestPacket(final Integer slaveAddress, final Integer writtingAddress, final Integer[] data)
	        throws ParsingException {
		this.slaveAddress = slaveAddress;
		this.writtingAddress = writtingAddress;
		this.data = data.clone();
		return super.assembleRequestPacket();
	}

	/**
	 * 
	 * Wraps assembleResponsePacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param writtingAddress
	 *            is a writing address
	 * @param data
	 *            is a data
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleResponsePacket(final Integer slaveAddress, final Integer writtingAddress, final Integer[] data)
	        throws ParsingException {
		this.slaveAddress = slaveAddress;
		this.writtingAddress = writtingAddress;
		this.data = data.clone();
		return super.assembleResponsePacket();
	}

	@Override
	public ResponseCmd execute(CmdProcessor processor) {
		return processor.process(this);
	}
}
